<?php

class ProductionOrders extends Eloquent 
{
	protected $table = 'production_orders';
	
	public function loc()
	{
		return $this->belongsTo('Location', 'loc_id');
	}
	public function subLoc()
	{
		return $this->belongsTo('Location', 'sub_loc_id');
	}
	
}

?>