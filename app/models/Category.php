<?php

class Category extends Eloquent 
{
	
	public function inventory()
	{
		return $this->hasMany('Inventory');
	}
	
	public static function with_parent()
	{
		return $this->belongsTo('Category', 'parent');
	}
}

?>