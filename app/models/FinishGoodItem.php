<?php

class FinishGoodItem extends Eloquent 
{
	protected $table = 'finish_good_items';
	
	public function inventory()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}

}

?>