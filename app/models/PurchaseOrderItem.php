<?php

class PurchaseOrderItem extends Eloquent 
{
	protected $table = 'purchaseorder_items';
	
	public function po()
	{
		return $this->belongsTo('Purchaseorder', 'po_id');
	}
	
	public function rr()
	{
		return $this->hasOne('ReceivingReport', 'po_item_id');
	}
	
	public function prItem()
	{
		return $this->belongsTo('Purchaseitem', 'pr_item_id');
	}
	
	public static function count_po_quantity($pr_id, $status = '')
	{
		$total = DB::table('purchaseorder_items')
				->leftjoin('purchaseorders', 'purchaseorders.id', '=', 'purchaseorder_items.po_id')
				->where('purchaseorders.status', $status)
				->where('pr_item_id', $pr_id)
				->sum('quantity');
		
		return $total;
	}
}

?>