<?php

class Purchaseorder extends Eloquent 
{
/*
uses
$po = Purchaseorder::all();
	foreach($po as $p):
	
	//get the name of the category
	$p->categoryname->name;

	// of the supplier
	$p->supplier->name

etc...


	endforeach;
*/



	public function categoryname()
	{
		return $this->belongsTo('Category', 'category');
	}
	
	public function pr()
	{
		return $this->belongsTo('PurchaseRequisition', 'pr_id');
	}
	
	public function supplier()
	{
		return $this->belongsTo('Supplier', 'sp_id');
	}

	public function requesteBy()
	{
		return $this->belongsTo('User', 'requested_by');
	}
	
	public function approvedBy()
	{
		return $this->belongsTo('User', 'approved_by');
	}

	public function preparedBy()
	{
		return $this->belongsTo('User', 'prepared_by');
	}
}

?>