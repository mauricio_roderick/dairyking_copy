<?php

class CustomerItem extends Eloquent 
{
	public function itemInventory()
	{
		return $this->belongsTo('ItemInventory', 'item_id');
	}
}

?>