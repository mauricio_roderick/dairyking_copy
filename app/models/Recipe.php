<?php

class Recipe extends Eloquent 
{
	public function recipeItems()
	{
		return $this->hasMany('RecipeItem', 'rcp_id');
	}
	
	public function inv()
	{
		return $this->belongsTo('Inventory', 'finish_prod_id');
	}
}

?>