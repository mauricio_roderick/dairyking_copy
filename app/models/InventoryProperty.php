<?php

class InventoryProperty extends Eloquent 
{
	protected $table = 'inventoryproperties';
	
	
	public function rr()
	{
		return $this->belongsTo('ReceivingReport', 'rr_id');
	}
	
	public function inv()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}
	
	public function parentLocation()
	{
		return $this->belongsTo('Location', 'location');
	}
	
	public function subLocation()
	{
		return $this->belongsTo('Location', 'sub_location');
	}
	
}

?>