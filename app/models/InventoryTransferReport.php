<?php

class InventoryTransferReport extends Eloquent 
{
	protected $table = 'inventory_transfer_reports';
	
	public function inventory()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}

	public function fromLocation()
	{
		return $this->belongsTo('Location', 'from_location');
	}

	public function fromSubLocation()
	{
		return $this->belongsTo('Location', 'from_sub_location');
	}

	public function toLocation()
	{
		return $this->belongsTo('Location', 'to_location');
	}

	public function toSubLocation()
	{
		return $this->belongsTo('Location', 'to_sub_location');
	}

}

?>