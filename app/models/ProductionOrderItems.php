<?php

class ProductionOrderItems extends Eloquent 
{
	protected $table = 'production_order_items';
	
	public function productionOrder()
	{
		return $this->belongsTo('ProductionOrders', 'prod_ord_id');
	}
	
	public function invProperty()
	{
		return $this->belongsTo('InventoryProperty', 'inv_prop_id');
	}
}

?>