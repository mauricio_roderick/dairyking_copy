<?php

class RecipeItem extends Eloquent 
{
	protected $table = 'recipe_items';
	
	public function inventory()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}

	public function parentItem()
	{
		return $this->belongsTo('RecipeItem', 'parent');
	}

}

?>