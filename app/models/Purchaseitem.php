<?php

class Purchaseitem extends Eloquent 
{
/*
uses
$pi = Purchaseitem::all();
	foreach($pi as $p):
	
	//get the name of the item
	$p->inv->name;

	//get the original date of the purchase order
	$p->po->date

     etc...


	endforeach;
*/


	public function po() //purchase order
	{
		return $this->belongsTo('Purchaseorder', 'po_id');
	}
	public function pr()
	{
		return $this->belongsTo('PurchaseRequisition', 'pr_id');
	}
	public function inv()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}
}

?>