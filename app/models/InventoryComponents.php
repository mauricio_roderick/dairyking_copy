<?php

class InventoryComponents extends Eloquent 
{
	public function inv()
	{
		return $this->belongsTo('Inventory', 'iv_id');
	}
	
	public function parentInventory()
	{
		return $this->belongsTo('Inventory', 'inventory');
	}
}

?>