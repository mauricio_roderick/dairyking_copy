<?php

class PurchaseRequisition extends Eloquent 
{
	protected $table = 'purchaserequisitions';
/*
uses
$pr = PurchaseRequisition::all();
	foreach($pr as $p):
	
	//get the name of the requester
	$p->requester->name;

	// of the approver
	$p->approver->name




	endforeach;
*/
	public function requester()
	{
		return $this->belongsTo('User', 'requested_by');
	}
	
	public function approver()
	{
		return $this->belongsTo('User', 'approved_by');
	}

	public function loc()
	{
		return $this->belongsTo('Location', 'loc_id');
	}

	public function order()
	{
		return $this->hasMany('Purchaseorder', 'po_id');
	}

	public function addressedTo()
	{
		return $this->belongsTo('User', 'addressed_to');
	}

}

?>