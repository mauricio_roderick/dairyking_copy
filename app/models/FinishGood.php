<?php

class FinishGood extends Eloquent 
{
	protected $table = 'finish_goods';
	
	public function inv()
	{
		return $this->belongsTo('Inventory', 'inv_id');
	}

	public function productionOrder()
	{
		return $this->belongsTo('ProductionOrders', 'prod_ord_id');
	}

}

?>