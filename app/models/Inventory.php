<?php

class Inventory extends Eloquent 
{
	public function inventorySupplier()
	{
		return $this->belongsTo('Supplier', 'supplier');
	}
	
	public function invCategory()
	{
		return $this->belongsTo('SupplierCategory', 'category');
	}
	
}

?>