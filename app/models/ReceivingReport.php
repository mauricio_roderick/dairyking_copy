<?php

class ReceivingReport extends Eloquent 
{
	protected $table = 'receivingreports';
	
	public function user()
	{
		return $this->belongsTo('User', 'received_by');
	}
	
	public function poItem()
	{
		return $this->belongsTo('PurchaseOrderItem', 'po_item_id');
	}
	
	public function fromLocation()
	{
		return $this->belongsTo('Location', 'from_location');
	}
	
	public function fromSubLocation()
	{
		return $this->belongsTo('Location', 'from_sub_location');
	}
	
	public function toLocation()
	{
		return $this->belongsTo('Location', 'to_location');
	}
	
	public function toSubLocation()
	{
		return $this->belongsTo('Location', 'to_sub_location');
	}
	
	public static function get_items_for_receiving($po_id)
	{
		$items_for_receiving = DB::table('purchaseorder_items')
            ->leftjoin('receivingreports', 'purchaseorder_items.id', '=', 'receivingreports.po_item_id')
			->where('purchaseorder_items.po_id', $po_id)
			->get();
			
		return $items_for_receiving;	
	}
	
	public static function get_all_width_total_distributed()
	{
		// SELECT SUM( inventoryproperties.quantityofdelivery ) AS  'total_distributed', purchaseorder_items.quantity
		// FROM  `receivingreports` 
		// LEFT JOIN inventoryproperties ON inventoryproperties.rr_id = receivingreports.id
		// LEFT JOIN purchaseorder_items ON receivingreports.po_item_id = purchaseorder_items.id
		// GROUP BY inventoryproperties.rr_id
		
		
		$query = DB::table('receivingreports')
			->select(DB::raw("SUM( inventoryproperties.quantityofdelivery ) AS  'total_distributed_units', purchaseorder_items.quantity as available_units, receivingreports.id"))
            ->leftjoin('inventoryproperties', 'inventoryproperties.rr_id', '=', 'receivingreports.id')
            ->leftjoin('purchaseorder_items', 'receivingreports.po_item_id', '=', 'purchaseorder_items.id')
			->groupBy('receivingreports.id')
			->get();
			
		return $query;	
	}
	
}



?>