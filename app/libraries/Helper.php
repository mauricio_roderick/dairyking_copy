<?php

class Helper{

	public static function string_pad( $value = '')
	{
		$pad_length = 5;
		$pad_string  = '0';
		$pad_type = STR_PAD_LEFT ;
		
		return str_pad($value, $pad_length, $pad_string, $pad_type);
	}

	public static function format_mysql_date($date = null, $timezone = 'Asia/Manila')
	{
		$date = strtotime($date);
		if($date != null)
		{
			$timestamp = $date;
		}
		else
		{
			$timestamp = strtotime('now');
		}
		$current_tz = date_default_timezone_get();
		date_default_timezone_set($timezone);
		$output = date('Y-m-d', $timestamp);
		date_default_timezone_set($current_tz);
		return $output;
	}

	/*
	Converts any string to a MySQL time format.
	If no string is passed, it will return the current time.
	*/
	public static function format_mysql_time($time = null, $timezone = 'Asia/Manila')
	{
		$time = strtotime($time);
		if($time != null)
		{
			$timestamp = $time;
		}
		else
		{
			$timestamp = strtotime('now');
		}
		$current_tz = date_default_timezone_get();
		date_default_timezone_set($timezone);
		$output = date('H:i:s', $timestamp);
		date_default_timezone_set($current_tz);
		return $output;
	}

	/*
	Converts any string to a MySQL datetime format.
	If no string is passed, it will return the current date.
	*/
	public static function format_mysql_datetime($datetime = null, $timezone = 'Asia/Manila')
	{
		$datetime =strtotime($datetime);
		if($datetime != null)
		{
			$timestamp = $datetime;
		}
		else
		{
			$timestamp = strtotime('now');
		}
		$current_tz = date_default_timezone_get();
		date_default_timezone_set($timezone);
		$output = date('Y-m-d H:i:s', $timestamp);
		date_default_timezone_set($current_tz);
		return $output;
	}

	/*
	Converts any string to a custom date format, often used for front-end display.
	*/
	public static function format_date($date = 'now', $format = '', $timezone = 'Asia/Manila')
	{
		$date = strtotime($date);
		if($date != "") {
			if($format == '')
			{
				$format = 'M d, Y';
			}
			
			$current_tz = date_default_timezone_get();
			if($timezone == '')
			{
				$timezone = $current_tz;
			}
			date_default_timezone_set($timezone);
			$output = date($format, $date);
			date_default_timezone_set($current_tz);
			return $output;
		}
		else
		{
			return "n/a";
		}
	}

	/*
	Converts any string to a custom time format, often used for front-end display.
	*/
	public static function format_time($time = 'now', $format = '', $timezone = 'Asia/Manila')
	{
		$time = strtotime($time);
		if ($time != "") {
			if($format == '')
			{
				$format = 'h:i A';
			}
			
			$current_tz = date_default_timezone_get();
			if($timezone == '')
			{
				$timezone = $current_tz;
			}
			date_default_timezone_set($timezone);
			$output = date($format, $time);
			date_default_timezone_set($current_tz);
			return $output;
		}
		else 
		{
			return "n/a";
		}
	}

	/*
	Converts any string to a custom datetime format, often used for front-end display.
	*/
	public static function format_datetime($datetime = 'now', $format = '', $timezone = 'Asia/Manila')
	{
		$datetime = strtotime($datetime);
		if ($datetime != "") {
			if($format == '')
			{
				$format = 'M d, Y h:i A';
			}
			
			$current_tz = date_default_timezone_get();
			if($timezone == '')
			{
				$timezone = $current_tz;
			}
			date_default_timezone_set($timezone);
			$output = date($format, $datetime);
			date_default_timezone_set($current_tz);
			return $output;
		}
		else 
		{
			return "n/a";
		}
	}
}