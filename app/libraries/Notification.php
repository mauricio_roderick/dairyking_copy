<?php

class Notification{

    public static function write( $message = '', $type = '' ){
        Session::put('msg_content', $message);
        Session::put('msg_type', $type);
    }
	
    public static function clear(){
		Session::forget('msg_content');
		Session::forget('msg_type');
    }

    public static function display(){
		$message = Session::get('msg_content');
		$type = Session::get('msg_type');
		
		if($message)
		{
		return <<<EOT
			<div class="alert alert-{$type} fade in">
				<a class="close" data-dismiss="alert" href="#">&times;</a>
				<div class="center">{$message}</div>
			</div>
EOT;
		}
		else
		{
			return '';
		}
	}

}