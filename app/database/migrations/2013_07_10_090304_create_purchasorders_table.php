<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchasordersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('purchaseorders', function($table)
		{
		    $table->increments('id');
		    $table->integer('pr_id');
		    $table->integer('sp_id');
		    $table->date('date');
		    $table->string('remarks');
		    $table->integer('prepared_by');
		    $table->integer('requested_by');
		    $table->integer('approved_by');
		    $table->string('fax_received');
		    $table->string('ordered');
		    $table->string('paid');
		    $table->string('status');
		    $table->string('approved');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}