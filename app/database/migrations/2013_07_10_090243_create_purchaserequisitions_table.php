<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchaserequisitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
			Schema::create('purchaserequisitions', function($table)
		{
		    $table->increments('id');
		    $table->date('date');
		    $table->string('addressed_to');
		    $table->integer('loc_id');
		    $table->integer('requested_by');
		    $table->integer('approved_by');
		    $table->string('status');
		    $table->string('approved');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}