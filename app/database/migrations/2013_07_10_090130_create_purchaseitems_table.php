<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchaseitemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('purchaseitems', function($table)
		{
		    $table->increments('id');
		    $table->integer('po_id');
		    $table->integer('pr_id');
		    $table->integer('inv_id');
		    $table->string('quantity');
			$table->integer('counter');
		    $table->string('unit');
		    $table->string('unitofuse');
		    $table->decimal('multiplier',10,2);
		    $table->string('unitofdelivery');
		    $table->string('unit_price');
		    $table->string('amount');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}