<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductionOrderItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('production_order_items', function($table)
		{
			$table->increments('id');
			$table->integer('prod_ord_id');
			$table->integer('inv_prop_id');
			$table->decimal('quantity', 10, 2);
			$table->integer('unitofuse');
			$table->integer('batch_id');
			$table->datetime('batch_datetime');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}