<?php

use Illuminate\Database\Migrations\Migration;

class CreateReceivingreportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('receivingreports', function($table)
		{
		    $table->increments('id');
		    $table->integer('po_item_id');
		    $table->integer('po_id');
		    $table->integer('quantity');
		    $table->decimal('available_quantity_to_use',10,2);
		    $table->decimal('multiplier',10,2);
		    $table->string('unitofdelivery');
		    $table->string('unitofuse');
		    $table->integer('from_location');
		    $table->integer('from_sub_location');
		    $table->integer('to_location');
		    $table->integer('to_sub_location');
		    $table->date('datereceived');
		    $table->integer('received_by');
		    $table->time('timereceived');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}