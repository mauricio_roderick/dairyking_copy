<?php

use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('suppliers', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->text('cat_id');
		    $table->string('address');
		    $table->string('description');
		    $table->string('contactperson');
		    $table->string('contactnumber');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}