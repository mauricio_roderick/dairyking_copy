<?php

use Illuminate\Database\Migrations\Migration;

class CreateFinishGoodItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('finish_good_items', function($table)
		{
			$table->increments('id');
			$table->integer('fgd_id');
			$table->integer('inv_id');
			$table->decimal('quantity', 10,2);
			$table->string('unit');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}