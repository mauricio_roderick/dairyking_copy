<?php

use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('logs', function($table)
		{
		    $table->increments('id');
		    $table->integer('user_id');
		    $table->string('url');
		    $table->string('request');
		    $table->string('ip');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}