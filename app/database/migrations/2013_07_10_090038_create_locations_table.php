<?php

use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('locations', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->integer('parent');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}