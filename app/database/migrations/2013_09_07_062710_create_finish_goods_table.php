<?php

use Illuminate\Database\Migrations\Migration;

class CreateFinishGoodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('finish_goods', function($table)
		{
			$table->increments('id');
			$table->integer('prod_ord_id');
			$table->integer('inv_id');
			$table->decimal('quantity', 10, 2);
			$table->string('unit_of_use');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}