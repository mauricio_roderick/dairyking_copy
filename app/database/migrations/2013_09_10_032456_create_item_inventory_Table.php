<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('item_inventories', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->text('description');
		    $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}