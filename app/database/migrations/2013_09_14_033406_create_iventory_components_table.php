<?php

use Illuminate\Database\Migrations\Migration;

class CreateIventoryComponentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory_components', function($table)
		{
		    $table->increments('id');
		    $table->integer('inventory');
		    $table->integer('parent');
		    $table->integer('inv_id');
			$table->decimal('quantity', 10,2);
			$table->string('unit');
			$table->decimal('multiplier', 10,2);
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}