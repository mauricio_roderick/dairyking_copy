<?php

use Illuminate\Database\Migrations\Migration;

class CreateCustomerItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_items', function($table)
		{
		    $table->increments('id');
		    $table->integer('customer_id');
		    $table->integer('item_id');
		    $table->integer('quantity');
		    $table->text('comments');
		    $table->date('date');
		    $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}