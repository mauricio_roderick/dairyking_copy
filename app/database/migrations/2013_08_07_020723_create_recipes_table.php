<?php

use Illuminate\Database\Migrations\Migration;

class CreateRecipesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipes', function($table)
		{
			$table->increments('id');
			$table->integer('finish_prod_id');
			$table->integer('multiplier');
			$table->text('unit_of_use');
			$table->text('description');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}