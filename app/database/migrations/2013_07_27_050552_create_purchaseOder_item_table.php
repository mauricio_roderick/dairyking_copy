<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOderItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchaseorder_items', function($table)
		{
			$table->increments('id');
			$table->integer('po_id');
			$table->integer('pr_item_id');
			$table->string('unit', 100);
			$table->integer('quantity');
			$table->decimal('unit_price', 10,2);
			$table->decimal('encoded_total_price', 10,2);
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}