<?php

use Illuminate\Database\Migrations\Migration;

class CreateSuppliercategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliercategories', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}