<?php

use Illuminate\Database\Migrations\Migration;

class CreateInventoryTransferReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory_transfer_reports', function($table)
		{
			$table->increments('id');
			$table->integer('from_inv_prop');
			$table->integer('from_location');
			$table->integer('from_sub_location');
			$table->integer('to_inv_prop');
			$table->integer('to_location');
			$table->integer('to_sub_location');
			$table->decimal('quantity', 10,2);
			$table->integer('inv_id');
			$table->string('unit');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}