<?php

use Illuminate\Database\Migrations\Migration;

class CreateInventorypropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('inventoryproperties', function($table)
		{
		    $table->increments('id');
		    $table->integer('rr_id');
		    $table->integer('inv_id');
		    $table->string('quantityofdelivery');
		    $table->decimal('multiplier',10,2);
		    $table->string('unitofdelivery');
		    $table->integer('available_quantity_to_use');
		    $table->string('unitofuse');
		    $table->integer('location');
		    $table->integer('sub_location');
		    $table->integer('warning');
		    $table->integer('lastuser');
		    $table->integer('batch_id');
		    $table->datetime('datereceived');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}