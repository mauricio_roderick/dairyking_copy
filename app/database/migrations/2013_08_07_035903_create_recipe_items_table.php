<?php

use Illuminate\Database\Migrations\Migration;

class CreateRecipeItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recipe_items', function($table)
		{
			$table->increments('id');
			$table->integer('rcp_id');
			$table->string('type');
			$table->integer('parent');
			$table->integer('inv_id');
			$table->decimal('quantity', 10,2);
			$table->decimal('multiplier', 10,2);
			$table->string('unit');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}