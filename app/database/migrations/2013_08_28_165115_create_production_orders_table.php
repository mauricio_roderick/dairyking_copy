<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductionOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('production_orders', function($table)
		{
			$table->increments('id');
			$table->integer('loc_id');
			$table->integer('sub_loc_id');
		    $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}