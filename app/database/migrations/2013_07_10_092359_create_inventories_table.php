<?php

use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
	Schema::create('inventories', function($table)
		{
		    $table->increments('id');
		    $table->string('name');
		    $table->string('description');
		    $table->string('type');
		    $table->decimal('multiplier',10,2);
		    $table->string('unitofdelivery');
		    $table->string('unitofuse');
		    $table->string('supplier');
		    $table->string('category');
		    $table->integer('lastuser');
		    $table->timestamps();

		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}