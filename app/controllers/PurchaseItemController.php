<?php

class PurchaseItemController extends BaseController {

	public function __construct()
	{
	
	}
	
	public function anyIndex()
	{	
		$params['purchaseitems'] = Purchaseitem::all();
		return View::make('purchase-item.index')->with($params);
	}

	public function anyCreate()
	{
		if(Input::has('submit'))
		{
			$purchaseitem = Input::all();
			$rules = array
						(
							'po_id' => 'required|numeric', 
							'pr_id' => 'required|numeric', 
							'inv_id' => 'required|numeric', 
							'quantity' => 'required|numeric', 
							'unit' => 'required', 
							'unit_price' => 'required|numeric', 
							'amount' => 'required|numeric' 
						);
						
			$v = Validator::make($purchaseitem, $rules);
			if($v->fails())
			{
				Input::flash();
				return Redirect::to(URL::current())->withErrors($v)->withInput();
			}
			
			$purchaseitem = new Purchaseitem;
			$purchaseitem->po_id = Input::get("po_id");
			$purchaseitem->pr_id = Input::get("pr_id");
			$purchaseitem->inv_id = Input::get("inv_id");
			$purchaseitem->quantity = Input::get("quantity");
			$purchaseitem->unit = Input::get("unit");
			$purchaseitem->unit_price = Input::get("unit_price");
			$purchaseitem->amount = Input::get("amount");
		
			$purchaseitem->save();
			
			Notification::write('New purchase item created.');
			return Redirect::to('admin/pi');
		}
		
		$params['purchase_reqs'] = PurchaseRequisition::all();
		$params['purchase_ords'] = $this->purchaseorders_array();
		$params['inventories'] = Inventory::all();
		return View::make('purchase-item.create')->with($params);
		
	}

	public function anyEdit($id = 0)
	{
		$purchaseitem = Purchaseitem::find($id);
		
		if(! $purchaseitem)
		{
			Notification::write('Purchase item not found.');	
			return Redirect::to('admin/pi');
		}
		
		if(Input::has('submit'))
		{
			$post = Input::all();
			
			$rules = array
						(
							'po_id' => 'required|numeric', 
							'pr_id' => 'required|numeric', 
							'inv_id' => 'required|numeric', 
							'quantity' => 'required|numeric', 
							'unit' => 'required', 
							'unit_price' => 'required|numeric', 
							'amount' => 'required|numeric'  
						);
						
			$v = Validator::make($post, $rules);
			
			if($v->passes())
			{
				$purchaseitem->po_id = Input::get("po_id");
				$purchaseitem->pr_id = Input::get("pr_id");
				$purchaseitem->inv_id = Input::get("inv_id");
				$purchaseitem->quantity = Input::get("quantity");
				$purchaseitem->unit = Input::get("unit");
				$purchaseitem->unit_price = Input::get("unit_price");
				$purchaseitem->amount = Input::get("amount");
			
				$purchaseitem->save();
				
				Notification::write('Purchase item updated.');
				return Redirect::to('admin/pi');
			}
			else
			{
				$params['purchaseitem'] = $post;
				$params['errors'] = $v->messages();
			}
		}
		else
		{
			$params['purchaseitem'] = $purchaseitem;
		}
		
		$params['purchase_ords'] = $this->purchaseorders_array();
		$params['purchase_reqs'] = PurchaseRequisition::all();
		$params['inventories'] = Inventory::all();
		
		return View::make('purchase-item.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$purchaseitem = Purchaseitem::find($id);
		
		if(! $purchaseitem)
		{
			Notification::write('Purchase item not found.');	
			return Redirect::to('admin/pi');
		}
		
		$params['purchaseitem'] = $purchaseitem;
		
		return View::make('purchase-item.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$purchaseitem = Purchaseitem::find($id);
		
		if($purchaseitem)
		{
			$purchaseitem->delete();
			Notification::write('Purchase item deleted.');	
		}
		else
		{
			Notification::write('Purchase item not found.');
		}
		
		return Redirect::to('admin/pi');
	}	
	
	public function purchaseorders_array($id = 0)
	{
		$purchaseorders = Purchaseorder::all();
		$return = array();
		
		foreach($purchaseorders as $pos)
		{
			$return[$pos->pr_id][$pos->id] = Helper::string_pad($pos->id);
		}
		
		return $return;
	}	
}