<?php

class ProductionOrderController extends BaseController {

	public function __construct()
	{	
		$this->invalid_item = 0;
		$this->item_error = array();
		$this->inv_prop_error = array();
		$this->prod_order_item_insert = array();
		$this->prod_order_items = array();
		$this->prod_order_item_ids = array();
		$this->inventory_list = array();
	}
	
	public function anyIndex()
	{	
		$params['production_orders'] = ProductionOrders::all();
		return View::make('production-order.index')->with($params);
	}

	public function anyCreate()
	{
		$params['recipe_raw_materials'] = array();
		$params['item_error'] = '';
		$params['inv_prop_error'] = '';
		$params['inventories'] =  array('0' => '- Select -');
		$params['inventory_list'] =  $params['sub_locations'] =  array();
		
		$locations =  Location::all();
		$params['locations'][''] = '- Select -';
		foreach($locations as $location)
		{
			if($location->parent == 0)
			{
				$params['locations'][$location->id] = $location->name;
			}
			else
			{
				$params['sub_locations'][$location->parent][$location->id] = $location->name;
			}
		}
		
		foreach(Inventory::whereRaw("type = 'Work in Process' OR type = 'Finish Product'")->get() as $inventory)
		{
			if($inventory)
			{
				$params['inventories'][$inventory->id] = $inventory->name.' ('.$inventory->unitofdelivery.')';
				$params['inventory_list'][$inventory->id] = $inventory;
				$this->inventory_list[$inventory->id] = $inventory;
			}
		}
		
		if($post = Input::all())
		{
			$rules = array
						(
							'loc_id' => 'required|numeric',
						);
						
			$v = Validator::make($post, $rules);
			if($v->fails())
			{
				$params['errors'] =  $v->messages();
			}
			
			$this->validate_production_order_items();
			
			if(count($this->item_error) == 0)
			{
				$params['recipe_raw_materials'] = $this->get_recipe_raw_materials();
				
				$this->validate_inventory_properties();
				
				if(count($this->inv_prop_error) == 0 && $v->passes())
				{
					$current_datetime = Helper::format_mysql_datetime();
					
					$production_order = array('loc_id' => $post['loc_id'], 'sub_loc_id' => $post['sub_loc_id'], 'created_at' => $current_datetime, 'updated_at' => $current_datetime);
					$prod_order_id = DB::table('production_orders')->insertGetId($production_order);
					
					foreach($this->selected_inv_prop as $selected_inv_prop)
					{
						$additional_columns = array('updated_at' => $current_datetime);
						
						DB::table('inventoryproperties')
						->where('id', $selected_inv_prop['inv_prop_id'])
						->decrement('available_quantity_to_use', $selected_inv_prop['quantity'], $additional_columns);
						
						$prod_ord_item = new ProductionOrderItems;
						$prod_ord_item->prod_ord_id = $prod_order_id;
						$prod_ord_item->quantity = $selected_inv_prop['quantity'];
						$prod_ord_item->inv_prop_id = $selected_inv_prop['inv_prop_id'];
						$prod_ord_item->unitofuse = $selected_inv_prop['unitofuse'];
						$prod_ord_item->batch_id = $selected_inv_prop['batch_id'];
						$prod_ord_item->batch_datetime = $selected_inv_prop['batch_datetime'];
						$prod_ord_item->save();
					}
					
					foreach($this->prod_order_items as $prod_order_item)
					{
						$item = $this->inventory_list[$prod_order_item['inv_id']];
						
						$insert = new FinishGood();
						$insert->prod_ord_id = $prod_order_id;
						$insert->inv_id = $prod_order_item['inv_id'];
						$insert->quantity = $item->multiplier * $prod_order_item['quantity'];
						$insert->unit_of_use = $item->unitofuse;
						$insert->save();
						
						$current_date = date('Y-m-d H:i:s');
						$inventory_prop = array(
													'inv_id' => $prod_order_item['inv_id'],
													'unitofuse' => $item->unitofuse,
													'available_quantity_to_use' => $item->multiplier * $prod_order_item['quantity'],
													'location' => Input::get('loc_id'),
													'sub_location' => Input::get('sub_loc_id'),
													'datereceived' => $current_date,
													'created_at' => $current_date,
													'updated_at' => $current_date,
													);
													
						$inv_prop_id = DB::table('inventoryproperties')->insertGetId($inventory_prop);
						
						DB::table('inventoryproperties')->where('id', $inv_prop_id)->update(array('batch_id' => $inv_prop_id));
					}
					
					Notification::write('Production order created.');
					return Redirect::to('admin/production-order');
				}
				else
				{
					$params['inv_prop_error'] = (count($this->inv_prop_error) > 0) ? '<div>'.implode($this->inv_prop_error,'</div><div>').'</div>' : '';
				}
			}
			else
			{
				$params['item_error'] = (count($this->item_error) > 0) ? '<small class="error">'.implode($this->item_error,'</small><small class="error">').'</small>' : '';
			}
		}
		
		$params['prod_order_items'] = $this->prod_order_items;
		return View::make('production-order.create')->with($params);
	}
	
	public function anyView($id = 0)
	{
		$production_order = ProductionOrders::find($id);
		
		if(! $production_order)
		{
			Notification::write('Production order not found.');	
			return Redirect::to('admin/production-order');
		}
		
		$params['production_order'] = $production_order;
		$params['finishgoods'] = FinishGood::where('prod_ord_id', $id)->get();
		
		return View::make('production-order.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$production_order = ProductionOrders::find($id);
		
		if($production_order)
		{
			FinishGood::where('prod_ord_id', $id)->delete();
			ProductionOrderItems::where('prod_ord_id', $id)->delete();
			$production_order->delete();
			Notification::write('Production order deleted.');	
		}
		else
		{
			Notification::write('Production order not found.');
		}
		
		return Redirect::to('admin/production-order');
	}
	
	public function anyCancel($id = 0)
	{
		$production_order = ProductionOrders::find($id);
		
		if($production_order)
		{
			$productionorderitems = ProductionOrderItems::where('prod_ord_id', $id)->get();
			
			if($productionorderitems->count())
			{
				foreach($productionorderitems as $prod_ord_item)
				{
					$inventoryproperty = InventoryProperty::find($prod_ord_item->inv_prop_id);
					$current_datetime = Helper::format_mysql_datetime();
					
					if($inventoryproperty)
					{
						$additional_columns = array('updated_at' => $current_datetime);
						
						DB::table('inventoryproperties')
						->where('id', $prod_ord_item->inv_prop_id)
						->increment('available_quantity_to_use', $prod_ord_item->quantity, $additional_columns);
					}
					else
					{
						$return_inventory_prop = new InventoryProperty();
						$return_inventory_prop->inv_id = $prod_ord_item->inv_id;
						$return_inventory_prop->batch_id = $prod_ord_item->batch_id;
						$return_inventory_prop->datereceived = $prod_ord_item->batch_datetime;
						$return_inventory_prop->available_quantity_to_use = $prod_ord_item->quantity;
						$return_inventory_prop->unitofuse = $prod_ord_item->unitofuse;
						$return_inventory_prop->save();
					}
				}
			}
			
			FinishGood::where('prod_ord_id', $id)->delete();
			$production_order->delete();
			Notification::write('Production order cancelled.');	
		}
		else
		{
			Notification::write('Production order not found.');
		}
		
		return Redirect::to('admin/production-order');
	}
	
	public function validate_production_order_items()
	{
		$prod_order_items = Input::get("prod_order_item");
		$quantity = Input::get("quantity");
		
		$invalid_item = 0;
		$invalid_quantity = 0;
		$null_quantity = 0;
		
		if($prod_order_items)
		{
			foreach($prod_order_items as $index => $id)
			{
				if(isset($this->inventory_list[$id]))
				{
					$this->prod_order_items[$index] = array(
									'inv_id' => $id,
									'quantity' => $quantity[$index],
									);
				
					$this->prod_order_item_ids[$index] = $id;
					
					if($quantity[$index] == '')
					{
						$null_quantity++;
					}
					else
					{
						if(! is_numeric($quantity[$index]))
						$invalid_quantity++;
					}
				}
				else
				{
					$invalid_item++;
				}
				
			}
		}
		
		if(count($this->prod_order_items) == 0)
		$this->item_error[] = 'Production item is required.';
		
		if($invalid_item > 0)
		$this->item_error[] = 'Invalid inventory found.';
		
		if($null_quantity > 0)
		$this->item_error[] = 'All inventory quantity fields are required.';
		
		if($invalid_quantity > 0)
		$this->item_error[] = 'Quantity fields must be in numeric value.';
	}
	
	public function get_recipe_raw_materials()
	{
		$components = InventoryComponents::whereIn('inventory', $this->prod_order_item_ids)->get();
		
		$return = $this->recipe_inv_prop_ids = array();
		foreach($components as $component)
		{
			$component_index = array_search($component->inventory, $this->prod_order_item_ids);
			
			if(isset($return[$component->inv_id][$component->unit]))
			{
				$return[$component->inv_id][$component->unit] += $component->quantity * $this->prod_order_items[$component_index]['quantity'];
			}
			else
			{
				$return[$component->inv_id][$component->unit] = $component->quantity * $this->prod_order_items[$component_index]['quantity'];
			}
			
			$this->required_inv_prop_ids[$component->inv_id] = $component->inv_id;
		}
		
		return $return;
	}
	
	public function validate_inventory_properties()
	{
		$this->selected_inv_prop = array();
		$inv_properties = Input::get('inv_prop');
		$post = Input::get();
		
		$invalid_inv_prop = 0;
		$invalid_quantity = 0;
		$exceed_quantity = 0;
		$null_quantity = 0;
		
		if($inv_properties)
		{
			foreach($inv_properties as $index => $inv_property)
			{
				$confirm_inv_property = InventoryProperty::find($inv_property);
				
				if($confirm_inv_property)
				{
					unset($this->required_inv_prop_ids[$confirm_inv_property->inv_id]);
					
					$quatity_field_name = 'inv_prop_quantity_'.$inv_property;
					
					$this->selected_inv_prop[] = array(
									'inv_prop_id' => $inv_property,
									'quantity' => $post[$quatity_field_name],
									'unitofuse' => $confirm_inv_property->unitofuse,
									'batch_id' => $confirm_inv_property->batch_id,
									'batch_datetime' => $confirm_inv_property->datereceived,
									);
					
					
					if(isset($post[$quatity_field_name]) && $post[$quatity_field_name] == '')
					{
						$null_quantity++;
					}
					else
					{
						if(! is_numeric($post[$quatity_field_name]))
						{
							$invalid_quantity++;
						}
						else
						{
							if($post[$quatity_field_name] > $confirm_inv_property->available_quantity_to_use)
							{
								$batch = date('Ymd', strtotime($confirm_inv_property->datereceived))."-".Helper::string_pad($confirm_inv_property->id);
								$this->inv_prop_error[] = 'Not enough stock for '.$confirm_inv_property->inv->name.' batch '.$batch.'.';
							}
						}
					}
				}
				else
				{
					$invalid_inv_prop++;
				}
			}
		}
		
		if(count($this->selected_inv_prop) == 0)
		{
			$this->inv_prop_error[] = 'Inventory property is required.';
		}
		else
		{
			if(count($this->required_inv_prop_ids) > 0)
			{
				$missing_inventories = Inventory::whereIn('id', $this->required_inv_prop_ids)->get();
				
				$missing_inventory_names = array();
				foreach($missing_inventories as $missing_inventory)
				{
					$missing_inventory_names[] = "'".$missing_inventory->name."'";
				}
				$this->inv_prop_error[] = 'No selected stocks for '.implode(', ', $missing_inventory_names).'.';
			}
		}
		
		if($invalid_inv_prop > 0)
		$this->inv_prop_error[] = 'Invalid inventory property found';
		
		if($null_quantity > 0)
		$this->inv_prop_error[] = 'All inventory property quantity fields are required.';
		
		if($invalid_quantity > 0)
		$this->inv_prop_error[] = 'Quantity fields must be in numeric value.';
		
		
	}
}