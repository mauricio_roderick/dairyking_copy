<?php

class CategoriesController extends BaseController {

	public function getIndex()
	{
		$params = Category::all();
		
		return View::make('category.index')->with('categories', $params);
	}

	public function anyCreate()
	{
		if(Input::has('submit'))
		{
			$category = Input::all();
			$rules = array
						(
							'name' => 'required',
						);
						
			$v = Validator::make($category, $rules);
			if($v->fails())
			{
				return Redirect::to(URL::current())->withErrors($v)->withInput();
			}

			$category = new Category;
			$category->name = Input::get("name");
			
			if(Input::get("parent"))
			$category->parent = Input::get("parent");
			$category->save();
			
			Notification::write('Category created.', 'success');
			return Redirect::to('admin/category');
		}
		else
		{
			$params['categories'] = Category::all();
			return View::make('category.create')->with($params);
		}
	}

	public function anyUpdate($id = 0)
	{
		$category = Category::find($id);
		
		if(! $category)
		{
			Notification::write('Category was not found.', 'alert');
			return Redirect::to('admin/category');
		}
		
		if(Input::has('submit'))
		{
			$params['category'] = Input::all();
			
			$rules = array
						(
							'name' => 'required',
						);
						
			$v = Validator::make($params['category'], $rules);
			if($v->fails())
			{
				Input::flash();
				return Redirect::to(URL::current())->withErrors($v)->with($params);
			}
			
			$category->name = Input::get("name");
			$category->parent = Input::get("parent");
			$category->save();
			
			Notification::write('Category updated.', 'success');
			return Redirect::to('admin/category');
		}
		else
		{
			$params['category'] = $category;
			$params['categories'] = Category::where('id', '!=', $id)->get();
			
			return View::make('category.update')->with($params);
		}
		
	}

	public function getView($id = 0)
	{
		$params['category'] = Category::find($id);
							
		if(! $params['category'])
		{
			Notification::write('Category was not found.', 'alert');
			return Redirect::to('admin/category');
		}
		
		$params['categories'] = Category::all();
		return View::make('category.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$category = Category::find($id);
		
		if($category)
		{
			$category->delete();
			Notification::write('Category deleted.');
			return Redirect::to('admin/category');	
		}
		else
		{
			Notification::write('Category not found.');
		}
		
		return Redirect::to('admin/category');
	}
	
}