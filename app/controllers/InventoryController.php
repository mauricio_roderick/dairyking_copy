<?php

class InventoryController extends BaseController {

	public function __construct()
	{					
		$this->yes_no_opt = array(
									'' => '- Select -',
									'Yes' => 'Yes',
									'No' => 'No'
									);
		
		$this->post_components = array();
		$this->components_error = array();
		$this->components_insert = array();
		$this->components_update = array();
		$this->exempted_ids = array();
		$this->inventory_components_id = array();
		$this->components_list = array();
		
		$this->inventory_types = array(
									'' => '- Select -',
									'Raw Material' => 'Raw Material', 
									'Packaging Material' =>  'Packaging Material', 
									'Work in Process' =>  'Work in Process', 
									'Finish Product' =>  'Finish Product', 
									);
	}
	
	public function anyIndex()
	{	
		$params['inventories'] = Inventory::all();
		return View::make('inventory.index')->with($params);
	}

	public function anyCreate()
	{
		$params['suppliers'] = Supplier::all();
		$params['components_error'] = '';
		$params['categories'] = SupplierCategory::all();
		$params['inventory_types'] = $this->inventory_types;
		
		$components = Inventory::where('type', '!=', 'Finish Product')->get();
		$params['components'] =  array('0' => '- Select -');
		$params['components_list'] =  array();
		foreach($components as $component)
		{
			$params['components'][$component->id] = $component->name;
			
			$params['components_list'][$component->id]['name'] = $component->name;
			$params['components_list'][$component->id]['type'] = $component->type;
			$params['components_list'][$component->id]['unitofuse'] = $component->unitofuse;
		}
		
		$this->components_list = $params['components_list'];
		
		if(Input::has('submit'))
		{
			$inventory = Input::all();
			$rules = array
						(
							'name' => 'required', 
							'description' => 'required', 
							'multiplier' => 'required|numeric', 
							'unitofdelivery' => 'required', 
							'unitofuse' => 'required', 
							'category'	=> 'required|numeric', 
							'type' => 'required', 
						);
			
			if($inventory['type'] == 'Work in Process' || $inventory['type'] == 'Finish Product')
			{
				$this->validateComponents();
			}
			
			$v = Validator::make($inventory, $rules);
			
			if($v->passes()  && count($this->components_error) == 0)
			{
				$current_date = date('Y-m-d H:i:s');
				
				$insert_inventory['name'] = Input::get("name");
				$insert_inventory['description'] = Input::get("description");
				$insert_inventory['multiplier'] = Input::get("multiplier");
				$insert_inventory['unitofdelivery'] = Input::get("unitofdelivery");
				$insert_inventory['unitofuse'] = Input::get("unitofuse");
				$insert_inventory['category'] = Input::get("category");
				$insert_inventory['type'] = Input::get("type");
				$insert_inventory['created_at'] = $current_date;
				$insert_inventory['updated_at'] = $current_date;
				
				$inv_id = DB::table('inventories')->insertGetId($insert_inventory);
				
				if($inventory['type'] == 'Work in Process' || $inventory['type'] == 'Finish Product')
				$this->insert_inventory_components($inv_id);
				
				Notification::write('Inventory created.');
				return Redirect::to('admin/inventory');
			}
			else
			{
				$params['errors'] =  $v->messages();
				$params['components_error'] = (count($this->components_error) > 0) ? '<small class="error">'.implode($this->components_error,'</small><small class="error">').'</small>' : '';
			}
		}
		
		return View::make('inventory.create')->with($params);
	}

	public function anyEdit($inv_id = 0)
	{
		$inventory = Inventory::find($inv_id);
		
		if(! $inventory)
		{
			Notification::write('Inventory not found.');	
			return Redirect::to('admin/inventory');
		}
		
		$params['inventory'] = $inventory;
		$params['suppliers'] = Supplier::all();
		$params['components_error'] = '';
		$params['categories'] = SupplierCategory::all();
		$params['inventory_types'] = $this->inventory_types;
		
		$components = Inventory::where('type', '!=', 'Finish Product')->where('id', '!=', $inv_id)->get();
		$params['components'] =  array('0' => '- Select -');
		$params['components_list'] =  array();
		foreach($components as $component)
		{
			$params['components'][$component->id] = $component->name;
			
			$params['components_list'][$component->id]['name'] = $component->name;
			$params['components_list'][$component->id]['type'] = $component->type;
			$params['components_list'][$component->id]['unitofuse'] = $component->unitofuse;
		}
		$this->components_list = $params['components_list'];
		
		$inventory_components = InventoryComponents::where('inventory', $inv_id)->get();
		$params['inventory_components'] =  array();
		foreach($inventory_components as $index => $component)
		{
			$params['inventory_components'][] = array(
													'id' => $component->id,
													'inv_id' => $component->inv_id,
													'unit' => $component->unit,
													'quantity' => $component->quantity,
													);
			
			$this->inventory_components_id[] = $component->id;
			$this->inventory_components[$component->id] = $component;
		}
		
		if(Input::has('submit'))
		{
			$params['inventory'] = Input::all();
			$rules = array
						(
						'name' => 'required', 
						'description' => 'required', 
						'multiplier' => 'required|numeric', 
						'unitofdelivery' => 'required', 
						'unitofuse' => 'required', 
						'category'	=> 'required|numeric', 
						'type' => 'required',
						);

			if($inventory['type'] == 'Work in Process' || $inventory['type'] == 'Finish Product')
			$this->validateComponents_forUpdate($inv_id);
			
			$v = Validator::make($params['inventory'], $rules);
			
			if($v->passes() && count($this->components_error) == 0)
			{
				$inventory->name = Input::get("name");
				$inventory->description = Input::get("description");
				$inventory->multiplier = Input::get("multiplier");
				$inventory->unitofdelivery = Input::get("unitofdelivery");
				$inventory->unitofuse = Input::get("unitofuse");
				$inventory->category = Input::get("category");
				$inventory->type = Input::get("type");
				$inventory->save();
				
				if($params['inventory']['type'] == 'Work in Process' || $inventory['type'] == 'Finish Product')
				{
					if(count($this->exempted_ids) > 0)
					DB::table('inventory_components')->whereNotIn('id', $this->exempted_ids)->where('inventory', $inv_id)->delete();
					
					$this->insert_inventory_components($inv_id);
					$this->update_inventory_components();
				}
				else
				{
					InventoryComponents::where('inventory', $inv_id)->delete();
				}
				
				Notification::write('Inventory updated.');
				return Redirect::to('admin/inventory');
			}
			else
			{
				$params['inventory_components'] = $this->post_components;
				$params['components_error'] = (count($this->components_error) > 0) ? '<small class="error">'.implode($this->components_error,'</small><small class="error">').'</small>' : '';
				$params['errors'] =  $v->messages();
			}
		}
		
		return View::make('inventory.update')->with($params);
	}

	public function anyView($id = 0)
	{
		$inventory = Inventory::find($id);
		
		if(! $inventory)
		{
			Notification::write('Inventory not found.');	
			return Redirect::to('admin/po');
		}
		
		$params['inventory'] = $inventory;
		
		$categories = SupplierCategory::all();
		$params['categories'] = array();
		
		foreach($categories as $category)
		{
			$params['categories'][$category->id] = $category->name;
		}
		
		return View::make('inventory.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$inventory = Inventory::find($id);
		
		if($inventory)
		{
			$inventory->delete();
			FinishGoodItem::where('fgd_id', $id)->delete(); 
			Notification::write('Inventory deleted.');	
		}
		else
		{
			Notification::write('Inventory not found.');
		}
		
		return Redirect::to('admin/inventory');
	}
	
	public function insert_inventory_components($inv_id = 0)
	{
		foreach($this->components_insert as $index => $item)
		{
			$inventory_component = new InventoryComponents;
			$inventory_component->inventory = $inv_id;
			$inventory_component->inv_id = $item['inv_id'];
			$inventory_component->quantity = $item['quantity'];
			$inventory_component->unit = $this->components_list[$item['inv_id']]['unitofuse'];			
			$inventory_component->save();
			
		}
	}
	
	public function update_inventory_components()
	{
		foreach($this->components_update as $index => $item)
		{
			$inventory_component = InventoryComponents::find($item['id']);			
			$inventory_component->quantity = $item['quantity'];			
			$inventory_component->save();
		}
	}
	
	public function validateComponents()
	{
		$components = Input::get("inventory_component");
		$quantity = Input::get("quantity");
		
		$invalid_quantity = 0;
		$invalid_component = 0;
		
		if($components)
		{
			foreach($components as $index => $component_id)
			{	
				if(isset($this->components_list[$component_id]))
				{
					$this->components_insert[] = array(
									'inv_id' => $component_id,
									'quantity' => $quantity[$index],
									);
					
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
				}
				else
				{
					$invalid_component++;
				}
			}
		}
		
		if(count($this->components_insert) == 0)
		$this->components_error[] = 'Atleast one component is required.';
		
		if($invalid_component > 0)
		$this->components_error[] = 'Invalid component found.';
		
		if($invalid_quantity > 0)
		$this->components_error[] = 'Quantity fields must be in numeric value.';
	}
	
	public function validateComponents_forUpdate()
	{
		$components = Input::get("inventory_component");
		$quantity = Input::get("quantity");
		$inventory_component_id = Input::get("id");
		
		$invalid_component = 0;
		$invalid_quantity = 0;
		
		if($components)
		{
			foreach($components as $index => $component_id)
			{	
				if(isset($this->components_list[$component_id]))
				{
					$inventory_component = array(
											'id' => $inventory_component_id[$index],
											'quantity' => $quantity[$index],
											'inv_id' => $component_id,
											);
											
					$this->post_components[$index] = $inventory_component;
											
					if(in_array($inventory_component_id[$index], $this->inventory_components_id))
					{
						$this->post_components[$index]['unit'] = $this->inventory_components[$inventory_component_id[$index]]->unit; // use item unit not inventory unit
						
						$this->components_update[$index] = $inventory_component;
						
						$this->exempted_ids[] = $inventory_component_id[$index];
					}
					else
					{
						$this->components_insert[$index] = $inventory_component;
					}
						
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
				}
				else
				{
					$invalid_component++;
				}
			}
		}
		
		if(count($this->components_insert) == 0 && count($this->components_update) == 0)
		$this->components_error[] = 'Atleast one component is required.';
		
		if($invalid_quantity > 0)
		$this->components_error[] = 'Quantity fields must be in numeric value.';
	}
	
	
	/* public function insert_inventory_components_($inv_id)
	{
		foreach($this->components_insert as $index => $item)
		{
			$selected_component = $this->components_list[$item['inv_id']];
			
			$inventory_component = array(
							'inventory' => $inv_id,
							'parent' => 0,
							'inv_id' => $item['inv_id'],
							'quantity' => $item['quantity'],
							'unit' => $selected_component['unitofuse'],
							'created_at' => $current_date,
							'updated_at' => $current_date,
							);
			
			$parent_component_id = DB::table('inventory_components')->insertGetId($inventory_component);
			
			if($selected_component['type'] == 'Work in Process')
			{
				$sub_components = InventoryComponents::where('inventory', $item['inv_id'])->get();
				
				if($sub_components->count())
				{
					foreach($sub_components as $sub_component)
					{
						$inventory_component = new InventoryComponents;
						$inventory_component['inventory'] = $inv_id;
						$inventory_component['parent'] = $parent_component_id;
						$inventory_component['inv_id'] = $sub_component->inv_id;
						$inventory_component['quantity'] = $sub_component->quantity * $item['quantity'];
						$inventory_component['unit'] = $sub_component->unit;			
						$inventory_component->save();
					}
				}
			}
		}
	} */
}