<?php

class LocationController extends BaseController {

	public function anyIndex()
	{
		$params['locations'] = Location::all();
		return View::make('location.index')->with($params);
	}

	public function anyCreate()
	{
		$params = array();
		
		if($post = Input::all())
		{
			$location = Input::all();
			$rules = array
						(
							'name' => 'required', 
						);
						
			$v = Validator::make($location, $rules);
			
			if($v->passes())
			{
				$location = new Location;
				$location->name = $post["name"];
				$location->parent = $post["parent"];
				$location->save();
				
				Notification::write('Department created.', 'success');
				return Redirect::to('admin/department');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$params['locations'] = Location::all();
		return View::make('location.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$location = Location::find($id);
		
		$params['location'] = $location;
		
		if($post = Input::all())
		{
			$params['location'] = $post;
			
			$rules = array
						(
							'name' => 'required', 
						);
						
			$v = Validator::make($params['location'], $rules);
			
			if($v->passes())
			{
				$location->name = $post["name"];
				$location->parent = $post["parent"];
				$location->save();
				
				Notification::write('Department updated.', 'success');
				return Redirect::to('admin/department');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$params['locations'] = Location::where('id', '!=', $id)->get();
		return View::make('location.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$params['location'] = Location::find($id);
		if(! $params['location'])
		{
			return Redirect::to('admin/department');
		}
		
		return View::make('location.view')->with($params);
	}
	
	public function anyDelete($id = 0)
	{
		$location = Location::find($id);
		
		if($location)
		{
			$location->delete();
			Notification::write('Location deleted.');
			return Redirect::to('admin/department');	
		}
		else
		{
			Notification::write('Location was not found.');
		}
		
		return Redirect::to('admin/department');
	}
	
}