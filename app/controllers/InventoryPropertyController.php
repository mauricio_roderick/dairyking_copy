<?php

class InventoryPropertyController extends BaseController {
	
	public function __construct()
	{					
		$this->module_title = 'Stock Overview';
		$this->module_name = 'Stock';
	}
	
	public function getIndex()
	{
		$params['module_title'] = $this->module_title;
		$params['module_name'] = $this->module_name;
		$params['inventoryproperties'] = InventoryProperty::all();
		
		return View::make('inventory-property.index')->with($params);
	}

	public function anyCreate()
	{
		$params['rr_dtls'] = '';
		
		if($post = Input::all())
		{
			$max_validation = '';
			
			if($post['rr_id'])
			{ 			
				$receivingreport = ReceivingReport::find($post['rr_id']);
				$po_item = $receivingreport->poItem;
				$pr_item = $po_item ? $po_item->prItem : false;
				$inventory = $pr_item ? $pr_item->inv : false;
				$distributed_qty = DB::table('inventoryproperties')->where('rr_id', $post['rr_id'])->sum('quantityofdelivery');
				
				$distributed_qty = $distributed_qty ? $distributed_qty : 0;
				$remaining_qty = $pr_item->quantity - $distributed_qty;
				
				if($remaining_qty <= 0)
				{
					Notification::write('Ivalid Receiving created.', 'success');
					return Redirect::to(URL::current());
				}
				
				$params['rr_dtls'] .= '<b>RR Details</b><table>';
				$params['rr_dtls'] .= '<tr><th>PO Id</th><th>PR Item Id</th><th>Inventory</th><th>Unit</th><th>Quantity</th><th>Distributed</th><th>Remaining</th></tr>';
				$params['rr_dtls'] .= '<tr><td>'.Helper::string_pad($receivingreport->po_id).'</td>';
				$params['rr_dtls'] .= $pr_item ? '<td>'. Helper::string_pad($pr_item->id).'</td>' : '<td></td>' ;
				$params['rr_dtls'] .= $inventory ? '<td>'. $inventory->name.'</td>' : '<td></td>' ;
				$params['rr_dtls'] .= $pr_item ? '<td>'. $pr_item->unit .'</td>' : '<td></td>' ;
				$params['rr_dtls'] .= $pr_item ? '<td>'. $pr_item->quantity .'</td>' : '<td></td>' ;
				$params['rr_dtls'] .= '<td>' . $distributed_qty . '</td>' ;
				$params['rr_dtls'] .= '<td>' . $remaining_qty . '</td>' ;
				$params['rr_dtls'] .= '</table>';
				
				$max_validation .= '|max:'.$remaining_qty;
			}
			
			$rules = array
						(
							'rr_id' => 'required|numeric',
							'quantityofdelivery' => 'required|numeric|min:1'.$max_validation,
							'loc_id' => 'required|numeric',
							'datereceived' => 'required|date',
						);
						
			$v = Validator::make($post, $rules);
			
			if($v->passes())
			{
				if(! isset($inventory) && ! $inventory)
				Notification::write('Inventory not found.', 'success');
				
				$inventoryproperty = new InventoryProperty;
				$inventoryproperty->rr_id = $post['rr_id'];
				$inventoryproperty->inv_id = $inventory->id;
				$inventoryproperty->quantityofdelivery = $post['quantityofdelivery'];
				$inventoryproperty->loc_id = $post['loc_id'];
				$inventoryproperty->datereceived = $post['datereceived'];
				$inventoryproperty->save();
				
				Notification::write($this->module_name.' created.', 'success');
				return Redirect::to('admin/inventory-property');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
			
		}

		$params['receivingreports'] = ReceivingReport::get_all_width_total_distributed();
		$params['locations'] = Location::all();
		$params['module_name'] = $this->module_name;
		return View::make('inventory-property.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$inventoryproperty = InventoryProperty::find($id);
		
		if(! $inventoryproperty)
		{
			Notification::write($this->module_name.' was not found.', 'alert');
			return Redirect::to('admin/inventory-property');
		}
		
		$params['inventoryproperty'] = $inventoryproperty;
		$params['inventoryproperty_orig'] = $inventoryproperty;
		
		if($post = Input::all())
		{
			$params['inventoryproperty'] = $post;
			
			$rules = array
						(
							'location' => "required|numeric",
							'sub_location' => 'numeric'
						);
						
			$v = Validator::make($post, $rules);
			
			if($v->passes())
			{
				$current_date = date('Y-m-d H:i:s');
				
				$rr = $params['inventoryproperty_orig']->rr;
				/* if($inventoryproperty->location != $post['location'] || $inventoryproperty->sub_location != $post['sub_location'])
				{
					$receivingreport = new ReceivingReport();
					$receivingreport->po_id = $rr ? $rr->po_id : '';
					$receivingreport->po_item_id = $rr ? $rr->po_item_id : '';
					$receivingreport->quantity = $params['inventoryproperty_orig']->quantityofdelivery;
					$receivingreport->available_quantity_to_use = $params['inventoryproperty_orig']->available_quantity_to_use;
					$receivingreport->multiplier = $params['inventoryproperty_orig']->multiplier;
					$receivingreport->unitofdelivery = $params['inventoryproperty_orig']->unitofdelivery;
					$receivingreport->unitofuse = $params['inventoryproperty_orig']->unitofuse;
					$receivingreport->from_location = $params['inventoryproperty_orig']->location;
					$receivingreport->from_sub_location = $params['inventoryproperty_orig']->sub_location;
					$receivingreport->to_location = $post['location'];
					$receivingreport->to_sub_location = $post['sub_location'];
					$receivingreport->datereceived = $current_date;
					$receivingreport->save();
				} */
				
				$inventoryproperty->location = $post['location'];
				$inventoryproperty->sub_location = $post['sub_location'];
				$inventoryproperty->save();
				
				Notification::write($this->module_name.' updated.', 'success');
				return Redirect::to('admin/inventory-property');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$locations = Location::all();
		$params['locations_array'] = array('' => '- Department -');
		$params['sub_locations'] = array();
		foreach($locations as $location)
		{
			if($location->parent == 0)
			{
				$params['locations_array'][$location->id] = $location->name;
			}
			else
			{
				$params['sub_locations'][$location->parent][$location->id] = $location->name;
			}
		}
		
		$params['module_name'] = $this->module_name;
		return View::make('inventory-property.update')->with($params);
		
	}

	public function getView($id = 0)
	{
		$params['inventoryproperty'] = InventoryProperty::find($id);
							
		if(! $params['inventoryproperty'])
		{
			Notification::write($this->name.' not found.', 'alert');
			return Redirect::to('admin/inventory-property');
		}
		
		$params['module_name'] = $this->module_name;
		return View::make('inventory-property.view')->with($params);
	}

	public function anyTransfer($id = 0)
	{
		$inventoryproperty = InventoryProperty::find($id);
							
		if(! $inventoryproperty)
		{
			Notification::write($this->module_name.' not found.', 'alert');
			return Redirect::to('admin/inventory-property');
		}
		$params['inventoryproperty'] = $inventoryproperty;
		
		if($post = Input::all())
		{
			$params['post'] = $post;
			
			$rules = array
						(
							'location' => "required|numeric",
							'sub_location' => 'numeric',
							'quantity' => 'required|integer|min:0|max:'.$inventoryproperty->available_quantity_to_use,
						);
						
			$v = Validator::make($post, $rules);
			
			if($v->passes())
			{
				$current_date = date('Y-m-d H:i:s');
				
				$existing_inv_prop = InventoryProperty::where('batch_id', $inventoryproperty->batch_id)
									->where('datereceived', $inventoryproperty->datereceived)
									->where('inv_id', $inventoryproperty->inv_id)
									->where('location', $post['location'])
									->where('sub_location', $post['sub_location'])
									->first();
				
				if($existing_inv_prop)
				{
					DB::table('inventoryproperties')->where('id', $existing_inv_prop->id)->increment('available_quantity_to_use', $post['quantity']);
					$inv_prop_id = $existing_inv_prop->id;
				}
				else
				{
					$new_inv_prop = array();
					$new_inv_prop['inv_id'] = $inventoryproperty->inv_id;
					$new_inv_prop['location'] = $post['location'];
					$new_inv_prop['sub_location'] = $post['sub_location'];
					$new_inv_prop['available_quantity_to_use'] = $post['quantity'];
					$new_inv_prop['unitofuse'] = $inventoryproperty->unitofuse;
					$new_inv_prop['batch_id'] = $inventoryproperty->batch_id;
					$new_inv_prop['datereceived'] = $inventoryproperty->datereceived;
					
					$inv_prop_id = DB::table('inventoryproperties')->insertGetId($new_inv_prop);
				}
				
				$transfer_report = new InventoryTransferReport();
				$transfer_report->from_inv_prop = $id;
				$transfer_report->from_location = $inventoryproperty->location;
				$transfer_report->from_sub_location = $inventoryproperty->sub_location;
				$transfer_report->to_inv_prop = $inv_prop_id;
				$transfer_report->to_location = $post['location'];
				$transfer_report->to_sub_location = $post['sub_location'];
				$transfer_report->quantity = $post['quantity'];
				$transfer_report->inv_id = $inventoryproperty->inv_id;
				$transfer_report->unit = $inventoryproperty->unitofuse;
				$transfer_report->save();
				
				$inventoryproperty->available_quantity_to_use = $inventoryproperty->available_quantity_to_use - $post['quantity'];
				$inventoryproperty->save();
				
				Notification::write($this->module_name.' transferred.', 'success');
				return Redirect::to('admin/inventory-property');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$locations = Location::all();
		$params['locations_array'] = array('' => '- Department -');
		$params['sub_locations'] = array();

		foreach($locations as $location)
		{
			if($location->parent == 0)
			{
				//if($inventoryproperty->location != $location->id)
				$params['locations_array'][$location->id] = $location->name;
			}
			else
			{
				if($inventoryproperty->sub_location != $location->id)
				$params['sub_locations'][$location->parent][$location->id] = $location->name;
			}
		}
		
		return View::make('inventory-property.transfer')->with($params);
	}
	
	public function anyDelete($id = 0)
	{
		$inventoryproperty = InventoryProperty::find($id);
		
		if($inventoryproperty)
		{
			$inventoryproperty->delete();
			Notification::write($this->name.' deleted.');
			return Redirect::to('admin/inventory-property');	
		}
		else
		{
			Notification::write($this->name.' not found.', 'error');
		}
		
		return Redirect::to('admin/inventory-property');
	}
	
	function postGetRrDetails()
	{
		$rr_id = Input::get('rr_id');
		$ip_id = Input::get('ip_id');
		
		$receivingreport = ReceivingReport::find($rr_id);
		
		$po_item = $receivingreport->poItem;
		$pr_item = $po_item ? $po_item->prItem : false;
		$inventory = $pr_item ? $pr_item->inv : false;
		
		if($ip_id)
		{
			$distributed_qty = DB::table('inventoryproperties')->where('rr_id', $rr_id)->sum('quantityofdelivery');
		}
		else
		{
			$distributed_qty = DB::table('inventoryproperties')->where('rr_id', $rr_id)->where('id', '!=', $ip_id)->sum('quantityofdelivery');
		}
		
		$distributed_qty = $distributed_qty ? $distributed_qty : 0;
		$remaining_qty = $pr_item->quantity - $distributed_qty;
		
		$rr_dtls = '<b>RR Details</b><table>';
		$rr_dtls .= '<tr><th>PO Id</th><th>PR Item Id</th><th>Inventory</th><th>Conversion</th><th>Quantity</th><th>Distributed</th><th>Remaining</th></tr>';
		$rr_dtls .= '<tr><td>'.Helper::string_pad($receivingreport->po_id).'</td>';
		$rr_dtls .= $pr_item ? '<td>'. Helper::string_pad($pr_item->id).'</td>' : '<td></td>' ;
		$rr_dtls .= $inventory ? '<td>'. $inventory->name.'</td>' : '<td></td>' ;
		$rr_dtls .= $pr_item ? '<td>'. $pr_item->unitofdelivery .' = '. $pr_item->multiplier. ' '. $pr_item->unitofuse .'</td>' : '<td></td>' ;
		$rr_dtls .= $pr_item ? '<td>'. $pr_item->quantity.'</td>' : '<td></td>' ;
		$rr_dtls .= '<td>' . $distributed_qty . '</td>' ;
		$rr_dtls .= '<td>' . $remaining_qty . '</td>' ;
		$rr_dtls .= '</table>';
		
		
		return $rr_dtls;
	}
}