<?php

class SupplierController extends BaseController {

	public function anyIndex()
	{
		$categories = SupplierCategory::all();
		
		foreach($categories as $category)
		{
			$params['categories'][$category->id] = $category->name;
		}
		
		$params['suppliers'] = Supplier::all();
		return View::make('suppliers.index')->with($params);
	}

	public function anyCreate()
	{
		if(Input::has('submit'))
		{
			$supplier = Input::all();
			$rules = array
						(
							'name' => 'required', 
							'address' => 'required', 
							'description' => 'required', 
							'contactperson'	=> 'required', 
							'contactnumber'	=> 'required', 
							'cat_id' => 'required', 
						);
						
			$v = Validator::make($supplier, $rules);
			if($v->fails())
			{
				Input::flash();
				return Redirect::to(URL::current())->withErrors($v)->withInput();
			}

			$supplier = new Supplier;
			$supplier->name = Input::get("name");
			$supplier->address = Input::get("address");
			$supplier->description = Input::get("description");
			$supplier->contactperson = Input::get("contactperson");
			$supplier->contactnumber = Input::get("contactnumber");
			$supplier->cat_id = json_encode(Input::get("cat_id"));
			$supplier->save();
			
			Notification::write('Supplier created.', 'success');
			return Redirect::to('admin/supplier');
		}
		else
		{
			$categories = SupplierCategory::all();
			return View::make('suppliers.create')->with('categories', $categories);
		}
	}

	public function anyUpdate($id = 0)
	{
		$supplier = Supplier::find($id);
		
		if(! $supplier)
		{
			Notification::write('Supplier not found.', 'success');
			return Redirect::to('admin/supplier');
		}
		
		$params['supplier'] = $supplier;
		
		if(Input::has('submit'))
		{
			$params['supplier'] = Input::all();
			
			$rules = array
						(
							'name' => 'required', 
							'address' => 'required', 
							'description' => 'required', 
							'contactperson'	=> 'required', 
							'contactnumber'	=> 'required',
							'cat_id' => 'required', 
						);
						
			$v = Validator::make($params['supplier'], $rules);
			
			if($v->passes())
			{
				$supplier->name = Input::get("name");
				$supplier->address = Input::get("address");
				$supplier->description = Input::get("description");
				$supplier->contactperson = Input::get("contactperson");
				$supplier->contactnumber = Input::get("contactnumber");
				$supplier->cat_id = json_encode(Input::get("cat_id"));
				$supplier->save();
				
				Notification::write('Supplier updated.', 'success');
				return Redirect::to('admin/supplier');
			}
			else
			{
				$params['errors'] =  $v->messages();
			}
		}
		
		$params['categories'] = SupplierCategory::all();
		return View::make('suppliers.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$params['supplier'] = Supplier::find($id);
		if(! $params['supplier'])
		{
			Notification::write('Supplier not found.', 'success');
			return Redirect::to('admin/supplier');
		}
		
		$categories = SupplierCategory::all();
		$params['categories'] = array();
		
		foreach($categories as $category)
		{
			$params['categories'][$category->id] = $category->name;
		}
		
		return View::make('suppliers.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$supplier = Supplier::find($id);
		
		if($supplier)
		{
			$supplier->delete();
			Notification::write('Supplier deleted.');
			return Redirect::to('admin/supplier');	
		}
		else
		{
			Notification::write('Supplier not found.');
		}
		
		return Redirect::to('admin/supplier');
	}
	
}