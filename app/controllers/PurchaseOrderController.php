<?php

class PurchaseOrderController extends BaseController {

	public function __construct()
	{					
		$this->yes_no_opt = array(
									'' => '- Select -',
									'Yes' => 'Yes',
									'No' => 'No'
									);
		$this->po_item_insert = array();
		$this->po_item_update = array();
		$this->po_items_id = array();
		$this->item_error = array();
		$this->exempted_ids = array();
		
	}
	
	public function anyIndex()
	{	
		$params['purchase_orders'] = Purchaseorder::all();
		return View::make('purchase-order.index')->with($params);
	}

	public function anyCreate()
	{
		$item = array();
		$params['users'] = user::all();
		$params['prs'] = PurchaseRequisition::where('approved', 'Yes')->get();
		$params['suppliers'] = Supplier::all();
		$params['yes_no_opt'] = $this->yes_no_opt;
		$params['locations'] = Location::all();
		$params['purchaseitems'] = Purchaseitem::where('pr_id', '=', Input::get('pr_id'))->get();
		$params['show_price_field'] = $this->show_price_field = (Auth::user()->type == 'Admin');
		
		if($purchase_orders = Input::all())
		{
			$rules = array
						(
							'pr_id' => 'required', 
							'sp_id' => 'required', 
							'date' => 'required|date', 
							'requested_by' => 'required',
						);
						
			$v = Validator::make($purchase_orders, $rules);
			$this->validateItems();
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$purchase_orders = array();
				$purchase_orders['pr_id'] = Input::get("pr_id");
				$purchase_orders['sp_id'] = Input::get("sp_id");
				$purchase_orders['date'] = Input::get("date");
				$purchase_orders['remarks'] = Input::get("remarks");
				$purchase_orders['prepared_by'] = Auth::user()->id;
				$purchase_orders['requested_by'] = Input::get("requested_by");
				$purchase_orders['status'] = 'To be approve';
				
				$id = DB::table('purchaseorders')->insertGetId($purchase_orders);
				
				foreach($this->po_item_insert as $index => $purchaseitem)
				{
					$purchase_item = Purchaseitem::find($purchaseitem); // to remove - for review
					$this->po_item_insert[$index]['po_id'] = $id;
				}
				
				DB::table('purchaseorder_items')->insert($this->po_item_insert);
				
				Notification::write('Purchase order created.');
				return Redirect::to('admin/po');
			}
			else
			{
				$params['errors'] =  $v->messages();
				
				if(isset($purchase_orders['purchaseitems']))
				{
					foreach($purchase_orders['purchaseitems'] as $index => $po_item)
					{
						$item[$po_item] = $purchase_orders['quantity'][$index];
					}
				}
			}
		}
		$params['item_error'] =  implode('<br/>', $this->item_error);
		$params['po_item'] = $item;
		
		return View::make('purchase-order.create')->with($params);
	}

	public function anyEdit($id = 0)
	{
		if(Auth::user()->type != 'Admin')
		{
			$previous_request = URL::previous();
			Notification::write('Access denied.');
			return Redirect::to($previous_request);
		}
		
		$purchase_order = Purchaseorder::find($id);
		if(! $purchase_order)
		{
			Notification::write('Purchase order not found.');	
			return Redirect::to('admin/po');
		}
		
		$po_items = PurchaseOrderItem::where('po_id', $id)->get();
			
		foreach($po_items as $po_item)
		{
			$this->po_items_id[] = $po_item->id;
		}
		
		$params['users'] = user::all();
		$params['prs'] = PurchaseRequisition::where('approved', 'Yes')->get();
		$params['suppliers'] = Supplier::all();
		$params['yes_no_opt'] = $this->yes_no_opt;
		$params['locations'] = Location::all();
		$params['purchase_order'] = $purchase_order;
		
		if($post = Input::all())
		{
			$params['purchase_order'] = $post;
			$params['purchase_order']['fax_received'] = (Input::get("fax_received")) ? true : false;
			$params['purchase_order']['paid']  = (Input::get("paid")) ? true : false;
			$params['purchase_order']['ordered'] = (Input::get("ordered")) ? true : false;
			
			$rules = array
						(
							'pr_id' => 'required', 
							'sp_id' => 'required', 
							'date' => 'required|date', 
							'requested_by' => 'required',
						);
						
			$v = Validator::make($post, $rules);
			$this->validateItems_update($id);
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$purchase_order->pr_id = Input::get("pr_id");
				$purchase_order->sp_id = Input::get("sp_id");
				$purchase_order->date = Input::get("date");
				$purchase_order->remarks = Input::get("remarks");
				$purchase_order->prepared_by = Auth::user()->id;
				$purchase_order->requested_by = Input::get("requested_by");
				$purchase_order->fax_received = (Input::get("fax_received")) ? 'Yes' : '';
				$purchase_order->paid = (Input::get("paid")) ? 'Yes' : '';
				$purchase_order->ordered = (Input::get("ordered")) ? 'Yes' : '';
				
				if(Input::get("approved") == '')
				{
					$purchase_order->approved_by = 0;
					$purchase_order->status = 'To be approve';
				}
				else
				{
					if($purchase_order->approved != Input::get("approved"))
					{
						$purchase_order->status = 'Approved';
						$purchase_order->approved_by = Auth::user()->id;
					}
				}
				
				$purchase_order->approved = Input::get("approved");
				$purchase_order->save();
				
				if(count($this->exempted_ids) > 0)
				DB::table('purchaseorder_items')->whereNotIn('id', $this->exempted_ids)->where('po_id', $id)->delete();
				
				if(count($this->po_item_insert) > 0)
				DB::table('purchaseorder_items')->insert($this->po_item_insert);
				
				if(count($this->po_item_update) > 0)
				{
					foreach($this->po_item_update as $po_item_update)
					{
						$po_item = PurchaseOrderItem::find($po_item_update['id']);
						$po_item->quantity = $po_item_update['quantity'];
						$po_item->unit_price = $po_item_update['unit_price'];
						$po_item->encoded_total_price = $po_item_update['encoded_total_price'];
						$po_item->pr_item_id = $po_item_update['pr_item_id'];
						$po_item->updated_at = $po_item_update['updated_at'];
						$po_item->save();
					}
				}
				
				Notification::write('Purchase order updated.');
				return Redirect::to('admin/po');
			}
			else
			{
				$params['errors'] =  $v->messages();
			}
		}
		
		$params['item_error'] =  implode('<br/>', $this->item_error);
		$params['po_item'] = $this->initialize_items($id);
		$params['purchaseitems'] = Purchaseitem::where('pr_id', '=', $params['purchase_order']['pr_id'])->get();
		
		return View::make('purchase-order.update')->with($params);
	}
	
	public function anyView($id = 0)
	{
		$purchase_order = Purchaseorder::find($id);
		
		if(! $purchase_order)
		{
			Notification::write('Purchase order not found.');	
			return Redirect::to('admin/po');
		}
		
		$params['purchase_order'] = $purchase_order;
		$params['purchase_order_items'] = PurchaseOrderItem::where('po_id', '=', $id)->get();
		$params['show_price_field'] = (Auth::user()->type == 'Admin');
		
		return View::make('purchase-order.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$purchase_order = Purchaseorder::find($id);
		
		if($purchase_order)
		{
			$purchase_order->delete();
			Notification::write('Purchase order deleted.');	
		}
		else
		{
			Notification::write('Purchase order not found.');
		}
		
		return Redirect::to('admin/po');
	}
	
	private function initialize_items($id)
	{
		$return = array();
		$po_items = Input::get('purchaseitems');
		$quantity = Input::get('quantity');
		$unit_price = Input::get('unit_price');
		$encoded_price = Input::get('encoded_price');
		$item_id = Input::get('id');
		
		if(Input::all())
		{
			$purchaseitems = Purchaseitem::where('pr_id', Input::get('pr_id'))->get();
			if($purchaseitems->count());
			{
				foreach($purchaseitems as $index => $purchaseitem)
				{
					$purchaseitems_id[$purchaseitem->id] = $index;
				}
			}
			
			if($po_items)
			{
				foreach($po_items as $index => $po_item)
				{
					$return[$po_item]['quantity'] = $quantity[$purchaseitems_id[$po_item]];
					$return[$po_item]['unit_price'] = $unit_price[$purchaseitems_id[$po_item]];
					$return[$po_item]['encoded_price'] = $encoded_price[$purchaseitems_id[$po_item]];
					$return[$po_item]['id'] = $item_id[$purchaseitems_id[$po_item]];
				}
			}
			
		}
		else
		{
			$po_items = PurchaseOrderItem::where('po_id', $id)->get();
			
			foreach($po_items as $po_item)
			{
				$return[$po_item->pr_item_id]['quantity'] = $po_item->quantity;
				$return[$po_item->pr_item_id]['unit_price'] = $po_item->unit_price;
				$return[$po_item->pr_item_id]['encoded_price'] = $po_item->encoded_total_price;
				$return[$po_item->pr_item_id]['id'] = $po_item->id;
			}
		}
		
		return $return;
	}
	
	private function validateItems()
	{
		$pr_id = Input::get("pr_id");
		$items = Input::get("purchaseitems");
		$quantity = Input::get("quantity");
		$unit_price = Input::get("unit_price");
		$encoded_price = Input::get("encoded_price");
		
		$quantity_error = 0;
		$invalid_quantity = 0;
		$null_price = 0;
		$invalid_price = 0;
		
		if($items)
		{
			$purchaseitems = Purchaseitem::where('pr_id', '=', $pr_id)->get();
			
			foreach($purchaseitems as $index => $purchaseitem)
			{
				if(in_array($purchaseitem->id, $items))
				{
					$item_unit_price = '';
					$item_total_price = '';
					$item_encoded_price = '';
					
					if($this->show_price_field)
					{
						$item_unit_price = $unit_price[$index];
						$item_encoded_price = $encoded_price[$index];
						$item_total_price = (is_numeric($unit_price[$index]) && is_numeric($quantity[$index])) ? $unit_price[$index] * $quantity[$index] : '';
					}
					
					$this->po_item_insert[] = array(
									'quantity' => $quantity[$index],
									'unit_price' => $item_unit_price,
									'encoded_total_price' => $item_encoded_price,
									'pr_item_id' => $purchaseitem->id,
									);
					
					if($quantity[$index] == '')
					$quantity_error++;
					
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
					
					if($unit_price[$index] == '')
					{
						$null_price++;
					}
					else
					{
						if(! is_numeric($unit_price[$index]))
						$invalid_price++;
					}
				}
			}
			
			if($quantity_error > 0)
			$this->item_error[] = 'All quantity fields are required.';
			
			if($invalid_quantity > 0)
			$this->item_error[] = 'All quantity fields must be in numeric value.';
			
			if($null_price > 0)
			$this->item_error[] = 'All price fields are required.';
			
			if($invalid_price > 0)
			$this->item_error[] = 'All price fields must be in numeric value.';
		}
		else
		{
			$this->item_error[] = 'Purchase requisition item is required.';
		}
	}
	
	private function validateItems_update($po_id)
	{
		$pr_id = Input::get("pr_id");
		$items = Input::get("purchaseitems");
		$quantity = Input::get("quantity");
		$unit_price = Input::get("unit_price");
		$encoded_price = Input::get("encoded_price");
		$id = Input::get("id");
		$quantity_error = 0;
		$invalid_quantity = 0;
		
		if($items)
		{
			$purchaseitems = Purchaseitem::where('pr_id', '=', $pr_id)->get();
			
			foreach($purchaseitems as $index => $purchaseitem)
			{
				if(in_array($purchaseitem->id, $items))
				{
					$po_item = array(
								'quantity' => $quantity[$index],
								'pr_item_id' => $purchaseitem->id,
								'encoded_total_price' => $encoded_price[$index],
								'unit_price' => $unit_price[$index],
								);
								
					$currend_date = date('Y-m-d H:i:s');
					if(in_array($id[$index], $this->po_items_id))
					{
						$this->po_item_update[$index] = $po_item;
						$this->po_item_update[$index]['id'] = $id[$index];
						$this->po_item_update[$index]['updated_at'] = $currend_date;
					
						$this->exempted_ids[] = $id[$index];
					}
					else
					{
						$this->po_item_insert[$index] = $po_item;
						$this->po_item_insert[$index]['po_id'] = $po_id;
						$this->po_item_insert[$index]['created_at'] = $currend_date;
						$this->po_item_insert[$index]['updated_at'] = $currend_date;
					}
					
					if($quantity[$index] == '')
					$quantity_error++;
					
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
				}
			}
			
			if($quantity_error > 0)
			$this->item_error[] = 'All quantity fields are required.';
			
			if($invalid_quantity > 0)
			$this->item_error[] = 'All quantity fields must be in numeric value.';
		}
		else
		{
			$this->item_error[] = 'Purchase requisition item is required.';
		}
		
	}
	
	public function postGetItems()
	{
		$pr_id = Input::get('pr_id');
		$purchaseitems = Purchaseitem::where('pr_id', '=', $pr_id)->get();
		$show_price_field = (Auth::user()->type == 'Admin');
		$items_list = '';
		
		if($purchaseitems->count())
		{
			$items_list = '<table><tr><td>Inventory</td><td>Quantity</td><td>Unit Price</td><td>Total Price</td><td>Encoded Price</td><td>Conversion</td><td>To be approve POs</td><td>Approved POs</td></tr>';
			$items_list = ($show_price_field) ? $items_list : str_replace('<td>Unit Price</td><td>Total Price</td><td>Encoded Price</td>', '', $items_list);
			
			foreach($purchaseitems as $purchaseitem)
			{
				$price_field = ($show_price_field) ? '<td>'. Form::text('unit_price[]', '', array('autocomplete' => 'off')) .'</td>' : '';
				$price_field .= '<td></td>';
				$price_field .= ($show_price_field) ? '<td>'. Form::text('encoded_price[]', '', array('autocomplete' => 'off')) .'</td>' : '';
				
				
				$items_list .= '<tr>';
				$items_list .= '<td>'. Form::checkbox('purchaseitems[]', $purchaseitem->id) . Form::hidden('id[]') .' ' . $purchaseitem->inv->name .'</td>';
				$items_list .= '<td>'. Form::text('quantity[]', $purchaseitem->quantity, array('autocomplete' => 'off')) .'</td>';
				$items_list .= $price_field;
				$items_list .= '<td>'. $purchaseitem->unitofdelivery .' = '. $purchaseitem->multiplier. ' '. $purchaseitem->unitofuse .'</td>';
				$items_list .= '<td>'. Purchaseorderitem::count_po_quantity($purchaseitem->id, 'To be approve') .'</td>';
				$items_list .= '<td>'. Purchaseorderitem::count_po_quantity($purchaseitem->id, 'Approved') .'</td>';
				$items_list .= '</tr>';
			}
			$items_list .= '<table>';
		}
		else
		{
			$items_list = '<tr><td colspan="3">No items found.</td></tr>';
		}
		
		return $items_list;
	}
	
}