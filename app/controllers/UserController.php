<?php

class UserController extends BaseController {

	public function getIndex()
	{
		$params['users'] = User::all();
		return View::make('user.index')->with($params);
	}

	public function anyCreate()
	{
		if(Input::has('submit'))
		{
			$user = Input::all();
			$rules = array
						(
							'firstname' => 'required', 
							'lastname' => 'required', 
							'email' => 'required', 
							'password'	=> 'required', 
							'permissions'	=> '', 
						);
						
			$v = Validator::make($user, $rules);
			
			if($v->passes())
			{
				$user = new User;
				$user->firstname = Input::get("firstname");
				$user->lastname = Input::get("lastname");
				$user->email = Input::get("email");
				$user->type = Input::get("type");
				$user->password = Hash::make(Input::get("password"));
				//$user->permissions = Input::get("permissions");
				$user->save();
				
				Notification::write('User created.', 'success');
				return Redirect::to('admin/user');
			}
			else
			{
				$params['errors'] =  $v->messages();
			}
		}
		
		$params['permissions'] = View::make('user.permissions');
		$params['user_types'] = array('' => '- Select -', 'User' => 'User', 'Manager' => 'Manager', 'Admin' => 'Administrator');
		return View::make('user.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$user = User::find($id);
		
		if(Input::has('submit'))
		{
			$params['user'] = Input::all();
			
			$rules = array
						(
							'firstname' => 'required', 
							'lastname' => 'required', 
							'email' => 'required', 
							'password'	=> 'required', 
							'permissions'	=> '', 
						);
						
			$v = Validator::make($params['user'], $rules);
			
			if($v->passes())
			{
				$user->firstname = Input::get("firstname");
				$user->lastname = Input::get("lastname");
				$user->email = Input::get("email");
				$user->type = Input::get("type");
				$user->password = Hash::make(Input::get("password"));
				$user->save();
				
				Notification::write('User updated.', 'success');
				return Redirect::to('admin/user');
			}
			else
			{
				$params['errors'] =  $v->messages();
			}
			
		}
		else
		{
			$params['user'] = $user;
		}
		
		$params['user_types'] = array('' => '- Select -', 'User' => 'User', 'Manager' => 'Manager', 'Admin' => 'Administrator');
		$params['permissions'] = View::make('user.permissions');
		return View::make('user.edit')->with($params);
	}

	public function getView($id = 0)
	{
		$params['user'] = User::find($id);
		if(! $params['user'])
		{
			return Redirect::to('admin/user');
		}
		
		return View::make('user.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$user = User::find($id);
		
		if($user)
		{
			$user->delete();
			Notification::write('User deleted.');
			return Redirect::to('admin/user');	
		}
		else
		{
			Notification::write('User not found.');
		}
		
		return Redirect::to('admin/user');
	}
	
}