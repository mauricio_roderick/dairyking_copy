<?php

class ReceivingReportController extends BaseController {

	public function __construct()
	{	
		$this->invalid_item = 0;
		$this->item_error = array();
		$this->rr_item_insert = array();
		
	}
	
	public function anyIndex()
	{	
		$params['receivingreports'] = ReceivingReport::all();
		return View::make('receiving-report.index')->with($params);
	}

	public function anyCreate()
	{
		if($post = Input::all())
		{
			$messages = array(
				'unique' => 'A receiving report with this item already exist.',
			);
			
			$rules = array
						(
							'po_id' => 'required', 
							'po_item_id' => 'unique:receivingreports,po_item_id', 
							'received_by' => 'required', 
							'datereceived' => 'required|date', 
						);
						
			$v = Validator::make($post, $rules, $messages);
			
			if($v->passes())
			{
				$receivingreport = new ReceivingReport;
				$receivingreport->po_id = Input::get("po_id");
				$receivingreport->po_item_id = Input::get("po_item_id");
				$receivingreport->received_by = Input::get("received_by");
				$receivingreport->datereceived = Input::get("datereceived");
				
				$receivingreport->save();
				
				Notification::write('Receiving report created.');
				return Redirect::to('admin/po');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$params['purchaseorders'] = Purchaseorder::all();
		$params['users'] = user::all();
		
		return View::make('receiving-report.create')->with($params);
	}
	
	public function anyBatchCreate()
	{
		if($post = Input::all())
		{
			$messages = array(
				'unique' => 'A receiving report with this item already exist.',
			);
			
			$rules = array
						(
							'po_id' => 'required',
							'received_by' => 'required', 
							'datereceived' => 'required|date', 
						);
						
			$v = Validator::make($post, $rules, $messages);
			
			$this->validate_rr_items();
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$current_date = date('Y-m-d H:i:s');
				$inventory_prop = array();
				
				foreach($this->rr_item_insert as $index => $rr_item)
				{
					
					$po_item = PurchaseOrderItem::find($rr_item['po_item_id']);
					$pr_item = $po_item ? $po_item->prItem : '';
					$inventory = $pr_item ? $pr_item->inv : '';
					$inv_id = $inventory ? $inventory->id : '';
					$inv_multiplier = $pr_item ? $pr_item->multiplier : 0;
					$unitofdelivery = $pr_item ? $pr_item->unitofdelivery : 0;
					$unitofuse = $pr_item ? $pr_item->unitofuse : 0;
					
					$rr_item_insert = array( 
											'po_id' => $rr_item['po_id'],
											'po_item_id' => $rr_item['po_item_id'],
											'quantity' => $rr_item['quantity'],
											'available_quantity_to_use' => $rr_item['quantity'] * $inv_multiplier,
											'multiplier' => $inv_multiplier,
											'unitofdelivery' => $unitofdelivery,
											'unitofuse' => $unitofuse,
											'to_location' => $rr_item['location'],
											'to_sub_location' => $rr_item['sub_location'],
											'datereceived' => Input::get("datereceived"),
											'received_by' => Input::get("received_by"),
											'created_at' => $current_date,
											'updated_at' => $current_date,
											);
					
					$rr_id = DB::table('receivingreports')->insertGetId($rr_item_insert);
					
					
					$inventory_prop = array(
													'rr_id' => $rr_id,
													'inv_id' => $inv_id,
													'multiplier' => $inv_multiplier,
													'unitofdelivery' => $unitofdelivery,
													'unitofuse' => $unitofuse,
													'quantityofdelivery' => $rr_item['quantity'],
													'available_quantity_to_use' => $rr_item['quantity'] * $inv_multiplier,
													'location' => $rr_item['location'],
													'sub_location' => $rr_item['sub_location'],
													'datereceived' => $current_date,
													'created_at' => $current_date,
													'updated_at' => $current_date,
													);
													
					$inv_prop_id = DB::table('inventoryproperties')->insertGetId($inventory_prop);
					
					DB::table('inventoryproperties')->where('id', $inv_prop_id)->update(array('batch_id' => $inv_prop_id));
				}
				
				Notification::write('Receiving report created.');
				return Redirect::to('admin/receiving-report');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		$params['item_error'] =  implode('<br/>', $this->item_error);
		$params['purchaseorders'] = Purchaseorder::where('ordered','Yes')->where('approved', 'Yes')->get();
		$params['users'] = User::all();
		$locations = Location::all();
		
		$params['locations_array'] = array('' => '- Department -');
		$params['sub_locations'] = array();
		foreach($locations as $location)
		{
			if($location->parent == 0)
			{
				$params['locations_array'][$location->id] = $location->name;
			}
			else
			{
				$params['sub_locations'][$location->parent][$location->id] = $location->name;
			}
		}
		
		return View::make('receiving-report.batch_create')->with($params);
	}

	public function anyEdit($id = 0)
	{
		if(Auth::user()->type != 'Admin')
		{
			$previous_request = URL::previous();
			Notification::write('Access denied.');
			return Redirect::to($previous_request);
		}
		
		$receivingreport = ReceivingReport::find($id);
		
		if(! $receivingreport)
		{
			Notification::write('Receiving report not found.');	
			return Redirect::to('admin/receiving-report');
		}
		
		if($post = Input::all())
		{
			$rules = array
						(
							'received_by' => 'required', 
							'datereceived' => 'required|date',
						);
						
			$v = Validator::make($post, $rules);
			
			if($v->passes())
			{
				$receivingreport->received_by = Input::get("received_by");
				$receivingreport->datereceived = Input::get("datereceived");
				$receivingreport->save();
				
				Notification::write('Receiving report updated.');
				return Redirect::to('admin/receiving-report');
			}
			else
			{
				$params['form_val'] = $post;
				$params['errors'] =  $v->messages();
			}
		}
		
		$params['form_val'] = $receivingreport;
		$params['receivingreport'] = $receivingreport;
		$params['users'] = User::all();
		
		return View::make('receiving-report.update')->with($params);
	}
	
	public function anyView($id = 0)
	{
		$receivingreport = ReceivingReport::find($id);
		
		if(! $receivingreport)
		{
			Notification::write('Receiving report not found.');	
			return Redirect::to('admin/receiving-report');
		}
		
		$params['receivingreport'] = $receivingreport;
		
		return View::make('receiving-report.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$receivingreport = ReceivingReport::find($id);
		
		if($receivingreport)
		{
			$receivingreport->delete();
			Notification::write('Receiving report deleted.');	
		}
		else
		{
			Notification::write('Receiving report not found.');
		}
		
		return Redirect::to('admin/receiving-report');
	}
	
	public function postGetPoItems()
	{
		$po_id = Input::get('po_id');
		$po_item_id = Input::get('po_item_id');
		
		$purchaseorderitems = PurchaseOrderItem::where('po_id', $po_id)->get();
		
		$items_list = '<select name="po_item_id"><option value="">-- Select PO Item --</option>';
		
		if($purchaseorderitems)
		{
			foreach($purchaseorderitems as $purchaseorderitem)
			{
				$pr_item = $purchaseorderitem->prItem;
				$inventory = $pr_item ? $pr_item->inv : false;
				$inventory_name = $inventory ? '('.$inventory->name.')' : '';
				
				$selected = ($po_item_id == $purchaseorderitem->id ) ? 'selected="selected"' : '';
				$items_list .= '<option value="'. $purchaseorderitem->id .'" '. $selected .' > '. Helper::string_pad($purchaseorderitem->id) .' '. $inventory_name .'</option>';
			}
		}
		
		$items_list .= '<select>';
		return $items_list;
	}
	
	private function validate_rr_items()
	{
		$po_id = Input::get('po_id');
		$po_item_ids = Input::get('po_item_id');
		$quantity = Input::get('quantity');
		$location = Input::get('location');
		$sub_location = Input::get('sub_location');
		
		$quantity_error = 0;
		$invalid_quantity = 0;
		$location_error = 0;
		
		$purchaseorderitems = PurchaseOrderItem::where('po_id', '=', $po_id)->get();
		if($purchaseorderitems->count())
		{
			foreach($purchaseorderitems as $index => $purchaseorderitem)
			{
				if($po_item_ids && in_array($purchaseorderitem->id, $po_item_ids))
				{
					$this->rr_item_insert[] = array(
									'quantity' => $quantity[$index],
									'location' => $location[$index],
									'sub_location' => $sub_location[$index],
									'po_item_id' => $purchaseorderitem->id,
									'po_id' => $po_id,
									);
					
					if($quantity[$index] == '')
					$quantity_error++;
					
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
					
					if($location[$index] == '')
					$location_error++;
				}
			}
			
			if($quantity_error > 0)
			$this->item_error[] = 'Quantity fields are required.';
			
			if($invalid_quantity > 0)
			$this->item_error[] = 'Quantity fields must be in numeric value.';
			
			if($location_error > 0)
			$this->item_error[] = 'Location field is required.';
		}
		
		if(count($this->rr_item_insert) == 0)
		{
			$this->item_error[] = 'Purchase order item is required.';
		}
	}
	
	public function postGetItemsForReceiving()
	{
		$po_id = Input::get('po_id');
		$locations = Location::where('parent', 0)->get();
		
		$locations_array[] = '- Department -';
		foreach($locations as $location)
		{
			$locations_array[$location->id] = $location->name;
		}
		
		$purchaseorderitems = PurchaseOrderItem::where('po_id', '=', $po_id)->get();
		
		$items_list = '';
		
		if($purchaseorderitems->count())
		{
			$items_list = '<table><tr><th></th><th>Quantity to Receive</th><th>Location</th><th>Inventory</th><th>Conversion</th><th>Ordered Quantity</th><th>Received Quantity</th><th>Pending Quantity</th></tr>';
			
			foreach($purchaseorderitems as $purchaseorderitem)
			{
				$pr_item = $purchaseorderitem->prItem;
				$inventory = $pr_item ? $pr_item->inv : '';
				$received_qty = DB::table('receivingreports')->where('po_item_id', $purchaseorderitem->id)->where('received_by', '>', 0)->sum('quantity');
				$received_qty = ($received_qty == '') ? 0 : $received_qty;
				$remaining_qty = $purchaseorderitem->quantity - $received_qty;
				$qty_attributes = array('autocomplete' => 'off');
				
				$items_list .= '<tr>';
				$items_list .= ($received_qty < $purchaseorderitem->quantity) ? '<td>'. Form::checkbox('po_item_id[]', $purchaseorderitem->id) .'</td>' : '<td></td>';
				$items_list .= ($received_qty < $purchaseorderitem->quantity) ? '<td>'. Form::text('quantity[]', '', $qty_attributes).'</td>' : '<td></td>';
				$items_list .= ($received_qty < $purchaseorderitem->quantity) ? '<td>'. Form::select('location[]', $locations_array) . Form::select('sub_location[]', array(' - Sub Department -')) .'</td>' : '<td></td>';
				$items_list .= $inventory ? '<td>'. $inventory->name .'</td>' : '<td></td>';
				$items_list .= $pr_item ? '<td>'. $pr_item->unitofdelivery. ' = '. $pr_item->multiplier . ' ' . $pr_item->unitofuse .'</td>' : '';
				$items_list .= '<td>'. $purchaseorderitem->quantity .'</td>';
				$items_list .= '<td>'. $received_qty .'</td>';
				$items_list .= '<td>'. $remaining_qty .'</td>';
				$items_list .= '</tr>';
				
			}
			
			$items_list .= '<table>';
		}
		else
		{
			$items_list = 'No items found';
		}
		
		return $items_list;
	}
}