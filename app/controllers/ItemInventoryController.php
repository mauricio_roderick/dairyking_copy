<?php

class ItemInventoryController extends BaseController {

	public function __construct()
	{					
		$this->module_title = 'Item Inventory';
		$this->module_name = 'Item Inventory';
	}
	
	public function anyIndex()
	{
		$params['item_inventories'] = ItemInventory::all();
		$params['title'] = $this->module_title;
		$params['module_name'] = $this->module_title;
		
		return View::make('item_inventory.index')->with($params);
	}

	public function anyCreate()
	{
		$params = array();
		$params['title'] = $this->module_name;
		
		if($post = Input::all())
		{
			$item_inventory = Input::all();
			$rules = array
						(
							'name' => 'required',
						);
						
			$v = Validator::make($item_inventory, $rules);
			
			if($v->passes())
			{
				$item_inventory = new ItemInventory;
				$item_inventory->name = $post["name"];
				$item_inventory->description = $post["description"];
				$item_inventory->save();
				
				Notification::write($this->module_name.' created.', 'success');
				return Redirect::to('admin/item-inventory');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		return View::make('item_inventory.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$item_inventory = ItemInventory::find($id);
		
		if(! $item_inventory)
		{
			Notification::write($this->module_name.' not found.');
			return Redirect::to('admin/item-inventory');
		}
		
		$params['item_inventory'] = $item_inventory;
		
		if($post = Input::all())
		{
			$params['item_inventory'] = $post;
			
			$rules = array
						(
							'name' => 'required',
							'contact_num' => 'numeric',
						);
						
			$v = Validator::make($params['item_inventory'], $rules);
			
			if($v->passes())
			{
				$item_inventory->name = $post["name"];
				$item_inventory->description = $post["description"];
				$item_inventory->save();
				
				Notification::write($this->module_name.' updated.', 'success');
				return Redirect::to('admin/item-inventory');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		return View::make('item_inventory.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$params['item_inventory'] = ItemInventory::find($id);
		if(! $params['item_inventory'])
		{
			Notification::write($this->module_name.' not found.');
			return Redirect::to('admin/item-inventory');
		}
		
		return View::make('item_inventory.view')->with($params);
	}
	
	public function anyDelete($id = 0)
	{
		$customer = ItemInventory::find($id);
		
		if($customer)
		{
			$customer->delete();
			Notification::write($this->module_name.' deleted.');
		}
		else
		{
			Notification::write($this->module_name.' not found.');
		}
		
		return Redirect::to('admin/item-inventory');
	}
	
}