<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	 
	public function construct(){
		$this->varaible = 'test';
		parent::construct();
	}	
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	
	public function missingMethod($parameters)
	{
		echo 'Page not found.';
	}

}