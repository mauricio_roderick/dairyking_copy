<?php

class IndexController extends BaseController {

	public function login()
	{
		if(! Auth::guest())
		{
			return Redirect::to('admin');
		}
		
		if(Input::all())
		{
			$userdata = array(
							 'email' => Input::get('username'),
							 'password' => Input::get('password')
							 );
							 
			if( Auth::attempt($userdata) )
			{
				Notification::write('Welcome '.Auth::user( )->firstname.' '.Auth::user( )->lastname.'!');
				return Redirect::to('admin');
			}
			else
			{
				Notification::write('Invalid username or password', 'error');
			}
		}
		
		return View::make('user.login');
	}
	
	public function logout()
	{
		Auth::logout();
		Notification::write('You are now logged out.');
		return Redirect::to('admin/login');
	}
	
	public function bootstrap()
	{
		return View::make('user.login');
	}

}