<?php

class CustomerController extends BaseController {

	public function anyIndex()
	{
		$params['customers'] = Customer::all();
		return View::make('customer.index')->with($params);
	}

	public function anyCreate()
	{
		$params = array();
		
		if($post = Input::all())
		{
			$customer = Input::all();
			$rules = array
						(
							'name' => 'required',
							'contact_num' => 'numeric',
						);
						
			$v = Validator::make($customer, $rules);
			
			if($v->passes())
			{
				$customer = new Customer;
				$customer->name = $post["name"];
				$customer->contact_person = $post["contact_person"];
				$customer->contact_num = $post["contact_num"];
				$customer->save();
				
				Notification::write('Customer created.', 'success');
				return Redirect::to('admin/customer');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		return View::make('customer.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$customer = Customer::find($id);
		
		if(! $customer)
		{
			Notification::write('Customer not found.');
			return Redirect::to('admin/customer');
		}
		
		$params['customer'] = $customer;
		
		if($post = Input::all())
		{
			$params['customer'] = $post;
			
			$rules = array
						(
							'name' => 'required',
							'contact_num' => 'numeric',
						);
						
			$v = Validator::make($params['customer'], $rules);
			
			if($v->passes())
			{
				$customer->name = $post["name"];
				$customer->contact_person = $post["contact_person"];
				$customer->contact_num = $post["contact_num"];
				$customer->save();
				
				Notification::write('Customer updated.', 'success');
				return Redirect::to('admin/customer');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		return View::make('customer.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$params['customer'] = Customer::find($id);
		if(! $params['customer'])
		{
			Notification::write('Customer not found.');
			return Redirect::to('admin/customer');
		}
		
		$params['customer_items'] = CustomerItem::where('customer_id', $id)->get();
		return View::make('customer.view')->with($params);
	}
	
	public function anyDelete($id = 0)
	{
		$customer = Customer::find($id);
		
		if($customer)
		{
			$customer->delete();
			Notification::write('Customer deleted.');
			return Redirect::to('admin/customer');	
		}
		else
		{
			Notification::write('Customer not found.');
		}
		
		return Redirect::to('admin/customer');
	}

	
	public function anyAddItem($customer_id = 0)
	{
		$customer = Customer::find($customer_id);
		
		if(! $customer)
		{
			Notification::write('Customer not found.');
			return Redirect::to('admin/customer');
		}
		
		$params = array();
		$params['item_inventories'] = array('' => '- Select -');
		$params['customer'] = $customer;
		
		$customer_items = CustomerItem::where('customer_id', $customer_id)->get();
		$customer_item_array = array('0');
		foreach($customer_items as $customer_item)
		{
			$customer_item_array[] = $customer_item->item_id;
		}
		
		$item_inventories = DB::table('item_inventories')->whereNotIn('id', $customer_item_array)->get();
		
		
		foreach($item_inventories as $item_inventory)
		{
			$params['item_inventories'][$item_inventory->id] = $item_inventory->name;
		}
		
		if($post = Input::all())
		{
			$item = Input::all();
			$rules = array
						(
							'item_id' => 'required|numeric',
							'quantity' => 'required|numeric',
							'date' => 'required|date',
						);
						
			$v = Validator::make($item, $rules);
			
			if($v->passes())
			{
				$new_customer_item = new CustomerItem;
				$new_customer_item->customer_id = $customer_id;
				$new_customer_item->item_id = $post["item_id"];
				$new_customer_item->quantity = $post["quantity"];
				$new_customer_item->comments = $post["comments"];
				$new_customer_item->date = $post["date"];
				$new_customer_item->save();
				
				Notification::write('Customer Item added.', 'success');
				return Redirect::to('admin/customer');
			}
			else
			{
				$params['errors'] = $v->messages();
			}
		}
		
		return View::make('customer.add_item')->with($params);
	}

}