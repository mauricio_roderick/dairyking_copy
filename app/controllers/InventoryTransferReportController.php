<?php

class InventoryTransferReportController extends BaseController {

	public function getIndex()
	{
		$params = InventoryTransferReport::all();
		
		return View::make('inventory-transfer-report.index')->with('reports', $params);
	}

	public function getView($id = 0)
	{
		$params['report'] = InventoryTransferReport::find($id);
							
		if(! $params['report'])
		{
			Notification::write('Transfer report was not found.', 'alert');
			return Redirect::to('admin/inventory-transfer-report');
		}
		
		$params['categories'] = Category::all();
		return View::make('inventory-transfer-report.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$report = InventoryTransferReport::find($id);
		
		if($report)
		{
			$report->delete();
			Notification::write('Transfer report deleted.');
		}
		else
		{
			Notification::write('Transfer report not found.');
		}
		
		return Redirect::to('admin/inventory-transfer-report');
	}
	
}