<?php

class RecipeController extends BaseController {

	public function __construct()
	{
		$this->item_error = array();
		$this->recipe_item_insert = array();
		$this->recipe_item_update = array();
		$this->exempted_ids = array();
		$this->recipe_items_id = array();
		$this->inventory_list = array();
	}
	
	public function getIndex()
	{
		$params = Recipe::all();
		
		return View::make('recipe.index')->with('recipes', $params);
	}

	public function anyCreate()
	{
		$params['inventory_item_error'] = '';
		$params['inventories'] =  array('0' => '- Select -');
		$params['inventory_list'] =  array();
		
		$params['finish_products'] = array('' => '- Select -');
		$finish_products = Inventory::where('type', 'Finish Product')->get();
		foreach($finish_products as $fp)
		{
			$params['finish_products'][$fp->id] = $fp->name.' ('.$fp->multiplier.' '.$fp->unitofuse .')';
			$finish_prod_lists[$fp->id] = $fp;
		}
		
		$inventories = Inventory::where('type', '!=', 'Finish Product')->get();
		foreach($inventories as $inventory)
		{
			$params['inventories'][$inventory->id] = $inventory->name.'('.$inventory->type.')';
			$params['inventory_list'][$inventory->id]['unitofdelivery'] = $inventory->unitofdelivery;
			$params['inventory_list'][$inventory->id]['unitofuse'] = $inventory->unitofuse;
			$params['inventory_list'][$inventory->id]['multiplier'] = $inventory->multiplier;
			$params['inventory_list'][$inventory->id]['type'] = $inventory->type;
		}
		
		$this->inventory_list = $params['inventory_list'];
		
		if(Input::has('submit'))
		{
			$category = Input::all();
			$rules = array
						(
							'finish_prod_id' => 'required',
						);
						
			$v = Validator::make($category, $rules);
			
			$this->validateItems();
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$current_date = date('Y-m-d H:i:s');
				
				$finish_prod_id = Input::get("finish_prod_id");
				
				$recipe = array();
				$recipe['finish_prod_id'] = Input::get("finish_prod_id");
				$recipe['multiplier'] = $finish_prod_lists[$finish_prod_id]->multiplier;
				$recipe['unit_of_use'] = $finish_prod_lists[$finish_prod_id]->unitofuse;
				$recipe['description'] = Input::get("description");
				$recipe['created_at'] = $current_date;
				$recipe['updated_at'] = $current_date;
				
				$rcp_id = DB::table('recipes')->insertGetId($recipe);
				
				foreach($this->recipe_item_insert as $index => $item)
				{
					$inventory_id = $item['inv_id'];
					
					$recipe_item = array(
										'rcp_id' => $rcp_id,
										'parent' => 0,
										'quantity' => $item['quantity'],
										'inv_id' => $item['inv_id'],
										'type' => $this->inventory_list[$inventory_id]['type'],
										'unit' => $this->inventory_list[$inventory_id]['unitofuse'],
										'multiplier' => ($this->inventory_list[$inventory_id]['type'] == 'Raw Material') ? 0 : $this->inventory_list[$inventory_id]['multiplier'],
										'created_at' => $current_date,
										'updated_at' => $current_date,
										);
													
					$recipe_item_id = DB::table('recipe_items')->insertGetId($recipe_item);
					
					if($this->inventory_list[$inventory_id]['type'] == 'Work in Process')
					{
						$finish_good_items = FinishGoodItem::where('fgd_id', '=', $inventory_id)->get();
						
						if($finish_good_items->count())
						{
							foreach ($finish_good_items as $finish_good_item)
							{
								$recipe_item = array(
													'parent' => $recipe_item_id,
													'inv_id' => $finish_good_item->inv_id,
													'type' => 'Raw Material',
													'rcp_id' => $rcp_id,
													'quantity' => $finish_good_item->quantity * $item['quantity'],
													'multiplier' => $finish_good_item->quantity,
													'unit' => $finish_good_item->unit,
													'created_at' => $current_date,
													'updated_at' => $current_date,
													);
													
								DB::table('recipe_items')->insert($recipe_item);
							}
						}
					}
				}
				
				Notification::write('Recipe created.', 'success');
				return Redirect::to('admin/recipe');
			}
			else
			{
				$params['errors'] =  $v->messages();
				$params['quantity'] =  Input::get("quantity");
				$params['inventory_item_error'] = (count($this->item_error) > 0) ? '<small class="error">'.implode($this->item_error,'</small><small class="error">').'</small>' : '';
			}
		}
		
		return View::make('recipe.create')->with($params);
	}

	public function anyUpdate($id = 0)
	{
		$recipe = Recipe::find($id);
		
		if(! $recipe)
		{
			Notification::write('Recipe was not found.', 'alert');
			return Redirect::to('admin/recipe');
		}
		
		$params['recipe'] = $recipe;
		$params['inventory_item_error'] = '';
		$params['inventories'] =  array('0' => '- Select -');
		$params['inventory_list'] =  array();
		
		$params['finish_products'] = array('' => '- Select -');
		$finish_products = Inventory::where('type', 'Finish Product')->get();
		foreach($finish_products as $fp)
		{
			$params['finish_products'][$fp->id] = $fp->name.' ('.$fp->multiplier.' '.$fp->unitofuse .')';
			$finish_prod_lists[$fp->id] = $fp;
		}
		
		$inventories = Inventory::where('type', '!=', 'Finish Product')->get();
		foreach($inventories as $inventory)
		{
			$params['inventories'][$inventory->id] = $inventory->name.'('.$inventory->type.')';
			$params['inventory_list'][$inventory->id]['unitofdelivery'] = $inventory->unitofdelivery;
			$params['inventory_list'][$inventory->id]['unitofuse'] = $inventory->unitofuse;
			$params['inventory_list'][$inventory->id]['multiplier'] = $inventory->multiplier;
			$params['inventory_list'][$inventory->id]['type'] = $inventory->type;
		}
		
		$this->inventory_list = $params['inventory_list'];
		
		$recipe_items = RecipeItem::where('rcp_id', '=', $id)->where('parent', 0)->get();
		foreach($recipe_items as $index => $recipe_item)
		{
			$params['recipe_inv_item'][$recipe_item->id] = $recipe_item;
			$this->recipe_inv_item[$recipe_item->id] = $recipe_item;
			$this->recipe_inv_item_to_delete[$recipe_item->id] = $recipe_item;
			
			$this->recipe_items_id[] = $recipe_item->id;
		}
		
		if(Input::has('submit'))
		{
			$params['recipe'] = Input::all();
			
			$rules = array
						(
							'finish_prod_id' => 'required',
						);
						
			$v = Validator::make($params['recipe'], $rules);
			
			$this->validateItems_forUpdate($id);
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$finish_prod_id = Input::get("finish_prod_id");
				
				$recipe['finish_prod_id'] = Input::get("finish_prod_id");
				$recipe['multiplier'] = $finish_prod_lists[$finish_prod_id]->multiplier;
				$recipe['unit_of_use'] = $finish_prod_lists[$finish_prod_id]->unitofuse;
				$recipe->description = Input::get("description");
				$recipe->save();
			
				// if(count($this->exempted_ids) > 0)
				// DB::table('recipe_items')->whereNotIn('id', $this->exempted_ids)->where('rcp_id', $id)->delete();
				
				$this->current_date = date('Y-m-d H:i:s');
				
				if(count($this->recipe_inv_item_to_delete) > 0)
				$this->delete_recipe_items($id); //delete
				
				if(count($this->recipe_item_insert) > 0)
				$this->insert_recipe_items($id); //insert
				
				if(count($this->recipe_item_update) > 0)
				$this->update_recipe_items($this->recipe_item_update); //update
				
				Notification::write('Recipe updated.', 'success');
				return Redirect::to('admin/recipe');
			}
			else
			{
				$params['inventory_item_error'] = (count($this->item_error) > 0) ? '<small class="error">'.implode($this->item_error,'</small><small class="error">').'</small>' : '';
				$params['errors'] =  $v->messages();
			}
		}
		
		return View::make('recipe.update')->with($params);
		
	}
	
	public function delete_recipe_items($id = 0)
	{
		foreach($this->recipe_inv_item_to_delete as $recipe_inv_item)
		{
			if($recipe_inv_item->type == 'Work in Process')
			RecipeItem::where('parent', $recipe_inv_item->id)->delete();
			
			RecipeItem::where('id', $recipe_inv_item->id)->delete();
		}
	}
	
	public function update_recipe_items($id = 0)
	{
		foreach($this->recipe_item_update as $recipe_item_update)
		{
			$recipe_item = RecipeItem::find($recipe_item_update['id']);
			$recipe_item->quantity = $recipe_item_update['quantity'];
			$recipe_item->save();
			
			if($recipe_item->type == 'Work in Process')
			{
				$recipe_sub_items = RecipeItem::where('parent', $recipe_item->id)->get();
				
				foreach($recipe_sub_items as $recipe_sub_item)
				{
					$recipe_item = RecipeItem::find($recipe_sub_item->id);
					$recipe_item->quantity = $recipe_item_update['quantity'] * $recipe_item->multiplier;
					$recipe_item->save();
				}
			}
		}
	}
	
	public function insert_recipe_items($recipe_id = 0)
	{
		foreach($this->recipe_item_insert as $index => $item)
		{
			$inventory_id = $item['inv_id'];
			
			$recipe_item = array(
								'rcp_id' => $recipe_id,
								'parent' => 0,
								'quantity' => $item['quantity'],
								'inv_id' => $inventory_id,
								'type' => $this->inventory_list[$inventory_id]['type'],
								'unit' => $this->inventory_list[$inventory_id]['unitofuse'],
								'multiplier' => ($this->inventory_list[$inventory_id]['type'] == 'Raw Material') ? 0 : $this->inventory_list[$inventory_id]['multiplier'],
								'created_at' => $this->current_date,
								'updated_at' => $this->current_date,
								);
											
			$recipe_item_id = DB::table('recipe_items')->insertGetId($recipe_item);
			
			if($this->inventory_list[$inventory_id]['type'] == 'Work in Process')
			{
				$finish_good_items = FinishGoodItem::where('fgd_id', '=', $inventory_id)->get();
				
				if($finish_good_items->count())
				{
					foreach ($finish_good_items as $finish_good_item)
					{
						$recipe_item = array(
											'parent' => $recipe_item_id,
											'inv_id' => $finish_good_item->inv_id,
											'type' => 'Raw Material',
											'rcp_id' => $recipe_id,
											'quantity' => $finish_good_item->quantity * $item['quantity'],
											'unit' => $finish_good_item->unit,
											'multiplier' => $finish_good_item->quantity,
											'created_at' => $this->current_date,
											'updated_at' => $this->current_date,
											);
											
						DB::table('recipe_items')->insert($recipe_item);
					}
				}
			}
		}
	}
	
	public function getView($id = 0)
	{
		$params['recipe'] = Recipe::find($id);
							
		if(! $params['recipe'])
		{
			Notification::write('Recipe was not found.', 'alert');
			return Redirect::to('admin/recipe');
		}
		
		return View::make('recipe.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$recipe = Recipe::find($id);
		
		if($recipe)
		{
			$recipe->delete();
			Notification::write('Recipe deleted.');
			return Redirect::to('admin/recipe');	
		}
		else
		{
			Notification::write('Recipe not found.');
		}
		
		return Redirect::to('admin/recipe');
	}
	
	public function validateItems()
	{
		$inventories = Input::get("inventory_item");
		$quantity = Input::get("quantity");
		
		$invalid_quantity = 0;
		$invalid_inv_id = 0;
		
		if($inventories)
		{
			foreach($inventories as $index => $value)
			{
				$inv_id = $inventories[$index];
				
				if(isset($this->inventory_list[$inv_id]))
				{
					$this->recipe_item_insert[] = array(
									'quantity' => $quantity[$index],
									'inv_id' => $inv_id,
									);
				}
				else
				{
					$invalid_inv_id++;
				}
				
				if(! is_numeric($quantity[$index]))
				$invalid_quantity++;
			}
		}
		
		if(count($this->recipe_item_insert) == 0)
		$this->item_error[] = 'Inventory item is required.';
		
		if($invalid_inv_id > 0)
		$this->item_error[] = 'Invalid inventory found';
		
		if($invalid_quantity > 0)
		$this->item_error[] = 'Quantity fields must be in numeric value.';
	}
	
	public function validateItems_forUpdate($rcp_id)
	{
		$inventories = Input::get("inventory_item");
		$quantity = Input::get("quantity");
		$item_id = Input::get("id");
		
		$quantity_error = 0;
		$invalid_quantity = 0;
		
		if($inventories)
		{
			foreach($inventories as $index => $value)
			{	
				$inv_id = $inventories[$index];
				
				if(isset($this->inventory_list[$inv_id]))
				{
					$recipe_item = array(
										'quantity' => $quantity[$index],
										'inv_id' => $inv_id,
										);
														
					if(in_array($item_id[$index], $this->recipe_items_id))
					{
						$this->recipe_item_update[$index] = $recipe_item;
						$this->recipe_item_update[$index]['id'] = $item_id[$index];
						
						unset($this->recipe_inv_item_to_delete[$item_id[$index]]);
						//$this->exempted_ids[] = $item_id[$index];
					}
					else
					{
						$this->recipe_item_insert[$index] = $recipe_item;
						$this->recipe_item_insert[$index]['rcp_id'] = $rcp_id;
					}
									
					if($quantity[$index] == '')
					$quantity_error++;	
					
					if(! is_numeric($quantity[$index]))
					$invalid_quantity++;
				}
			}
		}
		
		if(count($this->recipe_item_insert) == 0 && count($this->recipe_item_update) == 0)
		$this->item_error[] = 'Inventory item is required.';
		
		if($quantity_error > 0)
		$this->item_error[] = 'All quantity fields are required.';
		
		if($invalid_quantity > 0)
		$this->item_error[] = 'Quantity fields must be in numeric value.';
	}
}