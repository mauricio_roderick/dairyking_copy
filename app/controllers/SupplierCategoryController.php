<?php

class SupplierCategoryController extends BaseController {

	public function __construct()
	{					
		$this->module_title = 'Item Categories';
		$this->module_name = 'Item category';
	}
	
	public function getIndex()
	{
		$params['categories'] = SupplierCategory::all();
		$params['module_title'] = $this->module_title;
		$params['module_name'] = $this->module_name;
		
		return View::make('suppliercategory.index')->with($params);
	}

	public function anyCreate()
	{
		if(Input::has('submit'))
		{
			$category = Input::all();
			$rules = array
						(
							'name' => 'required',
						);
						
			$v = Validator::make($category, $rules);
			if($v->fails())
			{
				return Redirect::to(URL::current())->withErrors($v)->withInput();
			}

			$category = new SupplierCategory;
			$category->name = Input::get("name");
			$category->save();
			
			Notification::write($this->module_name.' created.', 'success');
			return Redirect::to('admin/supplier_category');
		}
		else
		{
			$params['categories'] = Category::all();
			$params['module_name'] = $this->module_name;
			
			return View::make('suppliercategory.create')->with($params);
		}
	}

	public function anyUpdate($id = 0)
	{
		$category = SupplierCategory::find($id);
		
		if(! $category)
		{
			Notification::write($this->module_name.' not found.', 'alert');
			return Redirect::to('admin/supplier_category');
		}
		
		if(Input::has('submit'))
		{
			$params['category'] = Input::all();
			
			$rules = array
						(
							'name' => 'required',
						);
						
			$v = Validator::make($params['category'], $rules);
			if($v->fails())
			{
				Input::flash();
				return Redirect::to(URL::current())->withErrors($v)->with($params);
			}
			
			$category->name = Input::get("name");
			$category->save();
			
			Notification::write($this->module_name.' updated.', 'success');
			return Redirect::to('admin/supplier_category');
		}
		else
		{
			$params['category'] = $category;
			$params['categories'] = Category::where('id', '!=', $id)->get();
			$params['module_name'] = $this->module_name;
			
			return View::make('suppliercategory.update')->with($params);
		}
		
	}

	public function getView($id = 0)
	{
		$params['category'] = SupplierCategory::find($id);
		
		if(! $params['category'])
		{
			Notification::write($this->module_name.' not found.', 'alert');
			return Redirect::to('admin/supplier_category');
		}
		
		$params['categories'] = Category::all();
		$params['module_name'] = $this->module_name;
		return View::make('suppliercategory.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$category = SupplierCategory::find($id);
		
		if($category)
		{
			$category->delete();
			Notification::write($this->module_name.' deleted.', 'success');
			return Redirect::to('admin/supplier_category');	
		}
		else
		{
			Notification::write($this->module_name.' not found.');
		}
		
		return Redirect::to('admin/supplier_category');
	}
	
}