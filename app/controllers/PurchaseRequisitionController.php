<?php

class PurchaseRequisitionController extends BaseController {

	public function __construct()
	{
		$this->status_opt = array(
									'' => '- Select -',
									'To be approved' => 'To be approved',
									'Approved' => 'Approved',
									'Ordered' => 'Ordered'
									);
									
		$this->approved_opt = array(
									'' => '- Select -',
									'Yes' => 'Yes',
									'No' => 'No'
									);
		
		$this->item_error = array();
		$this->purchase_item_insert = array();
		$this->purchase_item_update = array();
		$this->exempted_ids = array();
		$this->puchase_items_id = array();
		$this->inventory_list = array();
	}
	
	public function anyIndex()
	{	
		$params['purchase_requisitions'] = PurchaseRequisition::all();
		return View::make('purchase-requisitions.index')->with($params);
	}

	public function anyCreate()
	{
		$params['inventory_item_error'] = '';
		$params['inventories'] =  array('0' => '- Select -');
		$params['inventory_list'] =  array();
		
		$inventories = Inventory::where('type', '!=', 'Finish Product')->get();
		foreach($inventories as $inventory)
		{
			$params['inventories'][$inventory->id] = $inventory->name;
			$params['inventory_list'][$inventory->id]['unitofdelivery'] = $inventory->unitofdelivery;
			$params['inventory_list'][$inventory->id]['unitofuse'] = $inventory->unitofuse;
			$params['inventory_list'][$inventory->id]['multiplier'] = $inventory->multiplier;
		}
		
		if(Input::has('submit'))
		{
			$purchase_requisition = Input::all();
			$rules = array
						(
							'loc_id' => 'required', 
							'addressed_to' => 'required',  
						);
						
			$v = Validator::make($purchase_requisition, $rules);
			
			$this->validateItems();
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$current_date = date('Y-m-d H:i:s');
			
				$purchase_requisition = array();
				$purchase_requisition['loc_id'] = Input::get("loc_id");
				$purchase_requisition['addressed_to'] = Input::get("addressed_to");
				$purchase_requisition['requested_by'] = Auth::user()->id;
				$purchase_requisition['status'] = 'To be approve';
				$purchase_requisition['created_at'] = $current_date;
				$purchase_requisition['updated_at'] = $current_date;
				
				$id = DB::table('purchaserequisitions')->insertGetId($purchase_requisition);
				
				foreach($this->purchase_item_insert as $index => $item)
				{
					$this->purchase_item_insert[$index]['pr_id'] = $id;
					
					$inventory_id = $item['inv_id'];
					$this->purchase_item_insert[$index]['unitofdelivery'] = $params['inventory_list'][$inventory_id]['unitofdelivery'];
					$this->purchase_item_insert[$index]['unitofuse'] = $params['inventory_list'][$inventory_id]['unitofuse'];
					$this->purchase_item_insert[$index]['multiplier'] = $params['inventory_list'][$inventory_id]['multiplier'];
				}
				
				DB::table('purchaseitems')->insert($this->purchase_item_insert);
				
				Notification::write('New purchase requisition added.');
				return Redirect::to('admin/pr');
			}
			
			$params['quantity'] =  Input::get("quantity");
			$params['unit'] =  Input::get("unit");
			$params['inventory_item_error'] = (count($this->item_error) > 0) ? '<small class="error">'.implode($this->item_error,'</small><small class="error">').'</small>' : '';
			$params['errors'] =  $v->messages();
		}
		
		$params['users'] = user::all();
		$params['status_opt'] = $this->status_opt;
		$params['approved_opt'] = $this->approved_opt;
		$params['locations'] = Location::all();
		$params['purchase_ords'] = Purchaseorder::all();
		
		return View::make('purchase-requisitions.create')->with($params);
		
	}

	public function anyUpdate($id = 0)
	{
		if(Auth::user()->type != 'Admin')
		{
			$previous_request = URL::previous();
			Notification::write('Access denied.');
			return Redirect::to($previous_request);
		}
		
		$purchase_requisition = PurchaseRequisition::find($id);
		
		if(! $purchase_requisition)
		{
			Notification::write('Purchase Requisition not found.');
			return Redirect::to('admin/pr');
		}
		
		$params['purchase_requisition'] = $purchase_requisition;
		$params['purchase_requisition_orig'] = $purchase_requisition;
		$purchase_items = Purchaseitem::where('pr_id', '=', $id)->get();
		$params['inventory_item_error'] = '';
		$params['inventories'] =  array('0' => '- Select -');
		$params['inventory_list'] =  array();
		
		$inventories = Inventory::where('type', '!=', 'Finish Product')->get();
		foreach($inventories as $inventory)
		{
			$params['inventories'][$inventory->id] = $inventory->name;
			$params['inventory_list'][$inventory->id]['unitofdelivery'] = $inventory->unitofdelivery;
			$params['inventory_list'][$inventory->id]['unitofuse'] = $inventory->unitofuse;
			$params['inventory_list'][$inventory->id]['multiplier'] = $inventory->multiplier;
		}
		
		$this->inventory_list = $params['inventory_list'];
		
		foreach($purchase_items as $index => $purchase_item)
		{
			$params['unitofuse'][$index] = $purchase_item->unitofuse;
			$params['multiplier'][$index] = $purchase_item->multiplier;
			$params['unitofdelivery'][$index] = $purchase_item->unitofdelivery;
			$params['quantity'][$index] = $purchase_item->quantity;
			$params['inventory_items'][$index] = $purchase_item->inv_id;
			$params['id'][$index] = $purchase_item->id;
			
			$this->puchase_items_id[] = $purchase_item->id;
		}
		
		if(Input::has('submit'))
		{
			$params['purchase_requisition'] = Input::all();
			
			$rules = array
						(
							'loc_id' => 'required', 
							'addressed_to' => 'required', 
						);
						
			$v = Validator::make($params['purchase_requisition'], $rules);
			
			$this->validateItems_forUpdate($id);
			
			if($v->passes() && count($this->item_error) == 0)
			{
				$purchase_requisition->loc_id = Input::get("loc_id");
				$purchase_requisition->addressed_to = Input::get("addressed_to");
				
				if(Input::get("approved") == '')
				{
					$purchase_requisition->approved_by = 0;
					$purchase_requisition->status = 'To be approve';
				}
				else
				{
					if($purchase_requisition->approved != Input::get("approved"))
					{
						$purchase_requisition->status = 'Approved';
						$purchase_requisition->approved_by = Auth::user()->id;
					}
				}
				
				$purchase_requisition->approved = Input::get("approved");
				$purchase_requisition->save();
				
				if(count($this->exempted_ids) > 0)
				DB::table('purchaseitems')->whereNotIn('id', $this->exempted_ids)->where('pr_id', $id)->delete();
				
				if(count($this->purchase_item_insert) > 0)
				DB::table('purchaseitems')->insert($this->purchase_item_insert);
				
				if(count($this->purchase_item_update) > 0)
				{
					foreach($this->purchase_item_update as $purchase_item_update)
					{
						$purchase_item = Purchaseitem::find($purchase_item_update['id']);
						$purchase_item->quantity = $purchase_item_update['quantity'];
						$purchase_item->save();
					}
				}
				
				Notification::write('Purchase requisition updated.');
				return Redirect::to('admin/pr');
			}
			
			$params['quantity'] =  Input::get("quantity");
			$params['unit'] =  Input::get("unit");
			$params['id'] = Input::get("id");
			$params['inventory_item_error'] = (count($this->item_error) > 0) ? '<small class="error">'.implode($this->item_error,'</small><small class="error">').'</small>' : '';
			$params['errors'] =  $v->messages();
		}
		
		$params['users'] = user::all();
		$params['status_opt'] = $this->status_opt;
		$params['approved_opt'] = $this->approved_opt;
		$params['locations'] = Location::all();
		$params['purchase_ords'] = Purchaseorder::all();
		
		return View::make('purchase-requisitions.update')->with($params);
		
	}

	public function anyView($id = 0)
	{
		$purchase_requisition = PurchaseRequisition::find($id);
		
		if(! $purchase_requisition)
		{
			return Redirect::to('admin/pr');
		}
		
		$params['purchase_requisition'] = $purchase_requisition;
		$params['requested_by'] = User::find($purchase_requisition->requested_by);
		$params['approved_by'] = User::find($purchase_requisition->approved_by);
		$params['purchaseitems'] = Purchaseitem::where('pr_id', '=', $id)->get();
		
		return View::make('purchase-requisitions.view')->with($params);
	}

	public function anyDelete($id = 0)
	{
		$purchase_requisition = PurchaseRequisition::find($id);
		
		if($purchase_requisition)
		{
			$purchase_requisition->delete();
			Notification::write('Purchase Requisition deleted.');
			return Redirect::to('admin/pr');	
		}
		else
		{
			Notification::write('Purchase Requisition not found.');
		}
		
		return Redirect::to('admin/pr');
	}
	
	public function validateItems()
	{
		$inventories = Input::get("inventory_item");
		$quantity = Input::get("quantity");
		$unit = Input::get("unit");
		
		$quantity_error = 0;
		$not_numeric_quantity = 0;
		
		if($inventories)
		{
			foreach($inventories as $index => $value)
			{
				$this->purchase_item_insert[] = array(
									'quantity' => $quantity[$index],
									'inv_id' => $inventories[$index],
									);
									
				if($quantity[$index] == '')
				$quantity_error++;
				
				if(! is_numeric($quantity[$index]))
				$not_numeric_quantity++;
			}
		}
		else
		{
			$this->item_error[] = 'Please add atleast one inventory item.';
		}
		
		if($not_numeric_quantity > 0)
		$this->item_error[] = 'Quantity fields must be in numeric value.';
	}
	
	public function validateItems_forUpdate($pr_id)
	{
		$inventories = Input::get("inventory_item");
		$quantity = Input::get("quantity");
		$unit = Input::get("unit");
		$item_id = Input::get("id");
		
		$quantity_error = 0;
		$not_numeric_quantity = 0;
		
		if($inventories)
		{
			foreach($inventories as $index => $value)
			{	
				$purchase_item = array(
										'quantity' => $quantity[$index],
										'inv_id' => $inventories[$index],
										);
													
				if(in_array($item_id[$index], $this->puchase_items_id))
				{
					$this->purchase_item_update[$index] = $purchase_item;
					$this->purchase_item_update[$index]['id'] = $item_id[$index];
					
					$this->exempted_ids[] = $item_id[$index];
				}
				else
				{
					$this->purchase_item_insert[$index] = $purchase_item;
					$this->purchase_item_insert[$index]['pr_id'] = $pr_id;
					$this->purchase_item_insert[$index]['unitofdelivery'] = $this->inventory_list[$inventories[$index]]['unitofdelivery'];
					$this->purchase_item_insert[$index]['unitofuse'] = $this->inventory_list[$inventories[$index]]['unitofuse'];
					$this->purchase_item_insert[$index]['multiplier'] = $this->inventory_list[$inventories[$index]]['multiplier'];
				}
								
				if($quantity[$index] == '')
				$quantity_error++;	
				
				if(! is_numeric($quantity[$index]))
				$not_numeric_quantity++;
			}
			
		}
		else
		{
			$this->item_error[] = 'Please add atleast one inventory item.';
		}
		
		if($quantity_error > 0)
		$this->item_error[] = 'All quantity fields are required.';
		
		if($not_numeric_quantity > 0)
		$this->item_error[] = 'Quantity fields must be in numeric value.';
	}
	
}