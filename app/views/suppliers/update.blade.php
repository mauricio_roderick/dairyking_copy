@extends('template.main')

@section('title')
Edit Supplier
@stop

@section('content')
	{{ HTML::style('css/jquery.multiselect.css') }}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.widget.js') }}
	{{ HTML::script('js/jquery.multiselect.min.js') }}
	<form method="post">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>
					{{ Form::text('name', $supplier['name']) }}
					{{ $errors->first('name', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Address</th>
				<td>
					{{ Form::textarea('address', $supplier['address']) }}
					{{ $errors->first('address', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>
					{{ Form::textarea('description', $supplier['description'],  array('rows' => '5px')) }}
					{{ $errors->first('description', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Contact Person</th>
				<td>
					{{ Form::text('contactperson', $supplier['contactperson']) }}
					{{ $errors->first('contactperson', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td>
					{{ Form::text('contactnumber', $supplier['contactnumber']) }}
					{{ $errors->first('contactnumber', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
					<select name="cat_id[]" multiple="multiple">
						<?php 
							if(Input::all())
							{
								$cat_ids = isset($supplier['cat_id']) ? $supplier['cat_id'] : array();
							}
							else
							{
								$cat_ids = json_decode($supplier['cat_id']);
							}
						?>
						@foreach($categories as $category)
						<option value="{{ $category->id }}" <?php echo (is_array($cat_ids) && in_array($category->id, $cat_ids))  ? 'selected="selected"' : '' ?> >{{ $category->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('cat_id', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Update" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	<script>
	$(document).ready(function(){
	   $("[name='cat_id[]']").multiselect(); 
	});
	</script>
	<style type="text/css">
		.ui-multiselect-menu{font-size:12px}
		.ui-multiselect-menu label, .ui-multiselect-menu span{font-size:12px; line-height:auto; padding:0}
		.ui-multiselect-menu label input{margin:3px;}
	</style>
@stop
