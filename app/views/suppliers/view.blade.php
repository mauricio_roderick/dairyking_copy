@extends('template.main')

@section('title')
View Supplier
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>
					{{ $supplier['name'] }}
				</td>
			</tr>
			<tr>
				<th>Address</th>
				<td>
					{{ $supplier['address'] }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>{{ $supplier['description'] }}</td>
			</tr>
			<tr>
				<th>Contact Person</th>
				<td>{{ $supplier['contactperson'] }}</td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td>{{ $supplier['contactnumber'] }}</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
				<?php
					$category = json_decode($supplier->cat_id);
					foreach($category as $cat_id)
					echo $categories[$cat_id].'<br/>';
				?>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/supplier/update/'.$supplier['id']) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/supplier/') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
