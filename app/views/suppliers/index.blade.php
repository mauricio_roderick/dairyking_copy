@extends('template.main')

@section('title')
Suppliers
@stop

@section('page-nav')
<a href="{{ url('admin/supplier/create') }}">Create Supplier</a>
@stop

@section('content')
@if($suppliers->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>Supplier</th>
			<th># of Categories</th>
			<th width="300">Description</th>
			<th>Contact Person</th>
			<th>Contact Number</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($suppliers as $supplier)
			<tr>
				<td><a href="{{ url('admin/supplier/view/'.$supplier->id) }}" >{{ $supplier->name }}</a></td>
				<?php $category = $supplier->category ?>
				<td>
					<ul>
					<?php
					if($supplier->cat_id != '')
					{
						$supplier_categories = json_decode($supplier->cat_id);
						foreach ($supplier_categories as $supplier_category_id)
						{
						?>
							<li>{{ $categories[$supplier_category_id] }}</li>
						<?php
						}
					}
					?>
					</ul>
				</td>
				<td>{{ $supplier->description }}</td>
				<td>{{ $supplier->contactperson }}</td>
				<td>{{ $supplier->contactnumber }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/supplier/view/'.$supplier->id) }}" >View</a></li>
						<li><a href="{{ url('admin/supplier/update/'.$supplier->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/supplier/delete/'.$supplier->id) }}">Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No supplier found.' }}
@endif
@stop