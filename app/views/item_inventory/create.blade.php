@extends('template.main')

@section('title')
Create {{ $title }}
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', Input::get('name')) }}
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Description</th>
					<td>
						{{ Form::textarea('description', Input::get('description')) }}
						{{ $errors->first('description', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
