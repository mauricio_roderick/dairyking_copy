@extends('template.main')

@section('title')
{{ $title }}
@stop

@section('page-nav')
<a href="{{ url('admin/item-inventory/create') }}">Create {{ $module_name }}</a>
@stop

@section('content')
@if($item_inventories->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Location</th>
				<th>Description</th>
				<th width="60"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($item_inventories as $item_inventory)
			<tr>
				<td><a href="{{ url('admin/item-inventory/view/'.$item_inventory->id) }}" >{{ $item_inventory->name }}</a></td>
				<td>{{ $item_inventory->description }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/item-inventory/view/'.$item_inventory->id) }}" >View</a></li>
						<li><a href="{{ url('admin/item-inventory/update/'.$item_inventory->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/item-inventory/delete/'.$item_inventory->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop