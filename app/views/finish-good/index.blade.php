@extends('template.main')

@section('title')
Finish Goods
@stop

@section('content')
@if(count($finishgoods))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Inventory</th>
				<th>PROD ID</th>
				<th>Quantity</th>
				<th>Unit of Use</th>
				<th>Location</th>
				<th>Sub Location</th>
				{{--<th width="80"></th>--}}
			</tr>
		</thead>
		<tbody>
			@foreach ($finishgoods as $finishgood)
			<?php
				$inventory = $finishgood->inv;
				$inventory_name = $inventory ? $inventory->name : '';
				
				$prod_ord = $finishgood->productionOrder;
				$location = $prod_ord ? $prod_ord->loc : false;
				$location = $location ? $location->name : '';
				
				$sub_loccation = $prod_ord ? $prod_ord->subLoc : false;
				$sub_loccation = $sub_loccation ? $sub_loccation->name : '';
			
			?>
			<tr>
				<td>{{ $inventory_name }}</td>
				<td>{{ Helper::string_pad($finishgood->prod_ord_id) }}</td>
				<td>{{ $finishgood->quantity }}</td>
				<td>{{ $finishgood->unit_of_use }}</td>
				<td>{{ $location }}</td>
				<td>{{ $sub_loccation }}</td>
				{{--<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/category/view/'.$category->id) }}" >View</a></li>
						<li><a href="{{ url('admin/category/update/'.$category->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/category/delete/'.$category->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>--}}
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop