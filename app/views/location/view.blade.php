@extends('template.main')

@section('title')
View Department
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>{{ $location['name'] }}</td>
			</tr>
			<tr>
				<th>Parent</th>
				<?php $parent = Location::find($location['parent']) ?>
				<td>{{ $parent ? $parent->name : '' }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/department/update/'.$location['id']) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/department') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
