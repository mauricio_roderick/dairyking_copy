@extends('template.main')

@section('title')
Edit Department
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', $location['name']) }}
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Parent</th>
					<td>
						<select name="parent">
							<option value="0">- No Parent -</option>
							@foreach($locations as $loc)
							<option value="{{ $loc->id }}" <?php echo ($location['parent'] == $loc->id) ? 'selected="selected"' : '' ?> >{{ $loc->name }}</option>
							@endforeach
						</select>
						{{ $errors->first('parent', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
