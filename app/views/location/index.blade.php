@extends('template.main')

@section('title')
Department
@stop

@section('page-nav')
<a href="{{ url('admin/department/create') }}">Create Department</a>
@stop

@section('content')
@if($locations->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Location</th>
				<th width="60"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($locations as $location)
			<tr>
				<td><a href="{{ url('admin/department/view/'.$location->id) }}" >{{ $location->name }}</a></td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/department/view/'.$location->id) }}" >View</a></li>
						<li><a href="{{ url('admin/department/update/'.$location->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/department/delete/'.$location->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop