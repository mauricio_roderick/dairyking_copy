@extends('template.main')

@section('title')
Create Inventory
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>
					{{ Form::text('name', Input::get('name')) }}
					{{ $errors->first('name', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>
					{{ Form::textarea('description', Input::get('description'), array('style' => 'width:500px')) }}
					{{ $errors->first('description', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr class="unitofdelivery">
				<th>Unit of Delivery</th>
				<td>
					{{ Form::text('unitofdelivery', Input::get('unitofdelivery')) }}
					{{ $errors->first('unitofdelivery', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Unit of Use</th>
				<td>
					{{ Form::text('unitofuse', Input::get('unitofuse')) }}
					{{ $errors->first('unitofuse', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr class="multiplier">
				<th>Multiplier</th>
				<td>
					{{ Form::text('multiplier', Input::get('multiplier')) }}
					{{ $errors->first('multiplier', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
					<select name="category">
						<option value="">- Select -</option>
						@foreach($categories as $category)
						<option value="{{ $category->id }}" <?php echo (Input::get('category') == $category->id) ? 'selected="selected"' : '' ?> >{{ $category->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('category', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Type</th>
				<td>
					{{ Form::select('type', $inventory_types, Input::get('type') ? Input::get('type') : 'Raw') }}
					{{ $errors->first('type', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr class="components">
				<th>Components</th>
				<td>
					<div>
						<select name="components">
							<option value="">- Select -</option>
							@foreach($components_list as $id => $component)
							<option class="{{ studly_case($component['type'])}}" value="{{ $id }}" <?php echo (Input::get('components') == $id) ? 'selected="selected"' : '' ?> >{{ $component['name'] }}</option>
							@endforeach
						</select>
						<a class="btn btn-small add_btn">Add Item</a>
					</div>
					<div class="error_cont">{{ $components_error }}</div>
					<table class="item_cont">
						<?php 
							$quantity = Input::get('quantity');
							if($inventory_components = Input::get('inventory_component')){ ?>
							@foreach($inventory_components as $index => $component_id)
							<?php if(isset($components_list[$component_id])){ 
								$component = $components_list[$component_id];
								?>
								<tr>
									<td class="padded"><i class="icon-trash">&nbsp;</i></td>
									<td class="padded"><input type="hidden" name="inventory_component[]" value="{{ $component_id }}" /> {{ $component['name'] }}</td>
									@if($component['type'] == 'Work in Process')
									<td class="padded">Quantity in {{ $component['unitofdelivery'] .' ( 1 '. $component['unitofdelivery'] .' = '. $component['multiplier'] .' '. $component['unitofuse'].')' }}</td>
									@else
									<td class="padded">Quantity in {{ $component['unitofuse'] }}</td>
									@endif
									<td><input type="text" name="quantity[]" autocomplete="off" value="{{ $quantity[$index] }}" /></td>
								</tr>
							<?php } ?>
							
							@endforeach
						<?php } ?>
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	var components_list = <?php echo json_encode($components_list) ?>;
	
		$(document).ready(function(){
			function show_components()
			{
				var selected_opt = $('[name="type"]').find(':selected').val();
				
				if(selected_opt == 'Work in Process')
				{
					$('.components').show();
					$('.components .WorkInProcess').hide();
				}
				else if(selected_opt == 'Finish Product')
				{
					$('.components').show();
					$('.components .WorkInProcess').show();
				}
				else
				{
					$('.components').hide();
				}
			}
			
			show_components();
			
			$('[name="type"]').change(function(){
				show_components();
			})
		
			$('[type="reset"]').click(function(){
				$('.components').hide();
			})
		
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var inv_id = $("[name='components']").find('option:selected');
				
				if(inv_id.val() != 0)
				{
					if(typeof components_list[inv_id.val()] == 'undefined')
					{
						alert('Unknown component');
						return;
					}
					
					var component = components_list[inv_id.val()];
					
					if($('.item_cont [name="inventory_component[]"][value="' + inv_id.val() + '"]').size() > 0)
					{
						alert('Item "' + inv_id.html() + '" is already on your list');
						return;
					}
					
					var to_append = to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="inventory_component[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td class="padded"> Quantity in ' + component.unitofuse + '</td><td><input type="text" name="quantity[]" autocomplete="off" /></td></tr>';
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select an item.');
				}
			})
		});
	</script>
	
	<style type="text/css">
		.items_lbl{padding:5px 0 10px}
		.icon-trash{cursor:pointer; padding: 0 20px 0 0}
		.inv_item{padding:3px 0}
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
		select{margin: 0}
	</style>
@stop