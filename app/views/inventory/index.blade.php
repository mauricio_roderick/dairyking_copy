@extends('template.main')

@section('title')
Inventory
@stop

@section('page-nav')
<a href="{{ url('admin/inventory/create') }}">Create Inventory</a>
@stop

@section('content')
@if($inventories->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>Name</th>
			<th>Category</th>
			<th>Description</th>
			<th>Multiplier</th>
			<th>Type</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($inventories as $inventory)
			<?php 
				$category = $inventory->invCategory;
			?>
			<tr>
				<td><a href="{{ url('admin/inventory/view/'.$inventory->id) }}" >{{ $inventory->name }}</a></td>
				<td>{{ $category ? $category->name : '' }}</td>
				<td>{{ $inventory->description }}</td>
				<td>{{ $inventory->multiplier }}</td>
				<td>{{ $inventory->type }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/inventory/view/'.$inventory->id) }}" >View</a></li>
						<li><a href="{{ url('admin/inventory/edit/'.$inventory->id) }}" >Edit</a></li>
						<li><a class="delete" href="<?php echo url('admin/inventory/delete/'.$inventory['id']) ?>">Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No inventory found.' }}
@endif
@stop
