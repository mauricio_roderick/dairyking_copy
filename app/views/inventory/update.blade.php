@extends('template.main')

@section('title')
Edit Inventory
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>
					{{ Form::text('name', $inventory['name']) }}
					{{ $errors->first('name', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>
					{{ Form::textarea('description', $inventory['description'], array('style' => 'width:500px')) }}
					{{ $errors->first('description', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Unit of Delivery</th>
				<td>
					{{ Form::text('unitofdelivery', $inventory['unitofdelivery']) }}
					{{ $errors->first('unitofdelivery', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Unit of Use</th>
				<td>
					{{ Form::text('unitofuse',  $inventory['unitofuse']) }}
					{{ $errors->first('unitofuse', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Multiplier</th>
				<td>
					{{ Form::text('multiplier', $inventory['multiplier']) }}
					{{ $errors->first('multiplier', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
					<select name="category">
						<option value="">- Select -</option>
						@foreach($categories as $category)
						<option value="{{ $category->id }}" <?php echo ($inventory['category'] == $category->id) ? 'selected="selected"' : '' ?> >{{ $category->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('category', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Type</th>
				<td>
					{{ Form::select('type', $inventory_types, $inventory['type']) }}
					{{ $errors->first('type', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr class="components">
				<th>Components</th>
				<td>
					<div>
						<select name="components">
							<option value="">- Select -</option>
							@foreach($components_list as $id => $component)
							<option class="{{ studly_case($component['type'])}}" value="{{ $id }}" <?php echo (Input::get('components') == $id) ? 'selected="selected"' : '' ?> >{{ $component['name'] }}</option>
							@endforeach
						</select>
						<a class="btn btn-small add_btn">Add Item</a>
					</div>
					<div class="error_cont">{{ $components_error }}</div>
					<table class="item_cont">
						@foreach($inventory_components as $index => $inventory_component)
						<?php
							$component_id = $inventory_component['inv_id'];
							if(isset($components_list[$component_id]))
							{
						?>
							<tr>
								<td class="padded"><i class="icon-trash">&nbsp;</i></td>
								<td class="padded">
									<input type="hidden" name="inventory_component[]" value="{{ $component_id }}" /> {{ $components_list[$component_id]['name'] }}
									<input type="hidden" name="id[]" value="{{ $inventory_component['id'] }}" />
								</td>
								<td class="padded">Quanity in {{ isset($inventory_component['unit']) ? $inventory_component['unit'] : $components_list[$component_id]['unitofuse'] }}</td>
								<td><input type="text" name="quantity[]" value="{{ $inventory_component['quantity'] }}" autocomplete="off" /></td>
							</tr>
						<?php 
							}
						?>
						@endforeach
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Update" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	var components_list = <?php echo json_encode($components_list) ?>;
	console.log(components_list[1])
		$(document).ready(function() {
			function show_components()
			{
				var selected_opt = $('[name="type"]').find(':selected').val();
				
				if(selected_opt == 'Work in Process')
				{
					$('.components').show();
					$('.components .WorkInProcess').hide();
				}
				else if(selected_opt == 'Finish Product')
				{
					$('.components').show();
					$('.components .WorkInProcess').show();
				}
				else
				{
					$('.components').hide();
				}
			}
			
			show_components();
			
			$('[name="type"]').change(function(){
				show_components();
			})
		
			$('[type="reset"]').click(function(){
				$('.components').hide();
			})
		
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var inv_id = $("[name='components']").find('option:selected');
				
				if(inv_id.val() != 0)
				{
					if(typeof components_list[inv_id.val()] == 'undefined')
					{
						alert('Unknown Inventory');
						return;
					}
					
					var component = components_list[inv_id.val()];
					
					if($('.item_cont [name="inventory_component[]"][value="' + inv_id.val() + '"]').size() > 0)
					{
						alert('Item "' + inv_id.html() + '" is laready on your list');
						return;
					}
					
					var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="id[]" /><input type="hidden" name="inventory_component[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td class="padded">Quantity in ' + component.unitofuse + '</td><td><input type="text" name="quantity[]" autocomplete="off" /></td></tr>';
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select an item.');
				}
			})
		});
	</script>
	
	<style type="text/css">
		.items_lbl{padding:5px 0 10px}
		.icon-trash{cursor:pointer; padding: 0 20px 0 0}
		.inv_item{padding:3px 0}
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
		select{margin: 0}
	</style>
@stop
