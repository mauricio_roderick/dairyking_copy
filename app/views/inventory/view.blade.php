@extends('template.main')

@section('title')
View Inventory
@stop

@section('content')
	<?php $supplier = $inventory->inventorySupplier ?>
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>
					{{ $inventory->name }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>
					{{ $inventory->description }}
				</td>
			</tr>
			<tr>
				<th>Category</th>
				<td>
					<?php
						$category = $inventory->invCategory;
						$category = $category ? $category->name : '';
						echo $category;
					?>
				</td>
			</tr>
			<tr>
				<th>Unit of Delivery</th>
				<td>
					{{ $inventory->unitofdelivery }}
				</td>
			</tr>
			<tr>
				<th>Unit of Use</th>
				<td>
					{{ $inventory->unitofuse }}
				</td>
			</tr>
			<tr>
				<th>Multiplier</th>
				<td>
					{{ $inventory->multiplier }}
				</td>
			</tr>
			<tr>
				<th>Type</th>
				<td>
					{{ $inventory->type }}
				</td>
			</tr>
			<?php 
				if($inventory->type == 'Work in Process'){ 
				$components = InventoryComponents::where('inventory', $inventory->id)->get();
			?>
			<tr>
				<th>Components</th>
				<td>
					<div class="item_cont">
						<table>
							<tr>
								<th>Item name</th>
								<th>Quantity</th>
								<th>Unit</th>
							</tr>
							<?php
								foreach($components as $component)
								{
									$inventory = $component->parentInventory;
								?>
								<tr>
								<td>{{ $inventory ? $inventory->name : '<span class="label label-important">Missing inventory</span>' }}</td>
								<td>{{ $component->quantity }}</td>
								<td>{{ $component->unit }}</td>
								</tr>
							<?php 
								}
							?>
						</table>
					</div>
				</td>
			</tr>
			<?php } ?>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/inventory/edit/'.$inventory->id) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/inventory') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	
	<style type="text/css">
		.item_cont td, .table-form .item_cont th{border:0; text-align:center}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
	</style>
@stop
