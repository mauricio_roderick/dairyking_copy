@extends('template.main')

@section('title')
Update Category
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', $category['name']) }}
						{{ $errors->first('firstname', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Parent</th>
					<td>
						{{ Form::label('parent', 'Parent') }}
						<select name="parent">
							<option value="0">- No Parent -</option>
							@foreach($categories as $category)
							<option value="{{ $category->id }}" <?php echo ($category['parent'] == $category->id) ? 'selected="selected"' : '' ?> >{{ $category->name }}</option>
							@endforeach
						</select>
						{{ $errors->first('parent', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
