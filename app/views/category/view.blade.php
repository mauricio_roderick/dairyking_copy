@extends('template.main')

@section('title')
View Category
@stop

@section('content')
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>{{ $category->name }}</td>
			</tr>
			<tr>
				<th>Parent</th>
				<td>
					<?php $parent = Category::find($category->parent) ?>
					{{ ($parent) ? $parent->name : 'No parent' }}
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/category/update/'.$category->id) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/category') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
