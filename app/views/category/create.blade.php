@extends('template.main')

@section('title')
Create Category
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', Input::old('name')) }}
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Parent</th>
					<td>
						<select name="parent">
							<option value="0">- No Parent -</option>
							@foreach($categories as $category)
							<option value="{{ $category->id }}" <?php echo (Input::old('parent') == $category->id) ? 'selected="selected"' : '' ?> >{{ $category->name }}</option>
							@endforeach
						</select>
						{{ $errors->first('parent', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
