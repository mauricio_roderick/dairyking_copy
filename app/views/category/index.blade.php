@extends('template.main')

@section('title')
Category
@stop

@section('page-nav')
<a href="{{ url('admin/category/create') }}">Create Category</a>
@stop

@section('content')
@if(count($categories))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Category</th>
				<th>Parent</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $category)
			<tr>
				<td><a href="{{ url('admin/category/view/'.$category->id) }}" >{{ $category->name }}</a></td>
				<td>
					<?php //$parent = $category->with_parent // problem with this, does not show parent category ?>
					<?php $parent = Category::find($category['parent']) ?>
					{{ ($parent) ? $parent->name : '' }}
				</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/category/view/'.$category->id) }}" >View</a></li>
						<li><a href="{{ url('admin/category/update/'.$category->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/category/delete/'.$category->id) }}" >Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop