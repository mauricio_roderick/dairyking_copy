
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>LWMC Dispatch Tool | @yield('title') </title>
	{{ HTML::style('css/normalize.css') }}
	{{ HTML::style('css/foundation.css') }}
	{{ HTML::style('css/custom.css') }}
	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('js/vendor/custom.modernizr.js') }}
</head>
<body class="template_plain">

	@include('template.plain_nav')
	
	<div class="row">
		<div class="large-4 columns" >&nbsp;</div>
		<div class="large-4 columns" >
			{{ Notification::display() }}
		</div>
		<div class="large-4 columns" >&nbsp;</div>
    </div>
	
	<div class="row">
		<div class="large-12 columns" >
			@yield('body')
		</div>
    </div>


	<footer class="row">
		<div class="large-12 columns">
		
		</div>
	</footer>

	<script>
		document.write('<script src={{url('')}}/js/vendor/' +
		('__proto__' in {} ? 'zepto' : 'jquery') +
		'.js><\/script>')
	</script>
	{{ HTML::script('js/foundation.min.js') }}
	<script>
		$(document).foundation();
  
	</script>
</body>
</html>
<?php Notification::clear(); ?>