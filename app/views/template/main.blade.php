<!DOCTYPE html>
<head>
	<title>Dairy King | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
	{{ HTML::style('css/style.css') }}
	{{ HTML::style('css/custom.css') }}
	{{ HTML::script('jquery-ui-1.10.3/jquery-1.9.1.js') }}
	{{ HTML::script('js/global_js.js') }}
	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('bootstrap/js/bootstrap.min.js') }}
	
	{{ HTML::style('jquery-ui-1.10.3/themes/base/jquery.ui.all.css') }}
	{{ HTML::style('jquery-ui-1.10.3/themes/base/jquery.ui.all.css') }}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.core.js') }}
	{{-- HTML::script('jquery-ui-1.10.3/ui/jquery.ui.widget.js') --}}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.datepicker.js') }}
	
	
</head>
<body class="template_main">
	@include('template.top_nav')
	
	<div class="container">
		
		<div class="row"></div>
		<div class="row">
			<div class="span9">
				{{ Notification::display() }}
				<div class="navbar">
				  <div class="navbar-inner">
					<a class="brand">@yield('title')</a>
					<ul class="nav pull-right">
					  <li >@yield('page-nav')</li>
					</ul>
				  </div>
				</div>
				@yield('content')
			</div>
			<div class="span4">
			</div>
		</div>
	</div>
	
	<div id="footer">
      <div class="container">
        <div class="row">
			<div class="span12">&nbsp;</div>
		</div>
      </div>
    </div>
</body>
</html>
<?php Notification::clear(); ?>
