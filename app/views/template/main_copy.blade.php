
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width" />

	<title>Dairy king | @yield('title') </title>
	{{ HTML::style('css/normalize.css') }}
	{{ HTML::style('css/foundation.css') }}
	{{ HTML::style('css/custom.css') }}
	{{ HTML::script('jquery-ui-1.10.3/jquery-1.9.1.js') }}
	{{ HTML::script('js/vendor/custom.modernizr.js') }}
	{{ HTML::script('js/global_js.js') }}
	
	
	{{ HTML::style('jquery-ui-1.10.3/themes/base/jquery.ui.all.css') }}
	{{ HTML::style('jquery-ui-1.10.3/themes/base/jquery.ui.all.css') }}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.core.js') }}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.widget.js') }}
	{{ HTML::script('jquery-ui-1.10.3/ui/jquery.ui.datepicker.js') }}
	<?php
	/* 
	<script src="../../jquery-1.9.1.js"></script>
	<script src="../../ui/jquery.ui.core.js"></script>
	<script src="../../ui/jquery.ui.widget.js"></script>
	<script src="../../ui/jquery.ui.datepicker.js"></script>
	<link rel="stylesheet" href="../demos.css"> */ ?>
	
</head>
<body>

<!-- Nav Bar -->

	@include('template.nav')

<!-- End Nav -->


	<!-- Main Page Content and Sidebar -->
	<div class="row main_content">
		{{ Notification::display() }}
		<div class="large-9 columns">
			<h2>@yield('title')</h2>
		</div>
		 <!-- Main Blog Content -->
		<div class="large-9 columns" role="content">
			
				@yield('body')

		</div>

		<!-- End Main Content -->

		<!-- Sidebar -->
		@include('template.side')
		<!-- End Sidebar -->
	</div>

	<!-- End Main Content and Sidebar -->


	<!-- Footer -->

	<footer class="row">
	<div class="large-12 columns">
	  <hr />
	  <div class="row">
		<div class="large-6 columns">
		  <p>&copy; Copyright.</p>
		</div>
		<div class="large-6 columns">
		 
		</div>
	  </div>
	</div>
	</footer>

	<script>
		document.write('<script src={{url('')}}/js/vendor/' +
		('__proto__' in {} ? 'zepto' : 'jquery') +
		'.js><\/script>')
	</script>
	{{ HTML::script('js/foundation.min.js') }}
	<script>
		$(document).foundation();
  
	</script>

  <!-- End Footer -->
</body>
</html>
<?php Notification::clear(); ?>
