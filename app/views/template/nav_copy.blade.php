<nav class="top-bar">

  <section class="top-bar-section">
  
	  <ul class="title-area">
		<!-- Title Area -->
		<li class="name">
		  <h1><a href="#">Dairy King</a></h1>
		</li>
		<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
		<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	  </ul>
	  
    <!-- Left Nav Section -->
    <ul class="left">
		<li class="divider"></li>
		<li class="has-dropdown">
			<a>Menu</a>
			<ul class="dropdown">
				<li class="has-dropdown">
					<a href="{{ url('admin/supplier') }}">Supplier</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/supplier') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/supplier/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/inventory') }}">Inventory</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/inventory') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/inventory/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/pr') }}">Purchase Requisition</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/pr') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/pr/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/po') }}">Purchase Orders</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/po') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/po/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/pi') }}">Purchase Items</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/pi') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/pi/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/location') }}">Location</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/location') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/location/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/user') }}">User</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/user') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/user/create') }}">Create</a></li>
					</ul>
				</li>
				<li class="divider"></li>
				<li class="has-dropdown">
					<a href="{{ url('admin/category') }}">Category</a>
					<ul class="dropdown">
						<li><a href="{{ url('admin/category') }}">Lists</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('admin/category/create') }}">Create</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li class="divider"></li>
    </ul>
	
	<ul class="right">
		<li class="divider hide-for-small"></li>
		<li class="hover"><a href="{{ url('logout') }}">Logout</a></li>
	</ul>
</nav>