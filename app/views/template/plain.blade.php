<!DOCTYPE html>
<html>
  <head>
    <title>Dairy King | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet">
	{{ HTML::style('css/style.css') }}
	{{ HTML::script('js/jquery.min.js') }}
	{{ HTML::script('bootstrap/js/bootstrap.min.js') }}
  </head>
  <body class="plain">
	<div class="nav_placeholder"></div>
	<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
			<h4 class="logo">Dairy King</h4>
        </div>
      </div>
    </div>
	
	<div class="container">
		<div class="row">
			<div class="span4 offset4">{{ Notification::display() }}</div>
			@yield('content')
		</div>
	</div>
	
  </body>
</html>
<?php Notification::clear(); ?>
