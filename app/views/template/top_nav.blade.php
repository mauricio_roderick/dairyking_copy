<div class="nav_placeholder"></div>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
	<div class="container">
	  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	  </button>
	  <a class="brand logo" >Project name</a>
	  <div class="nav-collapse collapse">
		<ul class="nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Supply Chain <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="{{ url('admin/pr') }}">Purchase Requisition</a></li>
					<li><a href="{{ url('admin/po') }}">Purchase Orders</a></li>
					<li><a href="{{ url('admin/receiving-report') }}">Receiving Report</a></li>
					<li><a href="{{ url('admin/production-order') }}">Production Order</a></li>
					<li><a href="{{ url('admin/inventory-property') }}">Stock Overview</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="{{ url('admin/supplier_category') }}">Item Categories</a></li>
					<li><a href="{{ url('admin/supplier') }}">Supplier database</a></li>
					<li><a href="{{ url('admin/inventory') }}">Inventory Database</a></li>
					{{--<li><a href="{{ url('admin/recipe') }}">Product Recipes</a></li>--}}
					<li><a href="{{ url('admin/department') }}">Department</a></li>
					<li><a href="{{ url('admin/item-inventory') }}">Item Database</a></li>
					<li><a href="{{ url('admin/customer') }}">Customer Database</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="{{ url('admin/inventory-transfer-report') }}">Inventory Transfer Report</a></li>
					<li><a href="{{ url('admin/finish-goods') }}">Finished Goods</a></li>
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Settings <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="{{ url('admin/user') }}">User Settings</a></li>
				</ul>
			</li>
		</ul>
		<ul class="nav pull-right">
		  <li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user( )->firstname.' '.Auth::user( )->lastname }} <b class="caret"></b></a>
			<ul class="dropdown-menu">
			  <li><a href="#">Profile</a></li>
			  <li class="divider"></li>
			  <li><a href="{{ url('logout') }}">Logout</a></li>
			</ul>
		  </li>
		</ul>
	  </div>
	</div>
  </div>
</div>