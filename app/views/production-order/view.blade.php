@extends('template.main')

@section('title')
View Production Order
@stop

@section('content')
	<?php 
		$recipe = $production_order->recipe;
		$recipe = $recipe ? $recipe->name : '';
		$location = $production_order->loc;
		$location = $location ? $location->name : '';
		$sub_location = $production_order->subLoc;
		$sub_location = $sub_location ? $sub_location->name : '';
	?>
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>PROD Id</th>
				<td>{{ Helper::string_pad($production_order->id) }}</td>
			</tr>
			<tr>
				<th>Location</th>
				<td>
					{{ $location }}
				</td>
			</tr>
			<tr>
				<th>Sub Location</th>
				<td>
					{{ $sub_location }}
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<table class="item_cont">
						<tr>
							<th>Recipe</th>
							<th>Quantity</th>
						</tr>
					<?php 
					if($finishgoods->count())
					{
						
						foreach($finishgoods as $finishgood)
						{
							$inventory = $finishgood->inv;
							$inventory_name = $inventory ? $inventory->name : '';
					?>
						<tr>
							<td> {{ $inventory_name }}</td>
							<td >{{ $finishgood->quantity }}</td>
						</tr>
					<?php
						}
					}
					?>
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/production-order') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	<style type="text/css">
		.table-form .item_cont td, .table-form .item_cont th{border:0; text-align:left}
	</style>
@stop
