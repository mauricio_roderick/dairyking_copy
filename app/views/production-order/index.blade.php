@extends('template.main')

@section('title')
Production Order
@stop

@section('page-nav')
<a href="{{ url('admin/production-order/create') }}">Create Production Order</a>
@stop

@section('content')
@if(count($production_orders))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>PROD Id</th>
				<th>Location</th>
				<th>Sub Location</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($production_orders as $production_order)
			<?php
				$location = $production_order->loc;
				$location = $location ? $location->name : '' ;
				$sub_loc = Location::find($production_order->sub_loc_id);
				$sub_loc = $sub_loc ? $sub_loc->name : '' ;
			?>
			<tr>
				<td><a href="{{ url('admin/production-order/view/'.$production_order->id) }}" >{{ Helper::string_pad($production_order->id) }}</a></td>
				<td>{{ $location }}</td>
				<td>{{ $sub_loc }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/production-order/view/'.$production_order->id) }}" >View</a></li>
						<li><a class="delete" href="{{ url('admin/production-order/cancel/'.$production_order->id) }}" >Cancel</a></li>
						<li><a class="delete" href="{{ url('admin/production-order/delete/'.$production_order->id) }}" >Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop