@extends('template.main')

@section('title')
Create Production Order
@stop

@section('content')
	<div class="production-order-create">
		<?php	echo ($inv_prop_error != '') ? '<div class="alert alert-error">'.$inv_prop_error.'</div>' : ''; ?>
		<ul class="nav nav-tabs" id="myTab">
			<li class="active" ><a href="#recipes">Recipes</a></li>
			<li><a href="#iventories">Suggested Inventories</a></li>
		</ul>

		<form method="POST" class="form-horizontal">
			<div class="tab-content">
				<div class="tab-pane active" id="recipes">
					<table class="table-form table-bordered">
						<tbody>
							<tr>
								<th>Location</th>
								<td>
									{{ Form::select('loc_id', $locations, Input::get('loc_id')) }}
									{{ $errors->first('loc_id', ' <span class="help-inline">:message</span>') }}
								</td>
							</tr>
							<tr>
								<th>Sub Location</th>
								<td>
									<?php
										$sub_loc_options = isset($sub_locations[Input::get('loc_id')]) ? $sub_locations[Input::get('loc_id')] : array();
										$sub_loc_options['0'] = '- Select -';
										ksort($sub_loc_options);
									?>
									{{ Form::select('sub_loc_id', $sub_loc_options, Input::get('sub_loc_id')) }}
									{{ $errors->first('sub_loc_id', ' <span class="help-inline">:message</span>') }}
								</td>
							</tr>
							<tr>
								<th>Inventory</th>
								<td>
									{{ Form::select('inventories', $inventories) }}
									<a class="btn btn-small add_btn">Add Item</a>
									<br/>
									<?php echo $item_error ?>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<table class="item_cont">
										<?php if($prod_order_items){ ?>
											@foreach($prod_order_items as $index => $item)
												<tr>
													<td class="padded"><i class="icon-trash">&nbsp;</i></td>
													<td class="padded"><input type="hidden" name="prod_order_item[]" value="{{ $item['inv_id'] }}" /> {{ $inventory_list[$item['inv_id']]->name }}</td>
													<td><input type="text" name="quantity[]" placeholder="Quantity" autocomplete="off" value="{{ $item['quantity'] }}" /></td>
												</tr>
											@endforeach
										<?php } ?>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
					<br/>
					<br/>
				</div>
				<div class="tab-pane" id="iventories">
					<?php
						$inv_prop = Input::get('inv_prop');
						$inv_prop_quantity = Input::get('inv_prop_quantity');
						$inv_prop_quantity_ctr = 0;
						
					?>
					@foreach ($recipe_raw_materials as $index => $recipe_raw_material)
						<div>
							<?php 
								$inventory = Inventory::find($index);
								if($inventory)
								{
							?>
							<?php $available_inventory_properties = InventoryProperty::where('inv_id', $index)->where('available_quantity_to_use', '!=', 0)->orderBy('datereceived')->get() ?>
							<div>
									<div class="alert">
										<strong>{{ $inventory->name.' &rarr; ' }}</strong>
										@foreach($recipe_raw_material as $unit => $multiplier)
											{{ $multiplier.' '.$unit.', ' }}
										@endforeach
										@if($available_inventory_properties->count() == 0)
										<span class="label label-inverse">{{ 'No stocks found.' }}</span>
										@endif
									</div>
							</div>
							<div class="available_inventory_properties">
							@if($available_inventory_properties->count())
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th></th>
											<th>Quantity</th>
											<th>Batch</th>
											<th>Available Quantity</th>
											<th>Unit</th>
											<th>Location</th>
											<th>Sub Location</th>
											<th>Datereceived</th>
											<th>Counter</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($available_inventory_properties as $available_inventory_property)
										<?php
											$batch = date('Ymd', strtotime($available_inventory_property->datereceived))."-".Helper::string_pad($available_inventory_property->id);
											$datereceived = date('M d Y', strtotime($available_inventory_property->datereceived));
											$now = time();
											$curr = strtotime($datereceived);
											$diff = $now - $curr;
											$counter = floor($diff/(60*60*24));
											$location = $available_inventory_property->parentLocation;
											$location = ($location) ? $location->name : '';
											$sub_location = $available_inventory_property->subLocation;
											$sub_location = ($sub_location) ? $sub_location->name : '';
										?>
										<tr>
											<?php $checked = is_array($inv_prop) && in_array($available_inventory_property->id, $inv_prop) ?>
											<td style="width:15px">{{ Form::checkbox('inv_prop[]', $available_inventory_property->id, $checked) }}</td>
											<td style="width:60px">{{ Form::text('inv_prop_quantity_'.$available_inventory_property->id, Input::get('inv_prop_quantity_'.$available_inventory_property->id), array('class' => 'quantity') ) }}</td>
											<td>{{ $batch }}</td>
											<td>{{ $available_inventory_property->available_quantity_to_use }}</td>
											<td>{{ $available_inventory_property->unitofuse }}</td>
											<td>{{ $location }}</td>
											<td>{{ $sub_location }}</td>
											<td>{{ Helper::format_datetime($available_inventory_property->datereceived) }}</td>
											<td>{{ $counter }}</td>
										</tr>
										<?php $inv_prop_quantity_ctr++ ?>
										@endforeach
									</tbody>
								</table>
							@endif
							</div>
							<?php	
								}
							?>
						</div>
					@endforeach
				</div>
				
				<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
			</div>
		</form>	
	</div>
	
	<script type="text/javascript">
	var inventory_list = <?php echo json_encode($inventory_list) ?>;
	var sub_locations = <?php echo json_encode($sub_locations) ?>;
		
		function show_sub_locations()
		{
			var value = $('[name="loc_id"] option:selected').val();
			var options = '<option value="0">- Select -</option>';
			
			if(typeof sub_locations[value] != 'undefined')
			{
				for(x in sub_locations[value])
				{
					options += '<option value="' + x + '" >' + sub_locations[value][x] + '</option>'
				}
			}
			
			$('[name="sub_loc_id"]').html(options);
		}
				
		$(document).ready(function() {
			
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var rcp_id = $("[name='inventories']").find('option:selected');
				
				if(rcp_id.val() != 0)
				{
					if(typeof inventory_list[rcp_id.val()] == 'undefined')
					{
						alert('Unknown Recipe');
						return;
					}
					
					var recipe = inventory_list[rcp_id.val()];
					
					if($('.item_cont [name="prod_order_item[]"][value="' + rcp_id.val() + '"]').size() > 0)
					{
						alert('Recipe "' + rcp_id.html() + '" is laready on your list');
						return;
					}
					
					var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="prod_order_item[]" value="' + rcp_id.val() + '" />' + rcp_id.html() + '</td><td><input type="text" placeholder="Quantity" name="quantity[]" autocomplete="off" /></td></tr>';
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select a recipe.');
				}
			})
			
			$('#myTab a').click(function(e){
				e.preventDefault();
				$(this).tab('show');
			})
			
			$('[name="loc_id"]').change(function(e){
				show_sub_locations();
			})
		});
	</script>
	<style type="text/css">
	.items_lbl{padding:5px 0 10px}
	.icon-trash{cursor:pointer; padding: 0 20px 0 0}
	.inv_item{padding:3px 0}
	.error_cont{padding-top:10px}
	.error_cont .error{display:block;}
	.item_cont td{border:0}
	.item_cont .padded{padding:15px 10px 10px}
	.item_cont input{width:100px}
	select{margin: 0}
	.production-order-create .alert{text-align:left}
	.production-order-create .quantity{width:50px;}
	.production-order-create .available_inventory_properties{max-height:510px; overflow:auto; font-size:12px }
	small.error{ display:block}
	</style>
@stop
