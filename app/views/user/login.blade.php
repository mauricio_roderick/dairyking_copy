@extends('template.plain')

@section('content')
	
	<div class="span4 offset4">
		<form method="POST" class="form-login">
			<h2 class="form-signin-heading">Login</h2>
			{{ Form::label('username', 'Username') }}
			{{ Form::text('username', Input::get('username')) }}
			{{ Form::label('password', 'Password') }}
			{{ Form::password('password', Input::old('name')) }}
			{{ Form::submit('Login', array('class' => 'btn btn-large btn-primary')) }}
		</form>
	</div>
@stop
