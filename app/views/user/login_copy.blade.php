@extends('template.plain')

@section('body')
	
	<div class="row user_login">
		<h2>Login</h2>
		<div class="large-4 columns">&nbsp;</div>
			<div class="large-4 columns">
				<form method="POST">
					{{ Form::label('username', 'Username') }}
					{{ Form::text('username', Input::get('username')) }}
					{{ Form::label('password', 'Password') }}
					{{ Form::password('password', Input::old('name')) }}
					{{ Form::submit('Login', array('class' => 'small button')) }}
				</form>
			</div>
		<div class="large-4 columns">&nbsp;</div>
    </div>
@stop
