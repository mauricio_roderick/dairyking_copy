@extends('template.main')

@section('title')
Create User
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>First name</th>
				<td>
					{{ Form::text('firstname', Input::get('firstname')) }}
					{{ $errors->first('firstname', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Last name</th>
				<td>
					{{ Form::text('lastname', Input::get('lastname')) }}
					{{ $errors->first('lastname', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Username</th>
				<td>
					{{ Form::text('email', Input::get('email')) }}
					{{ $errors->first('email', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Account Type</th>
				<td>
					{{ Form::select('type', $user_types, Input::get('type')) }}
					{{ $errors->first('type', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Password</th>
				<td>
					{{ Form::password('password', Input::get('password')) }}
					{{ $errors->first('password', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<!--<tr>
				<th>Permissions</th>
				<td>{{ $permissions }}</td>
			</tr>-->
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
@stop
