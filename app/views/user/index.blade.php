@extends('template.main')

@section('title')
Users
@stop

@section('page-nav')
<a href="{{ url('admin/user/create') }}">Create User</a>
@stop

@section('content')
@if($users->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>Name</th>
			<th width="300">Username</th>
			<th width="300">User type</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $user)
			<tr>
				<td><a href="{{ url('admin/user/view/'.$user->id) }}" >{{ $user->firstname.' '.$user->lastname }}</a></td>
				<td>{{ $user->email }}</td>
				<td>{{ $user->type }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/user/view/'.$user->id) }}" >View</a></li>
						<li><a href="{{ url('admin/user/update/'.$user->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/user/delete/'.$user->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No user found.' }}
@endif
@stop