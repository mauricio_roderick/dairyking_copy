@extends('template.main')

@section('title')
View User
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>First name</th>
				<td>{{ $user['firstname'] }}</td>
			</tr>
			<tr>
				<th>Last name</th>
				<td>{{ $user['lastname'] }}</td>
			</tr>
			<tr>
				<th>Username</th>
				<td>{{ $user['email'] }}</td>
			</tr>
			<tr>
				<th>User type</th>
				<td>{{ $user['type'] }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/user/update/'.$user['id']) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/user') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
