@extends('template.main')

@section('title')
Purchase Requisitions
@stop

@section('page-nav')
<a href="{{ url('admin/pr/create') }}">Create Purchase Requisition</a>
@stop

@section('content')
@if($purchase_requisitions->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>PR Id</th>
			<th>Addressed To</th>
			<th>Requested By</th>
			<th>Status</th>
			<th>Approved</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($purchase_requisitions as $purchase_requisition)
			<tr>
				<td><a href="{{ url('admin/pr/view/'.$purchase_requisition->id) }}" >{{ Helper::string_pad($purchase_requisition->id) }}</a></td>
				<td>
					<?php $addressed_to = $purchase_requisition->addressedTo ?>
					{{ $addressed_to ? $addressed_to->firstname.' '.$addressed_to->lastname : '' }}
				</td>
				<td>
					<?php $requester = $purchase_requisition->requester ?>
					{{ $requester ? $requester->firstname.' '.$requester->lastname : '' }}
				</td>
				<td>{{ $purchase_requisition->status }}</td>
				<td>{{ $purchase_requisition->approved }}</td>
				<td>
					<div class="btn-group">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							Action
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li><a href="{{ url('admin/pr/view/'.$purchase_requisition->id) }}">View</a></li>
							<?php if(Auth::user()->type == 'Admin'){ ?>
							<li><a href="{{ url('admin/pr/update/'.$purchase_requisition->id) }}">Edit </a></li>
							<?php } ?>
							<li><a class="delete" href="{{ url('admin/pr/delete/'.$purchase_requisition->id) }}">Delete</a></li>
						</ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop
