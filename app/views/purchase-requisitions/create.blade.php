@extends('template.main')

@section('title')
Create Purchase Requisition
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Department</th>
				<td>
					<select name="loc_id">
						<option value="" >- Select -</option>
						@foreach($locations as $location)
						<option value="{{ $location->id }}" {{ (Input::get('loc_id') == $location->id) ? 'selected="selected"' : '' }} >{{ $location->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('loc_id', ' <small class="error">:message</small>') }}
				</td>
			</tr>
			<tr>
				<th>Addressed to</th>
				<td>
					<select name="addressed_to">
						<option value="">- Select -</option>
						@foreach($users as $user)
						<option value="{{ $user->id }}" <?php echo (Input::get('addressed_to') == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
						@endforeach
					</select>
					{{ $errors->first('addressed_to', ' <small class="error">:message</small>') }}
				</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<div>
						{{ Form::select('inventories', $inventories) }}
						<a class="btn btn-small add_btn">Add Item</a>
					</div>
					<div class="error_cont">{{ $inventory_item_error }}</div>
					<div class="item_cont">
						<table>
							<?php if($inventory_items = Input::get('inventory_item')){ ?>
								@foreach($inventory_items as $index => $inventory_item)
									<tr>
										<td class="padded"><i class="icon-trash">&nbsp;</i></td>
										<td class="padded"><input type="hidden" name="inventory_item[]" value="{{ $inventory_item }}" /> {{ $inventories[$inventory_item] }}</td>
										<td><input type="text" name="quantity[]" placeholder="Quanity" value="{{ $quantity[$index] }}" /></td>
										<td class="padded">{{ $inventory_list[$inventory_item]['unitofdelivery'] .' = '. $inventory_list[$inventory_item]['multiplier'] .' '. $inventory_list[$inventory_item]['unitofuse'] }}</td>
									</tr>
								@endforeach
							<?php } ?>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
		
	<script type="text/javascript">
	var inventory_list = <?php echo json_encode($inventory_list) ?>;
	
		function hide_show_approved_cont()
		{
			var opt_value = $('[name="status"]').find('option:selected').val()
			
			if(opt_value == 'Approved')
				$('.approved_cont').show();
			else
				$('.approved_cont').hide();
		}
		
		hide_show_approved_cont()
		$(document).ready(function() {
			$('[name="status"]').change(function(){
				hide_show_approved_cont()
			})
			
			$('.alert').click(function(){
				$('.approved_cont').hide();
			})
			
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var inv_id = $("[name='inventories']").find('option:selected');
				
				if(inv_id.val() != 0)
				{
					if(typeof inventory_list[inv_id.val()] == 'undefined')
					{
						alert('Unknown Inventory');
						return;
					}
					
					var itenventory = inventory_list[inv_id.val()];
					
					if($('.item_cont [name="inventory_item[]"][value="' + inv_id.val() + '"]').size() > 0)
					{
						alert('Item "' + inv_id.html() + '" is laready on your list');
						return;
					}
					
					var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="inventory_item[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td><input type="text" name="quantity[]" placeholder="Quanity" /></td><td class="padded">' + itenventory.unitofdelivery + ' = ' + itenventory.multiplier + ' ' + itenventory.unitofuse + '</td></tr>';
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select an item.');
				}
			})
		});
	</script>
	
	<style type="text/css">
		.items_lbl{padding:5px 0 10px}
		.icon-trash{cursor:pointer; padding: 0 20px 0 0}
		.inv_item{padding:3px 0}
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
	</style>
@stop
