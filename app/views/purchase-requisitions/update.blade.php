@extends('template.main')

@section('title')
Update Purchase Requisition
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Requested by</th>
				<td>
					<?php $requester = $purchase_requisition_orig->requester ?>
					{{ $requester ? $requester->firstname.' '.$requester->lastname : ''}}
				</td>
			</tr>
			<tr>
				<th>Approved</th>
				<td>
					{{ Form::select('approved', array('' => '-- Select --', 'Yes' => 'Yes', 'No' => 'No'), $purchase_requisition['approved']) }}
				</td>
			</tr>
			<tr>
				<th>Department</th>
				<td>
					<select name="loc_id">
						<option value="" >- Select -</option>
						@foreach($locations as $location)
						<option value="{{ $location->id }}" {{ ($purchase_requisition['loc_id'] == $location->id) ? 'selected="selected"' : '' }} >{{ $location->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('loc_id', ' <small class="error">:message</small>') }}
				</td>
			</tr>
			<tr>
				<th>Addressed to</th>
				<td>
					<select name="addressed_to">
						<option value="">- Select -</option>
						@foreach($users as $user)
						<option value="{{ $user->id }}" <?php echo ($purchase_requisition['addressed_to'] == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
						@endforeach
					</select>
					{{ $errors->first('addressed_to', ' <small class="error">:message</small>') }}
				</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<div>
						{{ Form::select('inventories', $inventories) }}
						<a class="btn btn-small add_btn">Add Item</a>
					</div>
					<div class="error_cont">{{ $inventory_item_error }}</div>
					<div class="item_cont">
						<table>
						<?php if(! Input::all() && isset($inventory_items)){ ?>
							@foreach($inventory_items as $index => $inventory_item)
								<tr>
									<td class="padded"><i class="icon-trash">&nbsp;</i></td>
									<td class="padded">
										<input type="hidden" name="inventory_item[]" value="{{ $inventory_item }}" /> {{ $inventories[$inventory_item] }}
										<input type="hidden" name="id[]" value="{{ $id[$index] }}" />
										<input type="hidden" name="unitofuse[]" value="{{ $unitofuse[$index] }}" />
										<input type="hidden" name="multiplier[]" value="{{ $multiplier[$index] }}" />
										<input type="hidden" name="unitofdelivery[]" value="{{ $unitofdelivery[$index] }}" />
									</td>
									<td><input type="text" name="quantity[]" placeholder="Quanity" value="{{ $quantity[$index] }}" autocomplete="off" /></td>
									<td>{{ $unitofdelivery[$index] .' = '. $multiplier[$index] .' '. $unitofuse[$index] }}</td>
								</tr>
							@endforeach
						<?php }else{ 
							$inventory_items = Input::get('inventory_item');
							$id = Input::get('id');
							$quantity = Input::get('quantity');
							$unit = Input::get('unit');
							$unitofuse = Input::get('unitofuse');
							$multiplier = Input::get('multiplier');
							$unitofdelivery = Input::get('unitofdelivery');
							
							if($inventory_items){
						?>
							@foreach($inventory_items as $index => $inventory_item)
								<tr>
									<td class="padded"><i class="icon-trash">&nbsp;</i></td>
									<td class="padded">
										<input type="hidden" name="inventory_item[]" value="{{ $inventory_item }}" /> {{ $inventories[$inventory_item] }}
										<input type="hidden" name="id[]" value="{{ $id[$index] }}" />
										<input type="hidden" name="unitofuse[]" value="{{ $unitofuse[$index] }}" />
										<input type="hidden" name="multiplier[]" value="{{ $multiplier[$index] }}" />
										<input type="hidden" name="unitofdelivery[]" value="{{ $unitofdelivery[$index] }}" />
									</td>
									<td><input type="text" name="quantity[]" placeholder="Quanity" value="{{ $quantity[$index] }}" autocomplete="off" /></td>
									<td class="padded">
										<?php if($id[$index] == ''){ ?>
											{{ $inventory_list[$inventory_item]['unitofdelivery'] .' = '. $inventory_list[$inventory_item]['multiplier'] .' '. $inventory_list[$inventory_item]['unitofuse'] }}
										<?php }else{ ?>
											{{ $unitofdelivery[$index] .' = '. $multiplier[$index] .' '. $unitofuse[$index] }}
										<?php } ?>
									
									</td>
								</tr>
							@endforeach
						<?php }
						}
						?>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Update" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	var inventory_list = <?php echo json_encode($inventory_list) ?>;
	
		$(document).ready(function() {
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var inv_id = $("[name='inventories']").find('option:selected');
				
				if(inv_id.val() != 0)
				{
					if(typeof inventory_list[inv_id.val()] == 'undefined')
					{
						alert('Unknown Inventory');
						return;
					}
					
					if($('.item_cont [name="inventory_item[]"][value="' + inv_id.val() + '"]').size() > 0)
					{
						alert('Item "' + inv_id.html() + '" is laready on your list');
						return;
					}
					
					var itenventory = inventory_list[inv_id.val()];
						
					var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="id[]" /><input type="hidden" name="unitofuse[]" value="" /><input type="hidden" name="multiplier[]" value="" /><input type="hidden" name="unitofdelivery[]" value="" /><input type="hidden" name="inventory_item[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td><input type="text" name="quantity[]" placeholder="Quanity" autocomplete="off" /></td><td class="padded">' + itenventory.unitofdelivery + ' = ' + itenventory.multiplier + ' ' + itenventory.unitofuse + '</td></tr>';
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select an item.');
				}
			})
		});
	</script>
	
	<style type="text/css">
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
	</style>
@stop