@extends('template.main')

@section('title')
View Purchase Requisition
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Department</th>
				<td>
					<?php $location = $purchase_requisition->loc ?>
					{{ $location ? $location->name : ''}}
				</td>
			</tr>
			<tr>
				<th>Requested by</th>
				<td>
					<?php $requester = $purchase_requisition->requester ?>
					{{ $requester ? $requester->firstname.' '.$requester->lastname : ''}}
				</td>
			</tr>
			<tr>
				<th>Addressed to</th>
				<td>
					<?php $addressedTo = $purchase_requisition->addressedTo ?>
					{{ $addressedTo ? $addressedTo->firstname.' '.$addressedTo->lastname : ''}}
				</td>
			</tr>
			<tr>
				<th>Status</th>
				<td>
					{{ $purchase_requisition->status }}
				</td>
			</tr>
			<tr>
				<th>Approved</th>
				<td>
					{{ $purchase_requisition->approved }}
				</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<div class="item_cont">
						<table>
							<tr>
								<th>Item name</th>
								<th>Quantity</th>
								<th>Conversion</th>
							</tr>
							<?php foreach($purchaseitems as $purchaseitem){ ?>
								<?php $inv = $purchaseitem->inv ?>
								<tr>
								<td>{{ $inv ? $inv->name : '&nbsp;' }}</td>
								<td>{{ $purchaseitem->quantity }}</td>
								<td>{{ $purchaseitem->unitofdelivery .' = '. $purchaseitem->multiplier. ' '. $purchaseitem->unitofuse }}</td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/pr/update/'.$purchase_requisition->id) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/pr') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	
	<style type="text/css">
		.item_cont td, .table-form .item_cont th{border:0; text-align:center}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
	</style>	
@stop
