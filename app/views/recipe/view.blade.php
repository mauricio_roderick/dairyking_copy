@extends('template.main')

@section('title')
View Recipe
@stop

@section('content')
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Inventory<br/> (Finish Product)</th>
				<td>
					<?php
						$invenoty_name = $recipe->inv;
						$invenoty_name = $invenoty_name ? $invenoty_name->name : '';
						echo $invenoty_name;
					?>
				</td>
			</tr>
			<tr>
				<th>Quantity</th>
				<td>
					{{ $recipe->multiplier.' '.$recipe->unit_of_use }}
				</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>
					{{ $recipe->description }}
				</td>
			</tr>
			<tr>
				<th>Components</th>
				<td>
					<table class="item_cont">
					<?php $recipe_items = $recipe->recipeItems ?>
					<?php 
					if($recipe_items)
					{
						
						foreach($recipe_items as $recipe_item)
						{
						$inventory = $recipe_item->inventory;
					?>
						<tr>
							<td> {{ $inventory ? $inventory->name : '' }}</td>
							<td >{{ $inventory ? '(' . $inventory->unitofdelivery.' = '.$inventory->multiplier.' '.$inventory->unitofuse . ')' : '' }}</td>
							<td >{{ ' - ' }}</td>
							<td >
								{{ $recipe_item->quantity }}
								{{ $inventory ? ' '.$inventory->unitofuse : '' }}
							</td>
						</tr>
					<?php
						}
					}
					?>
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/recipe/update/'.$recipe->id) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/recipe') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	<style type="text/css">
		.items_lbl{padding:5px 0 10px}
		.icon-trash{cursor:pointer; padding: 0 20px 0 0}
		.inv_item{padding:3px 0}
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
		select{margin: 0}
	</style>
@stop
