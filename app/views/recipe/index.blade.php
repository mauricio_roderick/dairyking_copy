@extends('template.main')

@section('title')
Recipe
@stop

@section('page-nav')
<a href="{{ url('admin/recipe/create') }}">Create Recipe</a>
@stop

@section('content')
@if(count($recipes))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Name</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($recipes as $recipe)
			<?php
				$invenoty_name = $recipe->inv;
				$invenoty_name = $invenoty_name ? $invenoty_name->name : '';
			?>
			<tr>
				<td><a href="{{ url('admin/recipe/view/'.$recipe->id) }}" >{{ $invenoty_name }}</a></td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/recipe/view/'.$recipe->id) }}" >View</a></li>
						<li><a href="{{ url('admin/recipe/update/'.$recipe->id) }}" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/recipe/delete/'.$recipe->id) }}" >Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop