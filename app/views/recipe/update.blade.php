@extends('template.main')

@section('title')
Update Recipe
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Inventory<br/> (Finish Product)</th>
					<td>
						{{ Form::select('finish_prod_id', $finish_products, $recipe['finish_prod_id']) }}
						{{ $errors->first('finish_prod_id', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Description</th>
					<td>
						{{ Form::textarea('description', $recipe['description'], array('style' => 'width:500px')) }}
						{{ $errors->first('description', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Components</th>
					<td>
						<div>
							{{ Form::select('inventories', $inventories) }}
							<a class="btn btn-small add_btn">Add Item</a>
						</div>
						<div class="error_cont">{{ $inventory_item_error }}</div>
						<table class="item_cont">
							<?php if(! Input::all() && isset($recipe_inv_item)){ ?>
								@foreach($recipe_inv_item as $index => $inventory_item)
									<tr>
										<td class="padded"><i class="icon-trash">&nbsp;</i></td>
										<td class="padded">
											<input type="hidden" name="id[]" value="{{ $inventory_item->id }}" />
											<input type="hidden" name="inventory_item[]" value="{{ $inventory_item->inv_id }}" /> 
											{{ isset($inventories[$inventory_item->inv_id]) ? $inventories[$inventory_item->inv_id] : '' }}
										</td>
										<td class="padded">{{ ($inventory_item->type == 'Raw') ? 'Quantity in '. $inventory_item->unit : 'Quantity ('.$inventory_item->multiplier.' '.$inventory_item->unit.')' }}</td>
										<td><input type="text" name="quantity[]" value="{{ $inventory_item->quantity }}" autocomplete="off" /></td>
									</tr>
								@endforeach
							<?php }else{ 
								$id = Input::get('id');
								$post_inventory_items = Input::get('inventory_item');
								$quantity = Input::get('quantity');
								
								if($post_inventory_items){
							?>
								@foreach($post_inventory_items as $index => $inventory_item)
									<tr>
										<td class="padded"><i class="icon-trash">&nbsp;</i></td>
										<td class="padded">
											<input type="hidden" name="inventory_item[]" value="{{ $inventory_item }}" />
											<input type="hidden" name="id[]" value="{{ $id[$index] }}" />
											{{ $inventories[$inventory_item] }}
										</td>
										<td class="padded">
											<?php
												if($id[$index] == '')
												{
													echo ($inventory_list[$inventory_item]['type'] == 'Raw') ? 'Quantity in '. $inventory_list[$inventory_item]['multiplier'] : 'Quantity ('. $inventory_list[$inventory_item]['multiplier'].' '.$inventory_list[$inventory_item]['unitofuse'].')';
												}
												else
												{
													echo ($recipe_inv_item[$id[$index]]->type == 'Raw') ? 'Quantity in '. $recipe_inv_item[$id[$index]]->multiplier : 'Quantity ('. $recipe_inv_item[$id[$index]]->multiplier.' '.$recipe_inv_item[$id[$index]]->unitofuse.')';
												}
											?>
										</td>
										<td><input type="text" name="quantity[]" value="{{ $quantity[$index] }}" autocomplete="off" /></td>
									</tr>
								@endforeach
							<?php }
							}
							?>
						</table>
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	var inventory_list = <?php echo json_encode($inventory_list) ?>;
	
		$(document).ready(function() {
			
			$('.item_cont').delegate('.icon-trash', 'click', function(){
				$(this).parent().parent().remove();
			})
			
			$('.add_btn').click(function(){
				var inv_id = $("[name='inventories']").find('option:selected');
				
				if(inv_id.val() != 0)
				{
					if(typeof inventory_list[inv_id.val()] == 'undefined')
					{
						alert('Unknown Inventory');
						return;
					}
					
					var itenventory = inventory_list[inv_id.val()];
					
					if($('.item_cont [name="inventory_item[]"][value="' + inv_id.val() + '"]').size() > 0)
					{
						alert('Item "' + inv_id.html() + '" is laready on your list');
						return;
					}
					
					if(itenventory.type == 'Raw')
					{
						var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="id[]" /><input type="hidden" name="unitofuse[]" value="" /><input type="hidden" name="multiplier[]" value="" /><input type="hidden" name="inventory_item[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td class="padded">Quantity in ' + itenventory.unitofuse + '</td><td><input type="text" name="quantity[]" autocomplete="off" /></td></tr>';
					}
					else
					{
						var to_append = '<tr><td class="padded"><i class="icon-trash">&nbsp;</i></td><td class="padded"><input type="hidden" name="id[]" /><input type="hidden" name="unitofuse[]" value="" /><input type="hidden" name="multiplier[]" value="" /><input type="hidden" name="inventory_item[]" value="' + inv_id.val() + '" />' + inv_id.html() + '</td><td class="padded">Quantity (' + itenventory.multiplier + ' ' + itenventory.unitofuse + ')' + '</td><td><input type="text" name="quantity[]" autocomplete="off" /></td></tr>';
					}
					
					$('.item_cont').append(to_append);
				}
				else
				{
					alert('Please select an item.');
				}
			})
		});
	</script>
	
	<style type="text/css">
		.items_lbl{padding:5px 0 10px}
		.icon-trash{cursor:pointer; padding: 0 20px 0 0}
		.inv_item{padding:3px 0}
		.error_cont{padding-top:10px}
		.error_cont .error{display:block;}
		.item_cont td{border:0}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
		select{margin: 0}
	</style>
@stop
