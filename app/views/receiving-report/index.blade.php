@extends('template.main')

@section('title')
Receiving Report
@stop

@section('page-nav')
<a href="{{ url('admin/receiving-report/batch-create') }}">Create Receiving Report</a>
@stop

@section('content')
@if(count($receivingreports))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>RR Id</th>
				<th width="300">PO Id</th>
				<th width="300">PO Item Id</th>
				<th width="300">Inventory</th>
				<th width="300">Received By</th>
				<th width="300">Date received</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($receivingreports as $report)
			<?php
				$user = $report->user;
				$po_item = $report->poItem;
				$pr_item = $po_item ? $po_item->prItem : false;
				$inventory = $pr_item ? $pr_item->inv : false;
			?>
			<tr>
				<td><a href="{{ url('admin/receiving-report/view/'.$report->id) }}" >{{ Helper::string_pad($report->id) }}</a></td>
				<td>{{ Helper::string_pad($report->po_id) }}</td>
				<td>{{ Helper::string_pad($report->po_item_id) }}</td>
				<td>{{ $inventory ? $inventory->name : '' }}</td>
				<td>{{ $user ? $user->firstname.' '.$user->lastname : '' }} </td>
				<td>{{ $report->datereceived }} </td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/receiving-report/view/'.$report->id) }}" >View</a></li>
						<?php if(Auth::user()->type == 'Admin'){ ?>
						<li><a href="{{ url('admin/receiving-report/edit/'.$report->id) }}" class="button small secondary" >Edit</a></li>
						<?php } ?>
						<li><a class="delete" href="{{ url('admin/receiving-report/delete/'.$report->id) }}">Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop