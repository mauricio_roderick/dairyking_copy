@extends('template.main')

@section('title')
View Receiving Report
@stop

@section('content')
	<?php 
		$user = $receivingreport->user;
		$po_item = $receivingreport->poItem;
		$fromLocation = $receivingreport->fromLocation;
		$fromSubLocation = $receivingreport->fromSubLocation;
		$toLocation = $receivingreport->toLocation;
		$toSubLocation = $receivingreport->toSubLocation;
		$pr_item = $po_item ? $po_item->prItem : false;
		$inventory = $pr_item ? $pr_item->inv : false;
	?>
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>RR Id</th>
				<td>{{ Helper::string_pad($receivingreport->id) }}</td>
			</tr>
			<tr>
				<th>PO Id</th>
				<td>{{ Helper::string_pad($receivingreport->po_id) }}</td>
			</tr>
			<tr>
				<th>PO Item Id</th>
				<td>{{ Helper::string_pad($receivingreport->po_item_id) }}</td>
			</tr>
			<tr>
				<th>PR Item Id</th>
				<td>{{ $pr_item ? Helper::string_pad($pr_item->id) : '' }}</td>
			</tr>
			<tr>
				<th>Inventory</th>
				<td>{{ $inventory ? $inventory->name : '' }}</td>
			</tr>
			<tr>
				<th>Quantity Received</th>
				<td>{{ $receivingreport->quantity. ' '. $receivingreport->unitofdelivery .' = '. ( $receivingreport->quantity * $receivingreport->multiplier ) . ' ' . $receivingreport->unitofuse }}</td>
			</tr>
			<tr>
				<th>Remaining Quantity</th>
				<td>{{ $receivingreport->available_quantity_to_use.' '.$receivingreport->unitofuse }}</td>
			</tr>
			<tr>
				<th>From Department</th>
				<td>{{ $fromLocation ? $fromLocation->name : '' }}</td>
			</tr>
			<tr>
				<th>From Sub Department</th>
				<td>{{ $fromSubLocation ? $fromSubLocation->name : '' }}</td>
			</tr>
			<tr>
				<th>To Department</th>
				<td>{{ $toLocation ? $toLocation->name : '' }}</td>
			</tr>
			<tr>
				<th>To Sub Department</th>
				<td>{{ $toSubLocation ? $toSubLocation->name : '' }}</td>
			</tr>
			<tr>
				<th>Received By</th>
				<td>{{ $user ? $user->firstname.' '.$user->lastname : '' }}</td>
			</tr>
			<tr>
				<th>Date received</th>
				<td>{{ $receivingreport->datereceived }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<?php if(Auth::user()->type == 'Admin'){ ?>
					<a href="{{ url('admin/receiving-report/edit/'.$receivingreport->id) }}" class="btn btn-primary" >Edit</a>
					<?php } ?>
					<a href="{{ url('admin/receiving-report') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
