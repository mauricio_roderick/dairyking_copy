@extends('template.main')

@section('title')
Create Receiving Report
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Purchase Order</th>
					<td>
						<select name="po_id">
							<option value="">- Purchase Order -</option>
							@foreach($purchaseorders as $purchaseorder)
							<option value="{{ $purchaseorder->id }}" <?php echo (Input::get('po_id') == $purchaseorder->id) ? 'selected="selected"' : '' ?> >{{ Helper::string_pad($purchaseorder->id) }}</option>
							@endforeach
						</select>
						{{ $errors->first('po_id', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Purchase Order Item/s</th>
					<td>
						{{ $item_error }}
						<div class="items_cont">
							<?php 
								$po_id = Input::get('po_id');
								$po_item_ids = Input::get('po_item_id');
								$quantity = Input::get('quantity');
								$location = Input::get('location');
								$sub_location = Input::get('sub_location');
								
								if($po_id)
								{
									$purchaseorderitems = PurchaseOrderItem::where('po_id', '=', $po_id)->get();
									
									if($purchaseorderitems->count())
									{
										
							?>
										<table>
										<tr>
											<th></th>
											<th>Quantity to Receive</th>
											<th>Location</th>
											<th>Inventory</th>
											<th>Conversion</th>
											<th>Ordered Quantity</th>
											<th>Received Quantity</th>
											<th>Pending Quantity</th>
										</tr>
									<?php			
										foreach($purchaseorderitems as $index => $purchaseorderitem)
										{
											$pr_item = $purchaseorderitem->prItem;
											$inventory = $pr_item ? $pr_item->inv : '';
											$received_qty = DB::table('receivingreports')->where('po_item_id', $purchaseorderitem->id)->where('received_by', '>', 0)->sum('quantity');
											$received_qty = ($received_qty == '') ? 0 : $received_qty;
											$remaining_qty = $purchaseorderitem->quantity - $received_qty;
											$checked = ($po_item_ids && in_array($purchaseorderitem->id, $po_item_ids));
											
											$qty_value = (isset($quantity[$index])) ? $quantity[$index] : '';
											$location_value = (isset($location[$index])) ? $location[$index] : '';
											$sub_location_value = (isset($sub_location[$index])) ? $sub_location[$index] : '';
											$qty_attributes = array('autocomplete' => 'off');
											
											$sub_location_options = isset($sub_locations[$location_value]) ? $sub_locations[$location_value] : array();
											$sub_location_options[0] = '- Sub Department -';
											ksort($sub_location_options);
									?>
											<tr>
												<td>{{ ($received_qty < $pr_item->quantity) ? Form::checkbox('po_item_id[]', $purchaseorderitem->id, $checked) : '' }}</td>
												<td>{{ ($received_qty < $pr_item->quantity) ? Form::text('quantity[]', $qty_value , $qty_attributes) : '' }}</td>
												<td>{{ ($received_qty < $pr_item->quantity) ? Form::select('location[]', $locations_array, $location_value). Form::select('sub_location[]', $sub_location_options, $sub_location_value) : '' }}</td>
												<td>{{ $inventory ? $inventory->name : '' }}</td>
												<td>{{ $pr_item->unitofdelivery .' = '. $pr_item->multiplier. ' '. $pr_item->unitofuse }}</td>
												<td>{{ $purchaseorderitem->quantity }}</td>
												<td>{{ $received_qty }}</td>
												<td>{{ $remaining_qty }}</td>
											</tr>
									<?php 
										} 
									?>
										</table>
							<?php 	} 
									else
									{
										echo 'No items found.'; 
									} 
								} 
							?>
						</div>
					</td>
				</tr>
				<tr>
					<th>Received by</th>
					<td>
						<select name="received_by">
							<option value="">- Select -</option>
							@foreach($users as $user)
							<option value="{{ $user->id }}" <?php echo (Input::get('received_by') == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
							@endforeach
						</select>
						{{ $errors->first('received_by', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Date received</th>
					<td>
						{{ Form::text('datereceived', Input::get('datereceived'), array('id' => 'datepicker', 'autocomplete' => 'off')) }}
						{{ $errors->first('datereceived', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	$(window).load(function() {
		var sub_location = <?php echo json_encode($sub_locations) ?>;
		
		function show_items()
		{
			var po_id = $('[name="po_id"]').find('option:selected').val();
			if(po_id != '')
			{
				$(".items_cont").html("Loading...");
				$.ajax({
					type	: "POST",
					cache	: false,
					url		: "<?php echo url("admin/receiving-report/get-items-for-receiving")?>",
					data: ({ 'po_id' :  po_id }),
					success: function(data) {
						$('.items_cont').html(data);
					}
				});
			}
			else
			{
				$('.items_cont').html('');
			}
		}
		
		$("[name='po_id']").change( function(){
			show_items()
		});
		
		$("body").delegate( "[name='location[]']" , 'change', function(){
			var parent_location_id = $(this).find(":selected").val();
			var sub_location_options = '<option value="0">- Sub Department -</otpion>';
			
			if(typeof sub_location[parent_location_id] != 'undefined')
			{
				sub_locations = sub_location[parent_location_id];
				for (x in sub_locations)
				{
					sub_location_options += '<option value="' + x + '">' + sub_locations[x] + '</otpion>';
				}
			}
			
			$(this).next("select").html(sub_location_options)
		});
	})
	</script>
	
	<style type="text/css">
		.table-form .items_cont td, .table-form .items_cont th{border:0; text-align:left; font-size:12px; padding:5px}
		.items_cont tr td:first-child{text-align:center}
		.items_cont input{width:30px}
		.items_cont select{width:150px; padding:0px; height:auto;}
	</style>
@stop
