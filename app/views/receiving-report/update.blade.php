@extends('template.main')

@section('title')
Update Receiving Report
@stop

@section('content')
	<?php 
		$user = $receivingreport->user;
		$po_item = $receivingreport->poItem;
		$fromLocation = $receivingreport->fromLocation;
		$fromSubLocation = $receivingreport->fromSubLocation;
		$toLocation = $receivingreport->toLocation;
		$toSubLocation = $receivingreport->toSubLocation;
		$pr_item = $po_item ? $po_item->prItem : false;
		$inventory = $pr_item ? $pr_item->inv : false;
	?>
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>RR Id</th>
					<td>{{ Helper::string_pad($receivingreport->id) }}</td>
				</tr>
				<tr>
					<th>PO Id</th>
					<td>{{ Helper::string_pad($receivingreport->po_id) }}</td>
				</tr>
				<tr>
					<th>PO Item Id</th>
					<td>{{ Helper::string_pad($receivingreport->po_item_id) }}</td>
				</tr>
				<tr>
					<th>PR Item Id</th>
					<td>{{ $pr_item ? Helper::string_pad($pr_item->id) : '' }}</td>
				</tr>
				<tr>
					<th>Inventory</th>
					<td>{{ $inventory ? $inventory->name : '' }}</td>
				</tr>
				<tr>
					<th>Quantity Received</th>
					<td>{{ $receivingreport->quantity. ' '. $receivingreport->unitofdelivery .' = '. ( $receivingreport->quantity * $receivingreport->multiplier ) . ' ' . $receivingreport->unitofuse }}</td>
				</tr>
				<tr>
					<th>Remaining Quantity</th>
					<td>{{ $receivingreport->available_quantity_to_use.' '.$receivingreport->unitofuse }}</td>
				</tr>
				<tr>
					<th>From Department</th>
					<td>{{ $fromLocation ? $fromLocation->name : '' }}</td>
				</tr>
				<tr>
					<th>From Sub Department</th>
					<td>{{ $fromSubLocation ? $fromSubLocation->name : '' }}</td>
				</tr>
				<tr>
					<th>To Department</th>
					<td>{{ $toLocation ? $toLocation->name : '' }}</td>
				</tr>
				<tr>
					<th>To Sub Department</th>
					<td>{{ $toSubLocation ? $toSubLocation->name : '' }}</td>
				</tr>
				<tr>
					<th>Received by</th>
					<td>
						<select name="received_by">
							<option value="">- Select -</option>
							@foreach($users as $user)
							<option value="{{ $user->id }}" <?php echo ($form_val['received_by'] == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
							@endforeach
						</select>
						{{ $errors->first('received_by', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Date received</th>
					<td>
						{{ Form::text('datereceived', $form_val['datereceived'], array('id' => 'datepicker')) }}
						{{ $errors->first('datereceived', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
