@extends('template.main')

@section('title')
Create Receiving Report
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Purchase Order</th>
					<td>
						<select name="po_id">
							<option value="0">- Purchase Order -</option>
							@foreach($purchaseorders as $purchaseorder)
							<option value="{{ $purchaseorder->id }}" <?php echo (Input::get('po_id') == $purchaseorder->id) ? 'selected="selected"' : '' ?> >{{ Helper::string_pad($purchaseorder->id) }}</option>
							@endforeach
						</select>
						{{ $errors->first('po_id', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Purchase Order Item</th>
					<td>
						<div class="items_cont">
						<?php 
							$po_id = Input::get('po_id');
							if($po_id)
							{
								$po_items = PurchaseOrderItem::where('po_id', $po_id)->get();
						?>
							<select name="po_item_id">
								<option value="">- Select -</option>
								<?php 
									foreach($po_items as $po_item){ 
									
									$pr_item = $po_item->prItem;
									$inventory = $pr_item ? $pr_item->inv : false;
									$inventory_name = $inventory ? '('.$inventory->name.')' : '';
									$selected = (Input::get('po_item_id') == $po_item->id ) ? 'selected="selected"' : '';
								?>
								<option value="{{ $po_item->id }}" {{ $selected }} >{{ Helper::string_pad($po_item->id).$inventory_name }}</option>
								<?php } ?>
							</select>
						<?php
							}
						?>
						{{ $errors->first('po_item_id', ' <span class="help-inline">:message</span>') }}
						</div>
					</td>
				</tr>
				<tr>
					<th>Received by</th>
					<td>
						<select name="received_by">
							<option value="">- Select -</option>
							@foreach($users as $user)
							<option value="{{ $user->id }}" <?php echo (Input::get('received_by') == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
							@endforeach
						</select>
						{{ $errors->first('received_by', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Date received</th>
					<td>
						{{ Form::text('datereceived', Input::get('datereceived'), array('id' => 'datepicker')) }}
						{{ $errors->first('datereceived', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				{{--<tr>
					<th>Time received</th>
					<td>
						{{ Form::text('timereceived', Input::get('timereceived')) }}
						{{ $errors->first('timereceived', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>--}}
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	$(window).load(function() {
	
		function show_items()
		{
			var po_id = $('[name="po_id"]').find('option:selected').val();
			var po_item_id = "<?php echo Input::get('po_item_id') ?>";
			if(po_id != '')
			{
				$(".items_cont").html("Loading...");
				$.ajax({
					type	: "POST",
					cache	: false,
					url		: "<?php echo url("admin/receiving-report/get-po-items")?>",
					data: ({ 'po_id' :  po_id, 'po_item_id' : po_item_id }),
					success: function(data) {
						$('.items_cont').html(data);
					}
				});
				
			}
		}
		
		$("[name='po_id']").change( function(){
			show_items()
		});
	})
	</script>
@stop
