@extends('template.main')

@section('title')
Create Customer
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', Input::get('name')) }}
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Contact Person</th>
					<td>
						{{ Form::text('contact_person', Input::get('contact_person')) }}
						{{ $errors->first('contact_person', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Contact Number</th>
					<td>
						{{ Form::text('contact_num', Input::get('contact_num')) }}
						{{ $errors->first('contact_num', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
