@extends('template.main')

@section('title')
Add Customer Item
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Customer</th>
					<td>
						{{ $customer->name }}
					</td>
				</tr>
				<tr>
					<th>Item</th>
					<td>
						{{ Form::select('item_id', $item_inventories, Input::get('item_id')) }}
						{{ $errors->first('item_id', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Quantity</th>
					<td>
						{{ Form::text('quantity', Input::get('quantity')) }}
						{{ $errors->first('quantity', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Date</th>
					<td>
						{{ Form::text('date', Input::get('date'), array('id' => 'datepicker', 'autocomplete' => 'off')) }}
						{{ $errors->first('date', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Comments</th>
					<td>
						{{ Form::textarea('comments', Input::get('comments')) }}
						{{ $errors->first('comments', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
