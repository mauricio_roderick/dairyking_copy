@extends('template.main')

@section('title')
Edit Customer
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>
						{{ Form::text('name', $customer['name']) }}
						{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Contact Person</th>
					<td>
						{{ Form::text('contact_person', $customer['contact_person']) }}
						{{ $errors->first('contact_person', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Contact Number</th>
					<td>
						{{ Form::text('contact_num', $customer['contact_num']) }}
						{{ $errors->first('contact_num', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
@stop
