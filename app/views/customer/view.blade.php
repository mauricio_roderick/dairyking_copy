@extends('template.main')

@section('title')
View Customer
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>{{ $customer['name'] }}</td>
			</tr>
			<tr>
				<th>Contact Person</th>
				<td>{{ $customer['contact_person'] }}</td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td>{{ $customer['contact_num'] }}</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<table class="item_cont">
						<tr>
							<th>Item</th>
							<th>Quantity</th>
							<th>Date</th>
							<th>Comments</th>
						</tr>
						@foreach($customer_items as $customer_item)
						<?php
							$item_name = $customer_item->itemInventory;
							$item_name = $item_name ? $item_name->name : '';
						?>
						<tr>
							<td>{{ $item_name }}</td>
							<td>{{ $customer_item->quantity }}</td>
							<td>{{ Helper::format_date($customer_item->date) }}</td>
							<td>{{ $customer_item->comments }}</td>
						</tr>
						
						@endforeach
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/customer/update/'.$customer['id']) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/customer') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	<style type="text/css">
		.item_cont td, .table-form .item_cont th{border:0; text-align:left; min-width:100px}
		.item_cont .padded{padding:15px 10px 10px}
	</style>
@stop
