@extends('template.main')

@section('title')
Customer
@stop

@section('page-nav')
<a href="{{ url('admin/customer/create') }}">Create Customer</a>
@stop

@section('content')
@if($customers->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Location</th>
				<th>Contact Person</th>
				<th>Contact Num</th>
				<th width="60"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($customers as $customer)
			<tr>
				<td><a href="{{ url('admin/customer/view/'.$customer->id) }}" >{{ $customer->name }}</a></td>
				<td>{{ $customer->contact_person }}</td>
				<td>{{ $customer->contact_num }}</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/customer/view/'.$customer->id) }}" >View</a></li>
						<li><a href="{{ url('admin/customer/update/'.$customer->id) }}" >Edit</a></li>
						<li><a href="{{ url('admin/customer/add-item/'.$customer->id) }}" >Add Item</a></li>
						<li><a class="delete" href="{{ url('admin/customer/delete/'.$customer->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop