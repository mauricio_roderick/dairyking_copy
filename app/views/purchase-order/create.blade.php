@extends('template.main')

@section('title')
Create Purchase Order
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Purchase Requsition</th>
				<td>
					<select name="pr_id">
						<option value="" >- Select -</option>
						@foreach($prs as $pr)
						<option value="{{ $pr->id }}" {{ (Input::get('pr_id') == $pr->id) ? 'selected="selected"' : '' }} >{{ Helper::string_pad($pr->id) }}</option>
						@endforeach
					</select>
					{{ $errors->first('pr_id', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Supplier</th>
				<td>
					<select name="sp_id">
						<option value="">- Select -</option>
						@foreach($suppliers as $supplier)
						<option value="{{ $supplier->id }}" <?php echo (Input::get('sp_id') == $supplier->id) ? 'selected="selected"' : '' ?> >{{ $supplier->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('sp_id', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Requested by</th>
				<td>
					<select name="requested_by">
						<option value="">- Select -</option>
						@foreach($users as $user)
						<option value="{{ $user->id }}" <?php echo (Input::get('requested_by') == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
						@endforeach
					</select>
					{{ $errors->first('requested_by', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Date</th>
				<td>
					{{ Form::text('date', Input::get('date'), array('id' => 'datepicker', 'autocomplete' => 'off')) }}
					{{ $errors->first('date', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Remarks</th>
				<td>
					{{ Form::textarea('remarks', Input::get('remarks'),  array('rows' => '5px')) }}
					{{ $errors->first('remarks', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<div class="js_call_status"></div>
					<div class="items_cont">
						<?php 
							echo $item_error;
							if($purchaseitems->count()){
						?>
						<table>
							<tr>
								<td>Inventory</td>
								<td>Quantity</td>
								<?php if($show_price_field){ ?>
								<td>Unit Price</td>
								<td>Total Price</td>
								<td>Encoded Price</td>
								<?php } ?>
								<td>Conversion</td>
								<td>To be approve POs</td>
								<td>Approved POs</td>
							</tr>
						<?php
							$post_quantity = Input::get('quantity');
							$post_unit_price = Input::get('unit_price');
							$post_encoded_price = Input::get('encoded_price');
							
							foreach($purchaseitems as $index => $purchaseitem)
							{
								$checked = (isset($po_item[$purchaseitem->id]));
								$quantity = isset($post_quantity[$index]) ? $post_quantity[$index] : $purchaseitem->quantity;
								$unit_price = isset($post_unit_price[$index]) ? $post_unit_price[$index] : '';
								$encoded_price = isset($post_encoded_price[$index]) ? $post_encoded_price[$index] : '';
								
								
								$to_be_approve = PurchaseOrderItem::count_po_quantity($purchaseitem->id, 'To be approve');
								$approved = PurchaseOrderItem::count_po_quantity($purchaseitem->id, 'Approved');
						?>
							<tr>
								<td>{{ Form::checkbox('purchaseitems[]', $purchaseitem->id, $checked) .' ' . $purchaseitem->inv->name }}</td>
								<td>{{ Form::text('quantity[]', $quantity, array('autocomplete' => 'off')) }}</td>
								<?php if($show_price_field){ ?>
								<td>{{ Form::text('unit_price[]', $unit_price, array('autocomplete' => 'off')) }}</td>
								<td>{{ (is_numeric($unit_price) && is_numeric($quantity)) ? $unit_price * $quantity : ''; }}</td>
								<td>{{ Form::text('encoded_price[]', $encoded_price, array('autocomplete' => 'off')) }}</td>
								<?php } ?>
								<td>{{ '1 '.$purchaseitem->unitofdelivery .' = '. $purchaseitem->multiplier. ' '. $purchaseitem->unitofuse }}</td>
								<td>{{ $to_be_approve }}</td>
								<td>{{ $approved }}</td>
							</tr>
						<?php } ?>
						</table>
						<?php } else { echo '<h5>No items found.</h5>'; } ?>
					</div>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	$(window).load(function() {
	
		function show_items()
		{
			var pr_id = $('[name="pr_id"]').find('option:selected').val();
			
			if(pr_id != '')
			{
				$(".js_call_status").html("Loading...");
				$('.items_cont').html('');
				$.ajax({
					type	: "POST",
					cache	: false,
					url		: "<?php echo url("admin/po/get-items")?>",
					data: ({ 'pr_id' :  pr_id}),
					success: function(data) {
						$('.items_cont').html(data);
						$(".js_call_status").html("");
					}
				});
				
			}
			else
			{
				$('.items_cont').html('');
			}
		}
		
		$("[name='pr_id']").change( function(){
			show_items()
		});
	})
	</script>
	<style type="text/css">
	.items_cont td{ border:0; text-align:center}
	.items_cont td input{ margin:-5px 0 0}
	.items_cont td input[type="text"]{ width:40px}
	</style>
@stop
