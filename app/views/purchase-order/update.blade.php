@extends('template.main')

@section('title')
Update Purchase Order
@stop

@section('content')
	<form method="POST" class="form-horizontal">
		<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Approved</th>
				<td>
					{{ Form::select('approved', array('' => '-- Select --', 'Yes' => 'Yes', 'No' => 'No'), $purchase_order['approved']) }}
					{{ $errors->first('loc_id', ' <small class="error">:message</small>') }}
				</td>
			</tr>
			<tr>
				<th>Purchase Requsition</th>
				<td>
					<select name="pr_id">
						<option value="" >- Select -</option>
						@foreach($prs as $pr)
						<option value="{{ $pr->id }}" {{ ($purchase_order['pr_id'] == $pr->id) ? 'selected="selected"' : '' }} >{{ Helper::string_pad($pr->id) }}</option>
						@endforeach
					</select>
					{{ $errors->first('pr_id', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Supplier</th>
				<td>
					<select name="sp_id">
						<option value="">- Select -</option>
						@foreach($suppliers as $supplier)
						<option value="{{ $supplier->id }}" <?php echo ($purchase_order['sp_id'] == $supplier->id) ? 'selected="selected"' : '' ?> >{{ $supplier->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('sp_id', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Requested by</th>
				<td>
					<select name="requested_by">
						<option value="">- Select -</option>
						@foreach($users as $user)
						<option value="{{ $user->id }}" <?php echo ($purchase_order['requested_by'] == $user->id) ? 'selected="selected"' : '' ?> >{{ $user->firstname.' '.$user->lastname }}</option>
						@endforeach
					</select>
					{{ $errors->first('requested_by', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Date</th>
				<td>
					{{ Form::text('date', $purchase_order['date'], array('id' => 'datepicker', 'autocomplete' => 'off')) }}
					{{ $errors->first('date', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Remarks</th>
				<td>
					{{ Form::textarea('remarks', $purchase_order['remarks'],  array('rows' => '5px')) }}
					{{ $errors->first('remarks', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<table class="status">
						<tr>
							<td>{{ Form::checkbox('ordered', 'check', $purchase_order['ordered']) }} Ordered</td>
							<td>{{ Form::checkbox('fax_received', 'check', $purchase_order['fax_received']) }} Fax received</td>
							<td>{{ Form::checkbox('paid', 'check', $purchase_order['paid']) }} Paid</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<label class="js_call_status"></label>
					
					<div class="items_cont">
					<?php 
							echo $item_error;
							if($purchaseitems->count()){
							
							$post_quantity = Input::get('quantity');
							$post_unit_price = Input::get('unit_price');
							$post_encoded_price = Input::get('encoded_price');
							$post_item_id = Input::get('id');
						?>
						<table>
							<tr>
								<td>Inventory</td>
								<td>Quantity</td>
								<td>Unit Price</td>
								<td>Total Price</td>
								<td>Encoded Price</td>
								<td>Conversion</td>
								<td>To be approve POs</td>
								<td>Approved POs</td>
							</tr>
						<?php
							foreach($purchaseitems as $index => $purchaseitem)
							{
								$checked = (isset($po_item[$purchaseitem->id]));
								$quantity = (isset($po_item[$purchaseitem->id])) ? $po_item[$purchaseitem->id]['quantity'] : $post_quantity[$index];
								$unit_price = (isset($po_item[$purchaseitem->id])) ? $po_item[$purchaseitem->id]['unit_price'] : $post_unit_price[$index];
								$encoded_price = (isset($po_item[$purchaseitem->id])) ? $po_item[$purchaseitem->id]['encoded_price'] : $post_encoded_price[$index];
								$id = (isset($po_item[$purchaseitem->id])) ? $po_item[$purchaseitem->id]['id'] : $post_item_id[$index];
								
								$to_be_approve = PurchaseOrderItem::count_po_quantity($purchaseitem->id, 'To be approve');
								$approved = PurchaseOrderItem::count_po_quantity($purchaseitem->id, 'Approved');
						?>
							<tr>
								<td>{{ Form::checkbox('purchaseitems[]', $purchaseitem->id, $checked) . Form::hidden('id[]', $id) .' '. $purchaseitem->inv->name }}</td>
								<td>{{ Form::text('quantity[]', $quantity, array('autocomplete' => 'off')) }}</td>
								<td>{{ Form::text('unit_price[]', $unit_price, array('autocomplete' => 'off')) }}</td>
								<td>{{ (is_numeric($quantity) && is_numeric($unit_price)) ? $unit_price * $quantity : ''; }}</td>
								<td>{{ Form::text('encoded_price[]', $encoded_price, array('autocomplete' => 'off')) }}</td>
								<td>{{ $purchaseitem->unitofdelivery .' = '. $purchaseitem->multiplier. ' '. $purchaseitem->unitofuse }}</td>
								<td>{{ $to_be_approve }}</td>
								<td>{{ $approved }}</td>
							</tr>
						<?php } ?>
						</table>
						<?php } else { echo '<h5>No items found.</h5>'; } ?>
					</div>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<input type="reset" class="btn" value="Reset" />
				</td>
			</tr>
		</tbody>
		</table>
	</form>
	
	<style type="text/css">
	.status td, .items_cont td{ border:0; text-align:center}
	.status td input, .items_cont td input{ margin:-5px 0 0}
	.status td input[type="text"], .items_cont td input[type="text"]{ width:40px}
	</style>
	<script type="text/javascript">
	$(window).load(function() {
	
		function show_items()
		{
			var pr_id = $('[name="pr_id"]').find('option:selected').val();
			
			if(pr_id != '')
			{
				$(".js_call_status").html("Loading...");
				$('.items_cont').html('');
				$.ajax({
					type	: "POST",
					cache	: false,
					url		: "<?php echo url("admin/po/get-items")?>",
					data: ({ 'pr_id' :  pr_id}),
					success: function(data) {
						$('.items_cont').html(data);
						$(".js_call_status").html("");
					}
				});	
			}
			else
			{
				$('.items_cont').html('');
			}
		}
		
		$("[name='pr_id']").change( function(){
			show_items()
		});
	})
	</script>
@stop
