@extends('template.main')

@section('title')
Purchase Order
@stop

@section('page-nav')
<a href="{{ url('admin/po/create') }}">Create Purchase Order</a>
@stop

@section('content')
@if($purchase_orders->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>PO Id</th>
			<th>PR Id</th>
			<th>Supplier</th>
			<th>Prepared by</th>
			<th>Approved by</th>
			<th>Approved</th>
			<th>Paid</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($purchase_orders as $purchase_order)
			<tr>
				<td>
					<a href="{{ url('admin/po/view/'.$purchase_order->id) }}" >{{ Helper::string_pad($purchase_order->id) }}</a>
				</td>
				<td>
					<?php $pr = $purchase_order->pr->id ?>
					{{ $pr ? Helper::string_pad($pr) : '' }}
				</td>
				<td>
					<?php $supplier = $purchase_order->supplier ?>
					{{ ($supplier) ? $supplier->name : '' }}
				</td>
				<td>
					<?php $prepared_by = $purchase_order->preparedBy ?>
					{{ ($prepared_by) ? $prepared_by->firstname.' '.$prepared_by->lastname : '' }}
				</td>
				<td>
					<?php $approved_by = $purchase_order->approvedBy ?>
					{{ ($approved_by) ?  $approved_by->firstname.' '.$approved_by->lastname : '' }}
				</td>
				<td>{{ $purchase_order->approved }}</td>
				<td>{{ $purchase_order->paid }}</td>
				<td class="dlt_cont">
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/po/view/'.$purchase_order->id) }}" >View</a></li>
						<?php if(Auth::user()->type == 'Admin'){ ?>
						<li><a href="{{ url('admin/po/edit/'.$purchase_order->id) }}" >Edit</a></li>
						<?php } ?>
						<li><a class="delete" href="{{ url('admin/po/delete/'.$purchase_order->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop
