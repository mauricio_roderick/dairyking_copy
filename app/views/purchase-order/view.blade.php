@extends('template.main')

@section('title')
View Purchase Order
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Date</th>
				<td>
					{{ $purchase_order->date }}
				</td>
			</tr>
			<tr>
				<th>Purchase Requisition</th>
				<td>
					<?php $pr = $purchase_order->pr->id ?>
					{{ $pr ? Helper::string_pad($pr) : '' }}
				</td>
			</tr>
			<tr>
				<th>Supplier</th>
				<td>
					<?php $supplier = $purchase_order->supplier ?>
					{{ ($supplier) ? $supplier->name : '' }}
				</td>
			</tr>
			<tr>
				<th>Prepared by</th>
				<td>
					<?php $prepared_by = $purchase_order->preparedBy ?>
					{{ ($prepared_by) ? $prepared_by->firstname.' '.$prepared_by->lastname : '' }}
				</td>
			</tr>
			<tr>
				<th>Requested by</th>
				<td>
					<?php $requeste_by = $purchase_order->requesteBy ?>
					{{ ($requeste_by) ? $requeste_by->firstname.' '.$requeste_by->lastname : '' }}
				</td>
			</tr>
			<tr>
				<th>Approved by</th>
				<td>
					<?php $approved_by = $purchase_order->approvedBy ?>
					{{ ($approved_by) ?  $approved_by->firstname.' '.$approved_by->lastname : '' }}
				</td>
			</tr>
			<tr>
				<th>Approved</th>
				<td>{{ $purchase_order->approved }}</td>
			</tr>
			<tr>
				<th>Fax Received</th>
				<td>{{ $purchase_order->fax_received }}</td>
			</tr>
			<tr>
				<th>Ordered</th>
				<td>{{ $purchase_order->ordered }}</td>
			</tr>
			<tr>
				<th>Paid</th>
				<td>{{ $purchase_order->paid }}</td>
			</tr>
			<tr>
				<th>Items</th>
				<td>
					<table class="item_cont">
						<tr>
							<th>Inventory</th>
							<th>Quantity</th>
							<th>Conversion</th>
							<?php if($show_price_field){ ?>
							<th>Unit Price</th>
							<th>Total Price</th>
							<th>Encoded Price</th>
							<?php } ?>
						</tr>
					<?php foreach($purchase_order_items as $purchase_order_item){ 
						$purchaseitem = Purchaseitem::find($purchase_order_item->pr_item_id);
						$inventory = $purchaseitem ? $purchaseitem->inv : false;
					?>
						<tr>
						<td>{{ $inventory ? $inventory->name : '' }}</td>
						<td>{{ $purchase_order_item->quantity }}</td>
						<td>{{ $purchaseitem ? $purchaseitem->unitofdelivery. ' = '. $purchaseitem->multiplier . ' ' . $purchaseitem->unitofuse : '' }}</td>
						<?php if($show_price_field){ ?>
						<td>{{ $purchase_order_item->unit_price }}</td>
						<td>{{ $purchase_order_item->unit_price * $purchase_order_item->quantity }}</td>
						<td>{{ $purchase_order_item->encoded_total_price }}</td>
						<?php } ?>
						</tr>
					<?php } ?>
					</table>
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<?php if(Auth::user()->type == 'Admin'){ ?>
					<a href="{{ url('admin/po/edit/'.$purchase_order->id) }}" class="btn btn-primary" >Edit</a>
					<?php } ?>
					<a href="{{ url('admin/po') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	<style type="text/css">
		.item_cont td, .table-form .item_cont th{border:0; text-align:center}
		.item_cont .padded{padding:15px 10px 10px}
		.item_cont input{width:100px}
	</style>
@stop
