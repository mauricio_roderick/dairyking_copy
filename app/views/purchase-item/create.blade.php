@extends('template.main')

@section('title')
Create Purchase Item
@stop

@section('body')
	<form method="post">
		<fieldset>
			<div class="row">
				<div class="large-6 columns">
					{{ Form::label('pr_id', 'Purchase Requisition') }}
					<select id="pr_id" name="pr_id">
						<option value="">- Select -</option>
						@foreach($purchase_reqs as $purchase_req)
						<option value="{{ $purchase_req->id }}" <?php echo (Input::old('pr_id') == $purchase_req->id) ? 'selected="selected"' : '' ?> >{{ Helper::string_pad($purchase_req->id) }}</option>
						@endforeach
					</select>
					{{ $errors->first('pr_id', '<small class="error">:message</small>') }}
				</div>
				<div class="large-6 columns">
					{{ Form::label('po_id', 'Purchase Order') }}
					<select id="po_id" name="po_id">
						<option value="">- Select -</option>
					</select>
					{{ $errors->first('po_id', '<small class="error">:message</small>') }}
				</div>
			</div>
			<div class="row">
				<div class="large-6 columns">
					{{ Form::label('inv_id', 'Inventory') }}
					<select id="inv_id" name="inv_id">
						<option value="">- Select -</option>
						@foreach($inventories as $inventory)
						<option value="{{ $inventory->id }}" <?php echo (Input::old('inv_id') == $inventory->id) ? 'selected="selected"' : '' ?> >{{ $inventory->name }}</option>
						@endforeach
					</select>
					{{ $errors->first('inv_id', ' <small class="error">:message</small>') }}
				</div>
				<div class="large-6 columns">
					{{ Form::label('quantity', 'Quantity') }}
					{{ Form::text('quantity', Input::old('quantity')) }}
					{{ $errors->first('quantity', ' <small class="error">:message</small>') }}
				</div>
			</div>
			<div class="row">
				<div class="large-6 columns">
					{{ Form::label('unit', 'Unit') }}
					{{ Form::text('unit', Input::old('unit')) }}
					{{ $errors->first('unit', ' <small class="error">:message</small>') }}
				</div>
				<div class="large-6 columns">
					{{ Form::label('unit_price', 'Unit Price') }}
					{{ Form::text('unit_price', Input::old('unit_price')) }}
					{{ $errors->first('unit_price', ' <small class="error">:message</small>') }}
				</div>
			</div>
			<div class="row">
				<div class="large-6 columns">
					{{ Form::label('amount', 'Amount') }}
					{{ Form::text('amount', Input::old('amount')) }}
					{{ $errors->first('amount', ' <small class="error">:message</small>') }}
				</div>
			</div>
			<div class="row">
				<div class="large-12 columns">
					<input type="submit" value="Submit" name="submit" class="small button" />
					<input type="reset" class="small alert button" value="Reset" />
				</div>
			</div>
		 </fieldset>
	</form>
	<script type="text/javascript">
		
		$(document).ready(function(){
			var purchase_ords = <?php echo json_encode($purchase_ords) ?>;
			
			function show_purchase_ords()
			{
				var opt_value = $('[name="pr_id"]').find('option:selected').val();
					
				select_options = '<option value="" >- Select -</option>';
				
				if(typeof purchase_ords[opt_value] != 'undefined')
				{
					var p_ords = purchase_ords[opt_value];
					var selected_p_ords = "<?php echo Input::old('pr_id') ?>";
					
					for(x in p_ords)
					{
						var selected = (selected_p_ords == x) ? 'selected="selected"' : '';
						select_options += '<option value="' + x + '" ' + selected + ' >' + p_ords[x] + '</option>';
					}
				}
				
				$('[name="po_id"]').html(select_options)
			}
			
			show_purchase_ords();
			$('[name="pr_id"]').change(function(){
				show_purchase_ords()
			})
		})
	</script>
@stop
