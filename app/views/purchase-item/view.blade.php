@extends('template.main')

@section('title')
View Purchase Item
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Purchase Requisition</th>
				<td>
					<?php $pr = $purchaseitem->pr ?>
					{{ ($pr) ? Helper::string_pad($pr->id) : 'No Purchase req.' }}
				</td>
			</tr>
			<tr>
				<th>Purchase Order</th>
				<td>
					<?php $po = $purchaseitem->po ?>
					{{ ($po) ? Helper::string_pad($po->id) : 'No Purchase ord.' }}
				</td>
			</tr>
			<tr>
				<th>Inventory</th>
				<td>
					<?php $inv = $purchaseitem->inv ?>
				{{ ($inv) ? Helper::string_pad($inv->name) : 'No inventory.' }}
				</td>
			</tr>
			<tr>
				<th>Quantity</th>
				<td>{{ $purchaseitem->quantity }}</td>
			</tr>
			<tr>
				<th>Unit</th>
				<td>{{ $purchaseitem->unit }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					{{--<a href="{{ url('admin/pi/edit/'.$purchaseitem->id) }}" class="btn btn-primary" >Edit</a>--}}
					<a href="{{ url('admin/pi') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
