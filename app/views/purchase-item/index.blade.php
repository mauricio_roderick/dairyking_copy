@extends('template.main')

@section('title')
Purchase Item
@stop

@section('page-nav')
{{--<a href="{{ url('admin/pi/create') }}">Create Purchase Item</a>--}}
@stop

@section('content')
@if($purchaseitems->count())
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
			<th>PR Id</th>
			<th>Status</th>
			<th>Approved</th>
			<th>Inventory</th>
			<th>Quantity</th>
			<th>Conversion</th>
			<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($purchaseitems as $purchaseitem)
			
			<?php 
				$po = $purchaseitem->po;
				$inv = $purchaseitem->inv;
			?>
			<tr>
				<td>
					<?php $pr = $purchaseitem->pr ?>
					<a href="{{ url('admin/pi/view/'.$purchaseitem->id) }}" >{{ ($pr) ?  Helper::string_pad($pr->id) : 'No Purchase Req.' }}</a>
				</td>
				<td>
					{{ ($pr) ? $pr->status : '' }}
				</td>
				<td>
					{{ ($pr) ? $pr->approved : '' }}
				</td>
				<td>
					{{ ($inv) ? $inv->name : '' }}
				</td>
				<td>
					{{ $purchaseitem->quantity }}
				</td>
				<td>
					{{ $purchaseitem->unitofdelivery . ' = '. $purchaseitem->multiplier .' '. $purchaseitem->unitofuse }}
				</td>
				<td class="dlt_cont">
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/pi/view/'.$purchaseitem->id) }}" >View</a></li>
						{{--<li><a href="{{ url('admin/pi/edit/'.$purchaseitem->id) }}" >Edit</a></li>--}}
						<li><a class="delete" href="{{ url('admin/pi/delete/'.$purchaseitem->id) }}" >Delete</a></li>
					  </ul>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop
