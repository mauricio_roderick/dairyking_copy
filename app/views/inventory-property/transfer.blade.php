@extends('template.main')

@section('title')
View Inventory List
@stop

@section('content')
	<?php
		$inventory = $inventoryproperty->inv;
		$receiving_report = $inventoryproperty->rr;
		$parent_loc = $inventoryproperty->parentLocation;
		$sub_loc = $inventoryproperty->subLocation;
		$batch = date('Ymd', strtotime($inventoryproperty->datereceived))."-".Helper::string_pad($inventoryproperty->id);
		$datereceived = date('M d Y', strtotime($inventoryproperty->datereceived));
		$now = time();
		$curr = strtotime($datereceived);
		$diff = $now - $curr;
		$counter = floor($diff/(60*60*24));
	?>
	<form method="POST">
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Department</th>
				<td>{{ $parent_loc ? $parent_loc->name : '' }}</td>
			</tr>
			<tr>
				<th>Sub Department</th>
				<td>{{ $sub_loc ? $sub_loc->name : '' }}</td>
			</tr>
			<tr>
				<th>Inventory</th>
				<td>{{ $inventory ? $inventory->name : '' }}</td>
			</tr>
			<tr>
				<th>Batch</th>
				<td>{{ $batch }}</td>
			</tr>
			<tr>
				<th>Counter</th>
				<td>{{ $counter.' days' }}</td>
			</tr>
			{{--<tr>
				<th>Quantity Recieved</th>
				<td>{{ ($inventoryproperty->rr_id > 0) ? $inventoryproperty->quantityofdelivery.' '.$inventoryproperty->unitofdelivery.' = '. ($inventoryproperty->quantityofdelivery * $inventoryproperty->multiplier) .' '.$inventoryproperty->unitofuse : 'n/a' }}</td>
			</tr>--}}
			<tr>
				<th>Available Quantity</th>
				<td>{{ $inventoryproperty->available_quantity_to_use.' '.$inventoryproperty->unitofuse }}</td>
			</tr>
			<tr>
				<th>Department</th>
				<td>
					{{ Form::select('location', $locations_array, Input::get('location')) }}
					{{ $errors->first('location', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<?php
				$parent_id = Input::get('location');
				$sub_location_options = isset($sub_locations[$parent_id]) ? $sub_locations[$parent_id] : array();
				$sub_location_options[0] = '- Sub Department -';
				ksort($sub_location_options);
			?>
			<tr>
				<th>Sub Department</th>
				<td>
					{{ Form::select('sub_location', $sub_location_options, Input::get('sub_location')) }}
					{{ $errors->first('sub_location', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th>Quantity</th>
				<td>
					{{ Form::text('quantity', Input::get('quantity'), array('placeholder' => 'Quantity in '.$inventoryproperty->unitofuse)) }}
					{{ $errors->first('quantity', ' <span class="help-inline">:message</span>') }}
				</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
					<a href="{{ url('admin/inventory-property') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
	</form>
	<script type="text/javascript">
	$(window).load(function() {
		var sub_location = <?php echo json_encode($sub_locations) ?>;
	
		$("[name='location']").change(function(){
			var parent_location_id = $(this).find(":selected").val();
			var sub_location_options = '<option value="0">- Sub Department -</otpion>';
			
			if(typeof sub_location[parent_location_id] != 'undefined')
			{
				sub_locations = sub_location[parent_location_id];
				for (x in sub_locations)
				{
					sub_location_options += '<option value="' + x + '">' + sub_locations[x] + '</otpion>';
				}
			}
			
			$("[name='sub_location']").html(sub_location_options)
		});
	})
	</script>
@stop
