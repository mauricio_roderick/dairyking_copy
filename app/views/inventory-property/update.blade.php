@extends('template.main')

@section('title')
Update {{ $module_name }}
@stop

@section('content')
	<form method="post">
		<?php
		$inventory = $inventoryproperty_orig->inv;
		$receiving_report = $inventoryproperty_orig->rr;
		$parent_loc = $inventoryproperty_orig->parentLocation;
		$sub_loc = $inventoryproperty_orig->subLocation;
		$batch = date('Ymd', strtotime($inventoryproperty_orig->datereceived))."-".Helper::string_pad($inventoryproperty_orig->id);
		$datereceived = date('M d Y', strtotime($inventoryproperty_orig->datereceived));
		$now = time();
		$curr = strtotime($datereceived);
		$diff = $now - $curr;
		$counter = floor($diff/(60*60*24));
		?>
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Inventory</th>
					<td>{{ $inventory ? $inventory->name : '' }}</td>
				</tr>
				<tr>
					<th>Batch</th>
					<td>{{ $batch }}</td>
				</tr>
				<tr>
					<th>Counter</th>
					<td>{{ $counter.' days' }}</td>
				</tr>
				<tr>
					<th>Available Quantity</th>
					<td>{{ $inventoryproperty_orig->available_quantity_to_use.' '.$inventoryproperty_orig->unitofuse }}</td>
				</tr>
				<tr>
					<th>Department</th>
					<td>
						{{ Form::select('location', $locations_array, $inventoryproperty['location']) }}
						{{ $errors->first('location', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<?php
					$parent_id = $inventoryproperty['location'];
					$sub_location_options = isset($sub_locations[$parent_id]) ? $sub_locations[$parent_id] : array();
					$sub_location_options[0] = '- Sub Department -';
					ksort($sub_location_options);
				?>
				<tr>
					<th>Sub Department</th>
					<td>
						{{ Form::select('sub_location', $sub_location_options, $inventoryproperty['sub_location']) }}
						{{ $errors->first('sub_location', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Update" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	
	<script type="text/javascript">
	$(window).load(function() {
		var sub_location = <?php echo json_encode($sub_locations) ?>;
	
		$("[name='location']").change(function(){
			var parent_location_id = $(this).find(":selected").val();
			var sub_location_options = '<option value="0">- Sub Department -</otpion>';
			
			if(typeof sub_location[parent_location_id] != 'undefined')
			{
				sub_locations = sub_location[parent_location_id];
				for (x in sub_locations)
				{
					sub_location_options += '<option value="' + x + '">' + sub_locations[x] + '</otpion>';
				}
			}
			
			$("[name='sub_location']").html(sub_location_options)
		});
	})
	</script>
	<style type="text/css">
		.rr_dtls td, .table-form .rr_dtls th{border:0; text-align:left}
		select option:disabled{background:#D3C8C8; color:#fff}
	</style>
@stop
