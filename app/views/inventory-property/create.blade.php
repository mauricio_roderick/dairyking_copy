@extends('template.main')

@section('title')
Create {{ $module_name }}
@stop

@section('content')
	<form method="post">
		<table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Receiving Report</th>
					<td>
						<select name="rr_id">
							<option value="">- Select Receiving Report -</option>
							@foreach($receivingreports as $receivingreport)
							<?php 
								$disabled = '';
								$label = Helper::string_pad($receivingreport->id);
								if($receivingreport->total_distributed_units >= $receivingreport->available_units)
								{
									$disabled = 'disabled="disabled"';
									$label = $label."(All Distributed)";
								}
								$selected = (Input::get('rr_id') == $receivingreport->id) ? 'selected="selected"' : ''
							?>
							<option {{ $disabled }} value="{{ $receivingreport->id }}" {{ $selected }} >{{ $label }}</option>
							@endforeach
						</select>
						{{ $errors->first('rr_id', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<div class="rr_dtls">
						<?php echo $rr_dtls ?>
						</div>
					</td>
				</tr>
				<tr>
					<th>Location</th>
					<td>
						<select name="loc_id">
							<option value="">- Select Location -</option>
							@foreach($locations as $location)
							<option value="{{ $location->id }}" <?php echo (Input::get('loc_id') == $location->id) ? 'selected="selected"' : '' ?> >{{ $location->name }}</option>
							@endforeach
						</select>
						{{ $errors->first('loc_id', ' <span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Quantity of Delivery</th>
					<td>
						{{ Form::text('quantityofdelivery', Input::get('quantityofdelivery')) }}
						{{ $errors->first('quantityofdelivery', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th>Date Received</th>
					<td>
						{{ Form::text('datereceived', Input::get('datereceived'), array('id' => 'datepicker', 'readonly' => 'readonly')) }}
						{{ $errors->first('datereceived', '<span class="help-inline">:message</span>') }}
					</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<input type="submit" value="Submit" name="submit" class="btn btn-primary" />
						<input type="reset" class="btn" value="Reset" />
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<style type="text/css">
		.rr_dtls td, .table-form .rr_dtls th{border:0; text-align:left}
		select option:disabled{background:#D3C8C8; color:#fff}
	</style>
	
	<script type="text/javascript">
	$(window).load(function() {
	
		function show_items()
		{
			var rr_id = $('[name="rr_id"]').find('option:selected').val();
			
			if(rr_id != '')
			{
				$(".rr_dtls").html("Loading...");
				$.ajax({
					type	: "POST",
					cache	: false,
					url		: "<?php echo url("admin/inventory-property/get-rr-details")?>",
					data: ({ 'rr_id' :  rr_id }),
					success: function(data) {
						$('.rr_dtls').html(data);
					}
				});
			}
		}
		
		$("[name='rr_id']").change( function(){
			show_items()
		});
	})
	</script>
@stop
