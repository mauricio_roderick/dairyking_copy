@extends('template.main')

@section('title')
View {{ $module_name }}
@stop

@section('content')
	<?php
		$inventory = $inventoryproperty->inv;
		$receiving_report = $inventoryproperty->rr;
		$parent_loc = $inventoryproperty->parentLocation;
		$sub_loc = $inventoryproperty->subLocation;
		$batch = date('Ymd', strtotime($inventoryproperty->datereceived))."-".Helper::string_pad($inventoryproperty->id);
		$datereceived = date('M d Y', strtotime($inventoryproperty->datereceived));
		$now = time();
		$curr = strtotime($datereceived);
		$diff = $now - $curr;
		$counter = floor($diff/(60*60*24));
	?>
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Department</th>
				<td>{{ $parent_loc ? $parent_loc->name : '' }}</td>
			</tr>
			<tr>
				<th>Sub Department</th>
				<td>{{ $sub_loc ? $sub_loc->name : '' }}</td>
			</tr>
			<tr>
				<th>Inventory</th>
				<td>{{ $inventory ? $inventory->name : '' }}</td>
			</tr>
			<tr>
				<th>Batch</th>
				<td>{{ $batch }}</td>
			</tr>
			<tr>
				<th>Counter</th>
				<td>{{ $counter.' days' }}</td>
			</tr>
			<tr>
				<th>Available Quantity</th>
				<td>{{ $inventoryproperty->available_quantity_to_use.' '.$inventoryproperty->unitofuse }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<?php if(Auth::user()->type == 'Admin'){ ?>
					<a href="{{ url('admin/inventory-property/update/'.$inventoryproperty->id) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/inventory-property/transfer/'.$inventoryproperty->id) }}" class="btn btn-primary" >Transfer</a>
					<?php } ?>
					<a href="{{ url('admin/inventory-property') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
