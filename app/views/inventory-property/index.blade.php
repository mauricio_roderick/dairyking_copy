@extends('template.main')

@section('title')
{{ $module_title }}
@stop

@section('page-nav')
<a href="{{ url('admin/inventory-property/create') }}">{{ 'Create ' . $module_name }}</a>
@stop

@section('content')
@if(count($inventoryproperties))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Inventory</th>
				<th>Department</th>
				<th>Sub Department</th>
				<th>Available <br/>Quantity</th>
				<th>Batch</th>
				<th>Counter</th>

				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($inventoryproperties as $inventoryproperty)
			<?php
				$inventory = $inventoryproperty->inv;
				$parent_loc = $inventoryproperty->parentLocation;
				$sub_loc = $inventoryproperty->subLocation;
				$batch = date('Ymd', strtotime($inventoryproperty->datereceived))."-".Helper::string_pad($inventoryproperty->batch_id);
				$datereceived = date('M d Y', strtotime($inventoryproperty->datereceived));
				$now = time();
				$curr = strtotime($datereceived);
				$diff = $now - $curr;
				$counter = floor($diff/(60*60*24));

			?>
			<tr>
				<td><a href="{{ url('admin/inventory-property/view/'.$inventoryproperty->id) }}" >{{ $inventory ? $inventory->name : '' }}</a></td>
				<td>{{ $parent_loc ? $parent_loc->name : '' }}</td>
				<td>{{ $sub_loc ? $sub_loc->name : '' }}</td>
				<td>{{ $inventoryproperty->available_quantity_to_use.' '.$inventoryproperty->unitofuse }}</td>
				<td>{{ $batch }} </td>
				<td>{{ $counter }} days</td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/inventory-property/view/'.$inventoryproperty->id) }}" >View</a></li>
						<?php if(Auth::user()->type == 'Admin'){ ?>
						<li><a href="{{ url('admin/inventory-property/transfer/'.$inventoryproperty->id) }}" >Transfer</a></li>
						<li><a class="delete" href="{{ url('admin/inventory-property/delete/'.$inventoryproperty->id) }}" >Delete</a></li>
						<?php } ?>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop