@extends('template.main')

@section('title')
View Department
@stop

@section('content')
	<table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Name</th>
				<td>{{ $item_inventory['name'] }}</td>
			</tr>
			<tr>
				<th>Description</th>
				<td>{{ $item_inventory['description'] }}</td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/item-inventory/update/'.$item_inventory['id']) }}" class="btn btn-primary" >Edit</a>
					<a href="{{ url('admin/item-inventory') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
