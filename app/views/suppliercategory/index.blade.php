@extends('template.main')

@section('title')
{{ $module_title }}
@stop

@section('page-nav')
<a href="{{ url('admin/supplier_category/create') }}">Create {{ $module_name }}</a>
@stop

@section('content')
@if(count($categories))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Category</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($categories as $category)
			<tr>
				<td><a href="{{ url('admin/supplier_category/view/'.$category->id) }}" >{{ $category->name }}</a></td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/supplier_category/view/'.$category->id) }}" >View</a></li>
						<li><a href="{{ url('admin/supplier_category/update/'.$category->id) }}" class="button small secondary" >Edit</a></li>
						<li><a class="delete" href="{{ url('admin/supplier_category/delete/'.$category->id) }}" class="button small secondary" >Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop