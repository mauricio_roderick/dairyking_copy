@extends('template.main')

@section('title')
View {{ $module_name }}
@stop

@section('content')
	 <table class="table-form table-bordered">
			<tbody>
				<tr>
					<th>Name</th>
					<td>{{ $category->name }}</td>
				</tr>
				<tr>
					<th></th>
					<td>
						<a href="{{ url('admin/supplier_category/update/'.$category->id) }}" class="btn btn-primary" >Edit</a>
						<a href="{{ url('admin/supplier_category') }}" class="btn" >Back</a>
					</td>
				</tr>
		</tbody>
	</table>
@stop
