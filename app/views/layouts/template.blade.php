
<!DOCTYPE html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title>LWMC Dispatch Tool | @yield('title') </title>
  {{ HTML::style('css/normalize.css') }}
  {{ HTML::style('css/foundation.css') }}
 
 {{ HTML::script('js/vendor/custom.modernizr.js') }}



</head>
<body>

<!-- Nav Bar -->

	@include('layouts.nav')

<!-- End Nav -->


  <!-- Main Page Content and Sidebar -->
  <div class="row">
	 <!-- Main Blog Content -->
    <div class="large-9 columns" role="content">

   
			@yield('body')
   
    </div>

    <!-- End Main Content -->

    <!-- Sidebar -->
@include('layouts.side')
    <!-- End Sidebar -->
  </div>

  <!-- End Main Content and Sidebar -->


  <!-- Footer -->

  <footer class="row">
    <div class="large-12 columns">
      <hr />
      <div class="row">
        <div class="large-6 columns">
          <p>&copy; Copyright no one at all. Go to town.</p>
        </div>
        <div class="large-6 columns">
          <ul class="inline-list right">
            <li><a href="#">Link 1</a></li>
            <li><a href="#">Link 2</a></li>
            <li><a href="#">Link 3</a></li>
            <li><a href="#">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <script>
  document.write('<script src=js/vendor/' +
  ('__proto__' in {} ? 'zepto' : 'jquery') +
  '.js><\/script>')
  </script>
  {{ HTML::script('js/foundation.min.js') }}
    {{ HTML::script('js/vendor/zepto.js') }}
   {{ HTML::script('js/foundation/foundation.js') }}
   {{ HTML::script('js/foundation/foundation.dropdown.js') }}
  <script>
    $(document).foundation();
  </script>

  <!-- End Footer -->
</body>
</html>
