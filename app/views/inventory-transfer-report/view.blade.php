@extends('template.main')

@section('title')
View Inventory Transfer Report
@stop

@section('content')
	<?php 
		$inventory = $report->inventory;
		$from_location = $report->fromLocation;
		$from_sub_location = $report->fromSubLocation;
		$to_location = $report->toLocation;
		$to_sub_location = $report->toSubLocation;
	?>
	 <table class="table-form table-bordered">
		<tbody>
			<tr>
				<th>Report Id</th>
				<td>{{ Helper::string_pad($report->id) }}</td>
			</tr>
			<tr>
				<th>Inventory</th>
				<td><?php echo ($inventory) ? $inventory->name : ''; ?></td>
			</tr>
			<tr>
				<th>Quantity</th>
				<td><?php echo $report->quantity.' '.$report->unit ?></td>
			</tr>
			<tr>
				<th>From Location</th>
				<td><?php echo ($inventory) ? $inventory->name : ''; ?></td>
			</tr>
			<tr>
				<th>From Sub Location</th>
				<td><?php echo ($from_sub_location) ? $from_sub_location->name : ''; ?></td>
			</tr>
			<tr>
				<th>To Location</th>
				<td><?php echo ($to_location) ? $to_location->name : ''; ?></td>
			</tr>
			<tr>
				<th>To Sub Location</th>
				<td><?php echo ($to_sub_location) ? $to_sub_location->name : ''; ?></td>
			</tr>
			<tr>
				<th></th>
				<td>
					<a href="{{ url('admin/inventory-transfer-report') }}" class="btn" >Back</a>
				</td>
			</tr>
		</tbody>
	</table>
@stop
