@extends('template.main')

@section('title')
Inventory Transfer Report
@stop

@section('content')
@if(count($reports))
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<th>Report Id</th>
				<th>Inventory</th>
				<th>From Location</th>
				<th>From Sub Location</th>
				<th>To Location</th>
				<th>To Sub Location</th>
				<th width="80"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($reports as $report)
			<?php
				$inventory = $report->inventory;
				$from_location = $report->fromLocation;
				$from_sub_location = $report->fromSubLocation;
				$to_location = $report->toLocation;
				$to_sub_location = $report->toSubLocation;
			?>
			<tr>
				<td><a href="{{ url('admin/inventory-transfer-report/view/'.$report->id) }}" >{{ Helper::string_pad($report->id) }}</a></td>
				<td><?php echo ($inventory) ? $inventory->name : ''; ?></td>
				<td><?php echo ($from_location) ? $from_location->name : ''; ?></td>
				<td><?php echo ($from_sub_location) ? $from_sub_location->name : ''; ?></td>
				<td><?php echo ($to_location) ? $to_location->name : ''; ?></td>
				<td><?php echo ($to_sub_location) ? $to_sub_location->name : ''; ?></td>
				<td>
					<div class="btn-group">
					  <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						Action
						<span class="caret"></span>
					  </a>
					  <ul class="dropdown-menu">
						<li><a href="{{ url('admin/inventory-transfer-report/view/'.$report->id) }}" >View</a></li>
						<li><a class="delete" href="{{ url('admin/inventory-transfer-report/delete/'.$report->id) }}" >Delete</a></li>
					  </ul>
					</div>
					
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	{{ 'No records found.' }}
@endif
@stop