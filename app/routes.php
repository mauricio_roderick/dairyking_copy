<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::filter('admin', function()
{	
	$uri_segment = Request::segment(2);

	if( $uri_segment == 'login' || $uri_segment == '' )
	{
		if(Auth::user()) return Redirect::to('admin/user');
	}
	else
	{
		if(Auth::guest()) 
		{
			Notification::write('Please login first.', 'alert');
			return Redirect::to('admin/login');
		}
	}
});


Route::any('/', 'IndexController@login');
Route::any('admin', 'IndexController@login');
Route::any('admin/login', 'IndexController@login');
Route::any('logout', 'IndexController@logout');
Route::when('admin', 'admin');
Route::when('admin/*', 'admin');

Route::controller('admin/user', 'UserController');
Route::controller('admin/category', 'CategoriesController');
Route::controller('admin/supplier_category', 'SupplierCategoryController');
Route::controller('admin/pr', 'PurchaseRequisitionController');
Route::controller('admin/department', 'LocationController');
Route::controller('admin/supplier', 'SupplierController');
Route::controller('admin/po', 'PurchaseOrderController');
Route::controller('admin/pi', 'PurchaseItemController');
Route::controller('admin/inventory', 'InventoryController');
Route::controller('admin/receiving-report', 'ReceivingReportController');
Route::controller('admin/inventory-property', 'InventoryPropertyController');
Route::controller('admin/recipe', 'RecipeController');
Route::controller('admin/production-order', 'ProductionOrderController');
Route::controller('admin/inventory-transfer-report', 'InventoryTransferReportController');
Route::controller('admin/finish-goods', 'FinishGoodsController');
Route::controller('admin/customer', 'CustomerController');
Route::controller('admin/item-inventory', 'ItemInventoryController');
Route::controller('admin/customer-item', 'CustomerItemController');
/* 
Route::get('category', 'CategoriesController@index');
Route::any('category/create', 'CategoriesController@create');
Route::any('category/update/{id?}', 'CategoriesController@update');
Route::get('category/view/{id?}', 'CategoriesController@view');

Route::get('pr', 'PurchaseRequisitionController@index');
Route::any('pr/create', 'PurchaseRequisitionController@create');
Route::any('pr/update/{id?}', 'PurchaseRequisitionController@update');
Route::get('pr/view/{id?}', 'PurchaseRequisitionController@view');

Route::get('location', 'LocationController@index');
Route::any('location/create', 'LocationController@create');
Route::any('location/update/{id?}', 'LocationController@update');
Route::get('location/view/{id?}', 'LocationController@view');

Route::get('supplier', 'SupplierController@index');
Route::any('supplier/create', 'SupplierController@create');
Route::any('supplier/update/{id?}', 'SupplierController@update');
Route::get('supplier/view/{id?}', 'SupplierController@view');
 */
 

App::missing(function($exception)
{
	$return = 'Page not found';
    return $return;
});



