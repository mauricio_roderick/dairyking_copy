$(document).ready(function(){

	$('.delete').click(function(){
		return confirm('Are you sure you want to continue with this action?');
	})
	
	$( "#datepicker" ).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});
	
	$('form').submit(function() {
		if($(this).hasClass('subitted'))
		{
			return false;
		}
		else
		{
			$(this).addClass('subitted');
		}
	});
	
})	