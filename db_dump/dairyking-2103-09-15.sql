-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2013 at 06:05 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dairyking_091313`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_items`
--

CREATE TABLE IF NOT EXISTS `customer_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `finish_goods`
--

CREATE TABLE IF NOT EXISTS `finish_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_ord_id` int(11) NOT NULL,
  `finish_prod_id` int(11) NOT NULL,
  `quantity` decimal(5,2) NOT NULL,
  `unit_of_use` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `finish_goods`
--

INSERT INTO `finish_goods` (`id`, `prod_ord_id`, `finish_prod_id`, `quantity`, `unit_of_use`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 2.00, 'pack', '2013-09-12 20:17:30', '2013-09-12 20:17:30'),
(2, 2, 5, 15.00, 'pack', '2013-09-12 21:14:56', '2013-09-12 21:14:56'),
(3, 3, 5, 15.00, 'pack', '2013-09-12 21:15:36', '2013-09-12 21:15:36'),
(4, 4, 5, 15.00, 'pack', '2013-09-12 21:15:59', '2013-09-12 21:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `finish_good_items`
--

CREATE TABLE IF NOT EXISTS `finish_good_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fgd_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(5,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `finish_good_items`
--

INSERT INTO `finish_good_items` (`id`, `fgd_id`, `inv_id`, `quantity`, `unit`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1.00, 'can', '2013-09-12 19:49:31', '2013-09-12 19:49:31'),
(2, 3, 2, 2.00, 'pcs', '2013-09-12 19:49:31', '2013-09-12 19:49:31');

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(5,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `name`, `description`, `type`, `multiplier`, `unitofdelivery`, `unitofuse`, `supplier`, `category`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Inventory 1', 'Description', 'Raw Material', 10.00, 'box', 'can', '', '1', 0, '2013-09-12 19:48:30', '2013-09-12 19:48:30'),
(2, 'Inventory 2', 'Description', 'Packaging Material', 20.00, 'box', 'pcs', '', '1', 0, '2013-09-12 19:49:00', '2013-09-12 19:49:00'),
(3, 'Inventory 3', 'Description', 'Work in Process', 20.00, 'box', 'can', '', '2', 0, '2013-09-12 19:49:31', '2013-09-12 19:49:31'),
(4, 'Inventory 4', 'Description', 'Finish Product', 2.00, 'box', 'pack', '', '3', 0, '2013-09-12 19:49:56', '2013-09-12 20:02:23'),
(5, 'inventory 5', 'Description', 'Finish Product', 15.00, 'box', 'pack', '', '3', 0, '2013-09-12 20:58:23', '2013-09-12 20:58:52'),
(6, 'inventory 6', 'Description', 'Finish Product', 10.00, 'box', 'pack', '', '2', 0, '2013-09-12 21:08:21', '2013-09-12 21:08:21'),
(7, 'inventory 7', 'Description', 'Raw Material', 10.00, 'box', 'pack', '', '2', 0, '2013-09-13 19:52:36', '2013-09-13 19:52:36'),
(24, 'WP1', 'Description', 'Work in Process', 2.00, 'aaa', 'bbb', '', '1', 0, '2013-09-13 23:12:38', '2013-09-13 23:12:38'),
(25, 'FP1', 'Description', 'Finish Product', 2.00, 'ccc', 'ddd', '', '2', 0, '2013-09-13 23:13:54', '2013-09-13 23:13:54'),
(26, 'FP1', 'Description', 'Finish Product', 2.00, 'ccc', 'ddd', '', '2', 0, '2013-09-13 23:15:43', '2013-09-13 23:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `inventoryproperties`
--

CREATE TABLE IF NOT EXISTS `inventoryproperties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rr_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantityofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(5,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available_quantity_to_use` int(11) NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` int(11) NOT NULL,
  `sub_location` int(11) NOT NULL,
  `warning` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `datereceived` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `inventoryproperties`
--

INSERT INTO `inventoryproperties` (`id`, `rr_id`, `inv_id`, `quantityofdelivery`, `multiplier`, `unitofdelivery`, `available_quantity_to_use`, `unitofuse`, `location`, `sub_location`, `warning`, `lastuser`, `batch_id`, `datereceived`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '3', 10.00, 'box', 28, 'can', 2, 4, 0, 0, 1, '2013-09-13 03:59:05', '2013-09-12 19:59:05', '2013-09-13 04:17:30'),
(2, 2, 2, '5', 20.00, 'box', 97, 'pcs', 2, 3, 0, 0, 2, '2013-09-13 03:59:05', '2013-09-12 19:59:05', '2013-09-13 04:17:30'),
(3, 3, 1, '100', 10.00, 'box', 1000, 'can', 1, 0, 0, 0, 3, '2013-09-13 04:09:53', '2013-09-12 20:09:53', '2013-09-12 20:09:53'),
(4, 4, 2, '100', 20.00, 'box', 2000, 'pcs', 1, 0, 0, 0, 4, '2013-09-13 04:09:53', '2013-09-12 20:09:53', '2013-09-12 20:09:53'),
(5, 5, 2, '100', 20.00, 'box', 1985, 'pcs', 0, 0, 0, 0, 5, '2013-09-13 04:10:10', '2013-09-12 20:10:10', '2013-09-13 05:15:59'),
(6, 6, 1, '100', 10.00, 'box', 991, 'can', 0, 0, 0, 0, 6, '2013-09-13 04:10:10', '2013-09-12 20:10:10', '2013-09-13 05:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_components`
--

CREATE TABLE IF NOT EXISTS `inventory_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventory` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `inventory_components`
--

INSERT INTO `inventory_components` (`id`, `inventory`, `parent`, `inv_id`, `quantity`, `unit`, `created_at`, `updated_at`) VALUES
(1, 24, 0, 1, 2.00, 'can', '2013-09-13 23:12:38', '2013-09-13 23:12:38'),
(2, 24, 0, 2, 4.00, 'pcs', '2013-09-13 23:12:38', '2013-09-13 23:12:38'),
(3, 25, 0, 24, 2.00, 'bbb', '2013-09-13 23:13:54', '2013-09-13 23:13:54'),
(4, 25, 1, 1, 0.00, 'can', '2013-09-13 23:13:54', '2013-09-13 23:13:54'),
(5, 25, 1, 2, 0.00, 'pcs', '2013-09-13 23:13:54', '2013-09-13 23:13:54'),
(6, 25, 0, 7, 7.00, 'pack', '2013-09-13 23:13:54', '2013-09-13 23:13:54'),
(7, 26, 0, 7, 7.00, 'pack', '2013-09-13 23:15:43', '2013-09-13 23:15:43'),
(8, 26, 0, 24, 4.00, 'bbb', '2013-09-13 23:15:43', '2013-09-13 23:15:43'),
(9, 26, 8, 1, 8.00, 'can', '2013-09-13 23:15:43', '2013-09-13 23:15:43'),
(10, 26, 8, 2, 16.00, 'pcs', '2013-09-13 23:15:43', '2013-09-13 23:15:43');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_transfer_reports`
--

CREATE TABLE IF NOT EXISTS `inventory_transfer_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_inv_prop` int(11) NOT NULL,
  `from_location` int(11) NOT NULL,
  `from_sub_location` int(11) NOT NULL,
  `to_inv_prop` int(11) NOT NULL,
  `to_location` int(11) NOT NULL,
  `to_sub_location` int(11) NOT NULL,
  `quantity` decimal(5,2) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_inventories`
--

CREATE TABLE IF NOT EXISTS `item_inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `parent`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Department 1', 0, 0, '2013-09-12 19:51:11', '2013-09-12 19:51:11'),
(2, 'Department 2', 0, 0, '2013-09-12 19:51:17', '2013-09-12 19:51:17'),
(3, 'Department 3', 2, 0, '2013-09-12 19:51:25', '2013-09-12 19:51:25'),
(4, 'Department 4', 2, 0, '2013-09-12 19:51:33', '2013-09-12 19:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_07_10_090024_create_users_table', 1),
('2013_07_10_090038_create_locations_table', 1),
('2013_07_10_090130_create_purchaseitems_table', 1),
('2013_07_10_090146_create_suppliers_table', 1),
('2013_07_10_090209_create_logs_table', 1),
('2013_07_10_090243_create_purchaserequisitions_table', 1),
('2013_07_10_090304_create_purchasorders_table', 1),
('2013_07_10_092120_create_receivingreports_table', 1),
('2013_07_10_092359_create_inventories_table', 1),
('2013_07_10_092417_create_inventoryproperties_table', 1),
('2013_07_10_092534_create_categories_table', 1),
('2013_07_26_155304_create_suppliercategory_table', 1),
('2013_07_27_050552_create_purchaseOder_item_table', 1),
('2013_08_07_020723_create_recipes_table', 1),
('2013_08_07_035903_create_recipe_items_table', 1),
('2013_08_24_055908_create_finish_good_items_table', 1),
('2013_08_28_024212_create_inventory_transfer_reports_table', 1),
('2013_08_28_165115_create_production_orders_table', 1),
('2013_08_30_222216_create_production_order_items_table', 1),
('2013_09_07_062710_create_finish_goods_table', 1),
('2013_09_10_032354_create_customers_table', 1),
('2013_09_10_032434_create_customer_items_table', 1),
('2013_09_10_032456_create_item_inventory_Table', 1),
('2013_09_14_024400_create_iventory_components_table', 2),
('2013_09_14_033406_create_iventory_components_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `production_orders`
--

CREATE TABLE IF NOT EXISTS `production_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loc_id` int(11) NOT NULL,
  `sub_loc_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `production_orders`
--

INSERT INTO `production_orders` (`id`, `loc_id`, `sub_loc_id`, `created_at`, `updated_at`) VALUES
(1, 2, 4, '2013-09-13 04:17:30', '2013-09-13 04:17:30'),
(2, 1, 0, '2013-09-13 05:14:56', '2013-09-13 05:14:56'),
(3, 1, 0, '2013-09-13 05:15:36', '2013-09-13 05:15:36'),
(4, 1, 0, '2013-09-13 05:15:59', '2013-09-13 05:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `production_order_items`
--

CREATE TABLE IF NOT EXISTS `production_order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_ord_id` int(11) NOT NULL,
  `inv_prop_id` int(11) NOT NULL,
  `quantity` decimal(8,2) NOT NULL,
  `unitofuse` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `batch_datetime` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `production_order_items`
--

INSERT INTO `production_order_items` (`id`, `prod_ord_id`, `inv_prop_id`, `quantity`, `unitofuse`, `batch_id`, `batch_datetime`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2.00, 0, 1, '2013-09-13 03:59:05', '2013-09-12 20:17:30', '2013-09-12 20:17:30'),
(2, 1, 2, 3.00, 0, 2, '2013-09-13 03:59:05', '2013-09-12 20:17:30', '2013-09-12 20:17:30'),
(3, 2, 5, 5.00, 0, 5, '2013-09-13 04:10:10', '2013-09-12 21:14:56', '2013-09-12 21:14:56'),
(4, 2, 6, 3.00, 0, 6, '2013-09-13 04:10:10', '2013-09-12 21:14:56', '2013-09-12 21:14:56'),
(5, 3, 5, 5.00, 0, 5, '2013-09-13 04:10:10', '2013-09-12 21:15:36', '2013-09-12 21:15:36'),
(6, 3, 6, 3.00, 0, 6, '2013-09-13 04:10:10', '2013-09-12 21:15:36', '2013-09-12 21:15:36'),
(7, 4, 5, 5.00, 0, 5, '2013-09-13 04:10:10', '2013-09-12 21:15:59', '2013-09-12 21:15:59'),
(8, 4, 6, 3.00, 0, 6, '2013-09-13 04:10:10', '2013-09-12 21:15:59', '2013-09-12 21:15:59');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseitems`
--

CREATE TABLE IF NOT EXISTS `purchaseitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `pr_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `counter` int(11) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(5,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `purchaseitems`
--

INSERT INTO `purchaseitems` (`id`, `po_id`, `pr_id`, `inv_id`, `quantity`, `counter`, `unit`, `unitofuse`, `multiplier`, `unitofdelivery`, `unit_price`, `amount`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, '3', 0, '', 'can', 10.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 0, 1, 2, '5', 0, '', 'pcs', 20.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, 2, 1, '100', 0, '', 'can', 10.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 0, 2, 2, '100', 0, '', 'pcs', 20.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 0, 3, 2, '100', 0, '', 'pcs', 20.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 0, 3, 1, '100', 0, '', 'can', 10.00, 'box', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorders`
--

CREATE TABLE IF NOT EXISTS `purchaseorders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prepared_by` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `fax_received` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordered` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `purchaseorders`
--

INSERT INTO `purchaseorders` (`id`, `pr_id`, `sp_id`, `date`, `remarks`, `prepared_by`, `requested_by`, `approved_by`, `fax_received`, `ordered`, `paid`, `status`, `approved`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2013-09-13', 'Remarks', 1, 1, 1, 'Yes', 'Yes', 'Yes', 'Approved', 'Yes', 0, '0000-00-00 00:00:00', '2013-09-12 19:54:39'),
(2, 2, 1, '2013-09-10', 'Remarks', 1, 1, 1, 'Yes', 'Yes', 'Yes', 'Approved', 'Yes', 0, '0000-00-00 00:00:00', '2013-09-12 20:08:03'),
(3, 3, 1, '2013-09-08', 'Remarks', 1, 1, 1, 'Yes', 'Yes', 'Yes', 'Approved', 'Yes', 0, '0000-00-00 00:00:00', '2013-09-12 20:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder_items`
--

CREATE TABLE IF NOT EXISTS `purchaseorder_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `pr_item_id` int(11) NOT NULL,
  `unit` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `encoded_total_price` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `purchaseorder_items`
--

INSERT INTO `purchaseorder_items` (`id`, `po_id`, `pr_item_id`, `unit`, `quantity`, `unit_price`, `encoded_total_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '', 3, 10.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 19:54:39'),
(2, 1, 2, '', 5, 20.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 19:54:39'),
(3, 2, 3, '', 100, 1000.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 20:08:03'),
(4, 2, 4, '', 100, 1000.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 20:08:03'),
(5, 3, 5, '', 100, 1000.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 20:09:22'),
(6, 3, 6, '', 100, 1000.00, 0.00, '0000-00-00 00:00:00', '2013-09-12 20:09:22');

-- --------------------------------------------------------

--
-- Table structure for table `purchaserequisitions`
--

CREATE TABLE IF NOT EXISTS `purchaserequisitions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `addressed_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `loc_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `purchaserequisitions`
--

INSERT INTO `purchaserequisitions` (`id`, `date`, `addressed_to`, `loc_id`, `requested_by`, `approved_by`, `status`, `approved`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, '0000-00-00', '1', 1, 1, 1, 'Approved', 'Yes', 0, '2013-09-12 19:52:30', '2013-09-12 19:53:03'),
(2, '0000-00-00', '1', 1, 1, 1, 'Approved', 'Yes', 0, '2013-09-12 20:05:55', '2013-09-12 20:06:25'),
(3, '0000-00-00', '1', 2, 1, 1, 'Approved', 'Yes', 0, '2013-09-12 20:06:10', '2013-09-12 20:06:35');

-- --------------------------------------------------------

--
-- Table structure for table `receivingreports`
--

CREATE TABLE IF NOT EXISTS `receivingreports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_item_id` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `available_quantity_to_use` decimal(5,2) NOT NULL,
  `multiplier` decimal(5,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_location` int(11) NOT NULL,
  `from_sub_location` int(11) NOT NULL,
  `to_location` int(11) NOT NULL,
  `to_sub_location` int(11) NOT NULL,
  `datereceived` date NOT NULL,
  `received_by` int(11) NOT NULL,
  `timereceived` time NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `receivingreports`
--

INSERT INTO `receivingreports` (`id`, `po_item_id`, `po_id`, `quantity`, `available_quantity_to_use`, `multiplier`, `unitofdelivery`, `unitofuse`, `from_location`, `from_sub_location`, `to_location`, `to_sub_location`, `datereceived`, `received_by`, `timereceived`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, 30.00, 10.00, 'box', 'can', 0, 0, 2, 4, '2013-09-13', 1, '00:00:00', 0, '2013-09-12 19:59:05', '2013-09-12 19:59:05'),
(2, 2, 1, 5, 100.00, 20.00, 'box', 'pcs', 0, 0, 2, 3, '2013-09-13', 1, '00:00:00', 0, '2013-09-12 19:59:05', '2013-09-12 19:59:05'),
(3, 3, 2, 100, 999.99, 10.00, 'box', 'can', 0, 0, 1, 0, '2013-09-12', 1, '00:00:00', 0, '2013-09-12 20:09:53', '2013-09-12 20:09:53'),
(4, 4, 2, 100, 999.99, 20.00, 'box', 'pcs', 0, 0, 1, 0, '2013-09-12', 1, '00:00:00', 0, '2013-09-12 20:09:53', '2013-09-12 20:09:53'),
(5, 5, 3, 100, 999.99, 20.00, 'box', 'pcs', 0, 0, 0, 0, '2013-09-12', 1, '00:00:00', 0, '2013-09-12 20:10:10', '2013-09-12 20:10:10'),
(6, 6, 3, 100, 999.99, 10.00, 'box', 'can', 0, 0, 0, 0, '2013-09-12', 1, '00:00:00', 0, '2013-09-12 20:10:10', '2013-09-12 20:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `finish_prod_id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL,
  `unit_of_use` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `finish_prod_id`, `multiplier`, `unit_of_use`, `description`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'pack', 'Description', '2013-09-12 19:50:35', '2013-09-12 20:55:28'),
(2, 4, 2, 'pack', '', '2013-09-12 20:42:22', '2013-09-12 20:53:30'),
(3, 5, 15, 'pack', 'Description', '2013-09-12 20:59:21', '2013-09-12 21:04:17'),
(4, 6, 10, 'pack', '', '2013-09-12 21:08:43', '2013-09-12 21:08:43'),
(5, 6, 10, 'pack', 'Description', '2013-09-12 21:08:59', '2013-09-12 21:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `recipe_items`
--

CREATE TABLE IF NOT EXISTS `recipe_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rcp_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(5,2) NOT NULL,
  `multiplier` decimal(5,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `recipe_items`
--

INSERT INTO `recipe_items` (`id`, `rcp_id`, `type`, `parent`, `inv_id`, `quantity`, `multiplier`, `unit`, `created_at`, `updated_at`) VALUES
(4, 2, 'Raw Material', 0, 1, 1.00, 10.00, 'can', '2013-09-12 20:42:22', '2013-09-12 20:42:22'),
(5, 2, 'Packaging Material', 0, 2, 1.00, 20.00, 'pcs', '2013-09-12 20:42:22', '2013-09-12 20:42:22'),
(6, 2, 'Work in Process', 0, 3, 1.00, 20.00, 'can', '2013-09-12 20:42:22', '2013-09-12 20:42:22'),
(7, 2, 'Raw Material', 6, 1, 1.00, 1.00, 'can', '2013-09-12 20:42:22', '2013-09-12 20:53:30'),
(8, 2, 'Raw Material', 6, 2, 2.00, 2.00, 'pcs', '2013-09-12 20:42:22', '2013-09-12 20:53:30'),
(9, 1, 'Raw Material', 0, 1, 1.00, 0.00, 'can', '2013-09-12 20:55:28', '2013-09-12 20:55:28'),
(10, 1, 'Packaging Material', 0, 2, 1.00, 20.00, 'pcs', '2013-09-12 20:55:28', '2013-09-12 20:55:28'),
(11, 1, 'Work in Process', 0, 3, 1.00, 20.00, 'can', '2013-09-12 20:55:28', '2013-09-12 20:55:28'),
(12, 1, 'Raw Material', 11, 1, 1.00, 1.00, 'can', '2013-09-12 20:55:28', '2013-09-12 20:55:28'),
(13, 1, 'Raw Material', 11, 2, 2.00, 2.00, 'pcs', '2013-09-12 20:55:28', '2013-09-12 20:55:28'),
(15, 3, 'Packaging Material', 0, 2, 1.00, 20.00, 'pcs', '2013-09-12 20:59:21', '2013-09-12 20:59:21'),
(16, 3, 'Work in Process', 0, 3, 2.00, 20.00, 'can', '2013-09-12 20:59:21', '2013-09-12 21:04:17'),
(17, 3, 'Raw Material', 16, 1, 2.00, 1.00, 'can', '2013-09-12 20:59:21', '2013-09-12 21:04:17'),
(18, 3, 'Raw Material', 16, 2, 4.00, 2.00, 'pcs', '2013-09-12 20:59:21', '2013-09-12 21:04:17'),
(19, 3, 'Raw Material', 0, 1, 1.00, 0.00, 'can', '2013-09-12 21:04:17', '2013-09-12 21:04:17'),
(20, 5, 'Raw Material', 0, 1, 1.00, 0.00, 'can', '2013-09-12 21:08:59', '2013-09-12 21:08:59'),
(21, 5, 'Packaging Material', 0, 2, 1.00, 20.00, 'pcs', '2013-09-12 21:09:36', '2013-09-12 21:09:36'),
(22, 5, 'Work in Process', 0, 3, 1.00, 20.00, 'can', '2013-09-12 21:09:36', '2013-09-12 21:09:36'),
(23, 5, 'Raw Material', 22, 1, 1.00, 1.00, 'can', '2013-09-12 21:09:36', '2013-09-12 21:09:36'),
(24, 5, 'Raw Material', 22, 2, 2.00, 2.00, 'pcs', '2013-09-12 21:09:36', '2013-09-12 21:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `suppliercategories`
--

CREATE TABLE IF NOT EXISTS `suppliercategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `suppliercategories`
--

INSERT INTO `suppliercategories` (`id`, `name`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Item category 1', 0, '2013-09-12 19:47:00', '2013-09-12 19:47:00'),
(2, 'Item category 2', 0, '2013-09-12 19:47:06', '2013-09-12 19:47:06'),
(3, 'Item category 3', 0, '2013-09-12 19:47:11', '2013-09-12 19:47:11');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` text COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactperson` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactnumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `cat_id`, `address`, `description`, `contactperson`, `contactnumber`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Supplier 1', '["1","2"]', 'Address 1', 'Description 1', 'Contact Person 1', '123455', 0, '2013-09-12 19:47:42', '2013-09-12 19:47:42'),
(2, 'Supplier 2', '["3"]', 'Address 2', 'Description 2', 'Contact Person 2', '123455', 0, '2013-09-12 19:48:04', '2013-09-12 19:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `type`, `permissions`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'fname', 'lname', 'test@yahoo.com', '$2y$08$3eWXVtUPT3owOh/6/b/Ynu1q9rCS0qE97BkwSnSLVfhGktAlodeWG', 'Admin', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
