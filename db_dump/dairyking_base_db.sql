-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2013 at 08:41 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dairyking_091313`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `address`, `contact_num`, `contact_person`, `created_at`, `updated_at`) VALUES
(1, 'customer 1', '', '123456', 'Contact Person', '2013-09-14 10:37:46', '2013-09-14 10:37:46'),
(2, 'customer 2', '', '123456', 'Contact Person', '2013-09-14 10:37:52', '2013-09-14 10:37:52'),
(3, 'customer 3', '', '123456', 'Contact Person', '2013-09-14 10:37:58', '2013-09-14 10:37:58'),
(4, 'customer 4', '', '123456', 'Contact Person', '2013-09-14 10:38:04', '2013-09-14 10:38:04'),
(5, 'customer 5', '', '123456', 'Contact Person', '2013-09-14 10:38:10', '2013-09-14 10:38:10'),
(6, 'customer 6', '', '123456', 'Contact Person', '2013-09-14 10:38:17', '2013-09-14 10:38:17');

-- --------------------------------------------------------

--
-- Table structure for table `customer_items`
--

CREATE TABLE IF NOT EXISTS `customer_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `customer_items`
--

INSERT INTO `customer_items` (`id`, `customer_id`, `item_id`, `quantity`, `comments`, `date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-08', '2013-09-14 10:38:41', '2013-09-14 10:38:41'),
(2, 1, 3, 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-01', '2013-09-14 10:38:59', '2013-09-14 10:38:59'),
(3, 2, 6, 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-05', '2013-09-14 10:39:55', '2013-09-14 10:39:55'),
(4, 2, 3, 2, 'Comments', '2013-09-12', '2013-09-14 10:40:36', '2013-09-14 10:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `finish_goods`
--

CREATE TABLE IF NOT EXISTS `finish_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_ord_id` int(11) NOT NULL,
  `finish_prod_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit_of_use` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `finish_good_items`
--

CREATE TABLE IF NOT EXISTS `finish_good_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fgd_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventories`
--

CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `inventories`
--

INSERT INTO `inventories` (`id`, `name`, `description`, `type`, `multiplier`, `unitofdelivery`, `unitofuse`, `supplier`, `category`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'inventory 1', 'Description', 'Raw Material', 5.00, 'a_delivery', 'a_use', '', '1', 0, '2013-09-14 10:30:59', '2013-09-14 10:30:59'),
(2, 'inventory 2', 'Description', 'Raw Material', 10.00, 'b_delivery', 'b_use', '', '2', 0, '2013-09-14 10:31:22', '2013-09-14 10:31:22'),
(3, 'inventory 3 PM', 'Description', 'Packaging Material', 5.00, 'c_delivery', 'c_use', '', '1', 0, '2013-09-14 10:31:49', '2013-09-14 10:31:49'),
(4, 'inventory 4 PM', 'Description', 'Packaging Material', 5.00, 'd_delivery', 'd_use', '', '4', 0, '2013-09-14 10:32:05', '2013-09-14 10:32:05'),
(5, 'inventory 5 WP', 'Description', 'Work in Process', 10.00, 'e_delivery', 'e_use', '', '5', 0, '2013-09-14 10:32:34', '2013-09-14 10:33:27'),
(6, 'inventory 6 WP', 'Description', 'Work in Process', 15.00, 'f_delivery', 'f_use', '', '5', 0, '2013-09-14 10:33:24', '2013-09-14 10:33:24'),
(7, 'inventory 7 FP', 'Description', 'Finish Product', 20.00, 'g_delivery', 'g_use', '', '2', 0, '2013-09-14 10:34:37', '2013-09-14 10:34:37'),
(8, 'inventory 8 FP', 'Description', 'Finish Product', 10.00, 'h_delivery', 'h_use', '', '2', 0, '2013-09-14 10:35:10', '2013-09-14 10:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `inventoryproperties`
--

CREATE TABLE IF NOT EXISTS `inventoryproperties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rr_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantityofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `available_quantity_to_use` int(11) NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` int(11) NOT NULL,
  `sub_location` int(11) NOT NULL,
  `warning` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `datereceived` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `inventory_components`
--

CREATE TABLE IF NOT EXISTS `inventory_components` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inventory` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `inventory_components`
--

INSERT INTO `inventory_components` (`id`, `inventory`, `parent`, `inv_id`, `quantity`, `unit`, `multiplier`, `created_at`, `updated_at`) VALUES
(1, 5, 0, 1, 1.00, 'a_use', 0.00, '2013-09-14 10:32:34', '2013-09-14 10:32:34'),
(2, 5, 0, 2, 2.00, 'b_use', 0.00, '2013-09-14 10:32:34', '2013-09-14 10:32:34'),
(3, 5, 0, 3, 3.00, 'c_use', 0.00, '2013-09-14 10:32:34', '2013-09-14 10:32:34'),
(4, 6, 0, 2, 2.00, 'b_use', 0.00, '2013-09-14 10:33:24', '2013-09-14 10:33:24'),
(5, 6, 0, 3, 3.00, 'c_use', 0.00, '2013-09-14 10:33:24', '2013-09-14 10:33:24'),
(6, 6, 0, 4, 4.00, 'd_use', 0.00, '2013-09-14 10:33:24', '2013-09-14 10:33:24'),
(7, 7, 0, 1, 1.00, 'a_use', 0.00, '2013-09-14 10:34:37', '2013-09-14 10:34:37'),
(8, 7, 0, 3, 3.00, 'c_use', 0.00, '2013-09-14 10:34:37', '2013-09-14 10:34:37'),
(9, 7, 0, 5, 5.00, 'e_use', 0.00, '2013-09-14 10:34:37', '2013-09-14 10:34:37'),
(10, 8, 0, 5, 5.00, 'e_use', 0.00, '2013-09-14 10:35:10', '2013-09-14 10:35:10'),
(11, 8, 0, 6, 6.00, 'f_use', 0.00, '2013-09-14 10:35:10', '2013-09-14 10:35:10'),
(12, 8, 0, 2, 2.00, 'b_use', 0.00, '2013-09-14 10:35:10', '2013-09-14 10:35:10');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_transfer_reports`
--

CREATE TABLE IF NOT EXISTS `inventory_transfer_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_inv_prop` int(11) NOT NULL,
  `from_location` int(11) NOT NULL,
  `from_sub_location` int(11) NOT NULL,
  `to_inv_prop` int(11) NOT NULL,
  `to_location` int(11) NOT NULL,
  `to_sub_location` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_inventories`
--

CREATE TABLE IF NOT EXISTS `item_inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `item_inventories`
--

INSERT INTO `item_inventories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'item 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:36:50', '2013-09-14 10:36:50'),
(2, 'item 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:36:56', '2013-09-14 10:36:56'),
(3, 'item 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:37:02', '2013-09-14 10:37:02'),
(4, 'item 4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:37:08', '2013-09-14 10:37:08'),
(5, 'item 5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:37:17', '2013-09-14 10:37:17'),
(6, 'item 6', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.', '2013-09-14 10:37:25', '2013-09-14 10:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `parent`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Department 1', 0, 0, '2013-09-14 10:35:28', '2013-09-14 10:35:28'),
(2, 'Department 2', 0, 0, '2013-09-14 10:35:33', '2013-09-14 10:35:33'),
(3, 'Department 3', 0, 0, '2013-09-14 10:35:39', '2013-09-14 10:35:39'),
(4, 'Department 4', 2, 0, '2013-09-14 10:35:48', '2013-09-14 10:35:48'),
(5, 'Department 5', 2, 0, '2013-09-14 10:35:54', '2013-09-14 10:35:54'),
(6, 'Department 6', 3, 0, '2013-09-14 10:36:04', '2013-09-14 10:36:04'),
(7, 'Department 7', 3, 0, '2013-09-14 10:36:10', '2013-09-14 10:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_07_10_090024_create_users_table', 1),
('2013_07_10_090038_create_locations_table', 1),
('2013_07_10_090130_create_purchaseitems_table', 1),
('2013_07_10_090146_create_suppliers_table', 1),
('2013_07_10_090209_create_logs_table', 1),
('2013_07_10_090243_create_purchaserequisitions_table', 1),
('2013_07_10_090304_create_purchasorders_table', 1),
('2013_07_10_092120_create_receivingreports_table', 1),
('2013_07_10_092359_create_inventories_table', 1),
('2013_07_10_092417_create_inventoryproperties_table', 1),
('2013_07_10_092534_create_categories_table', 1),
('2013_07_26_155304_create_suppliercategory_table', 1),
('2013_07_27_050552_create_purchaseOder_item_table', 1),
('2013_08_07_020723_create_recipes_table', 1),
('2013_08_07_035903_create_recipe_items_table', 1),
('2013_08_24_055908_create_finish_good_items_table', 1),
('2013_08_28_024212_create_inventory_transfer_reports_table', 1),
('2013_08_28_165115_create_production_orders_table', 1),
('2013_08_30_222216_create_production_order_items_table', 1),
('2013_09_07_062710_create_finish_goods_table', 1),
('2013_09_10_032354_create_customers_table', 1),
('2013_09_10_032434_create_customer_items_table', 1),
('2013_09_10_032456_create_item_inventory_Table', 1),
('2013_09_14_033406_create_iventory_components_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `production_orders`
--

CREATE TABLE IF NOT EXISTS `production_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `loc_id` int(11) NOT NULL,
  `sub_loc_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `production_order_items`
--

CREATE TABLE IF NOT EXISTS `production_order_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prod_ord_id` int(11) NOT NULL,
  `inv_prop_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `unitofuse` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `batch_datetime` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseitems`
--

CREATE TABLE IF NOT EXISTS `purchaseitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `pr_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `counter` int(11) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorders`
--

CREATE TABLE IF NOT EXISTS `purchaseorders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) NOT NULL,
  `sp_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prepared_by` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `fax_received` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordered` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `paid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseorder_items`
--

CREATE TABLE IF NOT EXISTS `purchaseorder_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `pr_item_id` int(11) NOT NULL,
  `unit` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `encoded_total_price` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchaserequisitions`
--

CREATE TABLE IF NOT EXISTS `purchaserequisitions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `addressed_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `loc_id` int(11) NOT NULL,
  `requested_by` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `approved` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `receivingreports`
--

CREATE TABLE IF NOT EXISTS `receivingreports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `po_item_id` int(11) NOT NULL,
  `po_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `available_quantity_to_use` decimal(10,2) NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `unitofdelivery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitofuse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_location` int(11) NOT NULL,
  `from_sub_location` int(11) NOT NULL,
  `to_location` int(11) NOT NULL,
  `to_sub_location` int(11) NOT NULL,
  `datereceived` date NOT NULL,
  `received_by` int(11) NOT NULL,
  `timereceived` time NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE IF NOT EXISTS `recipes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `finish_prod_id` int(11) NOT NULL,
  `multiplier` int(11) NOT NULL,
  `unit_of_use` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipe_items`
--

CREATE TABLE IF NOT EXISTS `recipe_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rcp_id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `multiplier` decimal(10,2) NOT NULL,
  `unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `suppliercategories`
--

CREATE TABLE IF NOT EXISTS `suppliercategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `suppliercategories`
--

INSERT INTO `suppliercategories` (`id`, `name`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'category 1', 0, '2013-09-14 10:27:53', '2013-09-14 10:27:53'),
(2, 'category 2', 0, '2013-09-14 10:28:00', '2013-09-14 10:28:00'),
(3, 'category 3', 0, '2013-09-14 10:28:06', '2013-09-14 10:28:06'),
(4, 'category 4', 0, '2013-09-14 10:28:13', '2013-09-14 10:28:13'),
(5, 'category 5', 0, '2013-09-14 10:28:20', '2013-09-14 10:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cat_id` text COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactperson` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactnumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `cat_id`, `address`, `description`, `contactperson`, `contactnumber`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'Supplier 1', '["1","2"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:28:50', '2013-09-14 10:28:50'),
(2, 'Supplier 2', '["1"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:28:58', '2013-09-14 10:30:15'),
(3, 'Supplier 3', '["2"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:29:12', '2013-09-14 10:30:13'),
(4, 'Supplier 4', '["3"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:29:28', '2013-09-14 10:29:28'),
(5, 'Supplier 5', '["4"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:29:44', '2013-09-14 10:29:44'),
(6, 'Supplier 6', '["5"]', 'Address', 'Description', 'Contact Person', '123455', 0, '2013-09-14 10:29:56', '2013-09-14 10:29:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastuser` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `type`, `permissions`, `lastuser`, `created_at`, `updated_at`) VALUES
(1, 'fname', 'fname', 'fname', '$2y$08$h8mYsuRR4nJ1P35Azk060.pzqFp9csBH3PGpc1OEPpdJZSbGsAHDG', 'Admin', '', 0, '2013-07-26 08:44:58', '2013-08-29 18:28:29'),
(2, 'fname2', 'lname2', 'test2@yahoo.com', '$2y$08$5YHaVpfPV0GPNJ3byoujwOCZ2FZ5iIpbgAL42hzV1H3ay/N22AJ6i', 'Admin', '', 0, '2013-07-26 09:30:49', '2013-07-26 09:30:49'),
(3, 'fname3', 'lname3', 'test3@yahoo.com', '$2y$08$sqMCXcL.yo1ZrioatZ0qWuv6BCRWrXBxcqJbLiKeD4uhG6UUmD/Dy', '', '', 0, '2013-07-26 09:31:03', '2013-07-26 09:31:03'),
(4, 'fname4', 'lname4', 'test4@yahoo.com', '$2y$08$b4RubSaAPHeeLqSOKih/nu1NDswO8VT.wRokx32auZZoG6jNQWuiG', '', '', 0, '2013-07-26 09:31:18', '2013-07-26 09:31:18'),
(5, 'fname n1', 'lname n1', 'user_n1', '$2y$08$2lmuYBDChDbAr0zY.tgjzO7B9zoc6Tnkm017o5IG9U0pMlHn9nA0O', 'User', '', 0, '2013-08-29 18:30:09', '2013-08-29 18:30:09'),
(7, 'First', 'Last', 'test@yahoo.com', '$2y$08$3eWXVtUPT3owOh/6/b/Ynu1q9rCS0qE97BkwSnSLVfhGktAlodeWG', 'Admin', '', 0, '2013-08-29 18:58:30', '2013-08-29 18:58:30');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
